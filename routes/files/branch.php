<?php

use Illuminate\Support\Facades\Route;

Route::prefix('branch')
    ->name('branch.')
    ->group(function () {
        // for select package
        // branch.package => /branch/package/{mainBranch}
        Route::get('/package/options', [\App\Http\Controllers\BranchController::class, 'packageOptions'])->name('packageOptions');

        Route::middleware('auth.role:main.super,main.master,main.admin,branch.admin')
            ->group(function () {
                // branch.index => /branch/
                Route::get('/', [\App\Http\Controllers\BranchController::class, 'index'])->name('index');
                // branch.dataTable => /branch/datatable
                Route::get('/datatable', [\App\Http\Controllers\BranchController::class, 'dataTable'])->name('dataTable');
            });

        Route::middleware('auth.role:main.super')
            ->group(function () {
                // branch.create => /branch/create
                Route::get('/create', [\App\Http\Controllers\BranchController::class, 'create'])->name('create');
                // branch.store => /branch/store
                Route::post('/store', [\App\Http\Controllers\BranchController::class, 'store'])->name('store');
                // branch.edit => /branch/edit/{mainBranch}
                Route::get('/edit/{mainBranch}', [\App\Http\Controllers\BranchController::class, 'edit'])->name('edit');
                // branch.update => /branch/update/{mainBranch}
                Route::post('/update/{mainBranch}', [\App\Http\Controllers\BranchController::class, 'update'])->name('update');
                // branch.toggleActive => /branch/toggle-active/{mainBranch}
                Route::post('/toggle-active/{mainBranch}', [\App\Http\Controllers\BranchController::class, 'toggleActive'])->name('toggleActive');

                Route::prefix('admin/{mainBranch}')
                    ->name('admin.')
                    ->group(function () {
                        // branch.admin.form => /branch/admin/{mainBranch}
                        Route::get('/', [\App\Http\Controllers\BranchController::class, 'viewFormAdmin'])->name('form');
                        // branch.saveAdmin => /branch/admin/{mainBranch}
                        Route::post('/', [\App\Http\Controllers\BranchController::class, 'saveAdmin'])->name('save');
                    });
            });
    });
