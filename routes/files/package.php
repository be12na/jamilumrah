<?php

use Illuminate\Support\Facades\Route;

Route::prefix('package')
    ->name('package.')
    ->middleware(['auth.profile', 'auth.has.bank'])
    ->group(function () {
        Route::middleware('auth.role:main.super,main.master,main.admin,branch.admin')
            ->group(function () {
                // package.index => /package/
                Route::get('/', [\App\Http\Controllers\PackageController::class, 'index'])->name('index');
                // package.dataTable => /package/datatable
                Route::get('/datatable', [\App\Http\Controllers\PackageController::class, 'dataTable'])->name('dataTable');
            });

        Route::middleware('auth.role:main.super')
            ->group(function () {
                // package.create => /package/create
                Route::get('/create', [\App\Http\Controllers\PackageController::class, 'create'])->name('create');
                // package.store => /package/store
                Route::post('/store', [\App\Http\Controllers\PackageController::class, 'store'])->name('store');
                // package.edit => /package/edit/{mainPackage}
                Route::get('/edit/{mainPackage}', [\App\Http\Controllers\PackageController::class, 'edit'])->name('edit');
                // package.update => /package/update/{mainPackage}
                Route::post('/update/{mainPackage}', [\App\Http\Controllers\PackageController::class, 'update'])->name('update');
                // package.toggleActive => /package/toggle-active/{mainPackage}
                Route::post('/toggle-active/{mainPackage}', [\App\Http\Controllers\PackageController::class, 'toggleActive'])->name('toggleActive');
            });
    });
