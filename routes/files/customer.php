<?php

use Illuminate\Support\Facades\Route;

Route::prefix('customer')
    ->name('customer.')
    ->middleware(['auth.profile', 'auth.has.bank'])
    ->group(function () {
        Route::middleware('auth.role:main.super,main.master,main.admin,branch.admin,user.agency')
            ->group(function () {
                // customer.index => /customer/
                Route::get('/', [\App\Http\Controllers\CustomerController::class, 'index'])->name('index');
                // customer.dataTable => /customer/datatable
                Route::get('/datatable', [\App\Http\Controllers\CustomerController::class, 'dataTable'])->name('dataTable');
                // customer.detail => /customer/detail/{customer}
                Route::get('/detail/{customer}', [\App\Http\Controllers\CustomerController::class, 'detail'])->name('detail');
            });

        Route::prefix('resend')
            ->name('resend.')
            ->middleware('auth.role:main.super,main.master,branch.admin')
            ->group(function () {
                // customer.create => /customer/create
                Route::get('/to-be-agency/{unRegisteredCustomerId}', [\App\Http\Controllers\CustomerController::class, 'resendToBeAgency'])->name('toBeAgency');
                // customer.store => /customer/store
                Route::post('/to-be-agency/{unRegisteredCustomerId}', [\App\Http\Controllers\CustomerController::class, 'resendToBeAgency'])->name('toBeAgency');
            });

        Route::middleware('auth.role:main.super,user.agency')
            ->group(function () {
                // customer.create => /customer/create
                Route::get('/create', [\App\Http\Controllers\CustomerController::class, 'create'])->name('create');
                // customer.store => /customer/store
                Route::post('/store', [\App\Http\Controllers\CustomerController::class, 'store'])->name('store');
            });

        Route::middleware('auth.role:main.super,branch.admin')
            ->group(function () {
                // customer.edit => /customer/edit/{customerEditable}
                Route::get('/edit/{customerEditable}', [\App\Http\Controllers\CustomerController::class, 'edit'])->name('edit');
                // customer.update => /customer/update/{customerEditable}
                Route::post('/update/{customerEditable}', [\App\Http\Controllers\CustomerController::class, 'update'])->name('update');
                // customer.transfer => /customer/transfer/{customerTransferable}
                Route::get('/transfer/{customerTransferable}', [\App\Http\Controllers\CustomerController::class, 'transfer'])->name('transfer');
                // customer.saveTransfer => /customer/save-transfer/{customerTransferable}
                Route::post('/save-transfer/{customerTransferable}', [\App\Http\Controllers\CustomerController::class, 'saveTransfer'])->name('saveTransfer');
            });

        Route::middleware('auth.role:main.super')
            ->group(function () {
                Route::prefix('approve/{customerApprove}')
                    ->name('approve.')
                    ->group(function () {
                        // customer.approve.view => /customer/approve/{customerApprove}
                        Route::get('/', [\App\Http\Controllers\CustomerController::class, 'approveCustomer'])->name('view');
                        // customer.approve.save => /customer/approve/{customerApprove}
                        Route::post('/', [\App\Http\Controllers\CustomerController::class, 'approveCustomer'])->name('save');
                    });

                Route::prefix('reject/{customerReject}')
                    ->name('reject.')
                    ->group(function () {
                        // customer.reject.view => /customer/reject/{customerReject}
                        Route::get('/', [\App\Http\Controllers\CustomerController::class, 'rejectCustomer'])->name('view');
                        // customer.reject.save => /customer/reject/{customerReject}
                        Route::post('/', [\App\Http\Controllers\CustomerController::class, 'rejectCustomer'])->name('save');
                    });

                Route::prefix('cancel/{customerCancel}')
                    ->name('cancel.')
                    ->group(function () {
                        // customer.cancel.view => /customer/reject/{customerCancel}
                        Route::get('/', [\App\Http\Controllers\CustomerController::class, 'cancelCustomer'])->name('view');
                        // customer.cancel.save => /customer/reject/{customerCancel}
                        Route::post('/', [\App\Http\Controllers\CustomerController::class, 'cancelCustomer'])->name('destroy');
                    });

                Route::prefix('remove/{customerRemove}')
                    ->name('remove.')
                    ->group(function () {
                        // customer.cancel.view => /customer/reject/{customerCancel}
                        Route::get('/', [\App\Http\Controllers\CustomerController::class, 'removeCustomer'])->name('view');
                        // customer.cancel.save => /customer/reject/{customerCancel}
                        Route::post('/', [\App\Http\Controllers\CustomerController::class, 'removeCustomer'])->name('destroy');
                    });

                Route::prefix('excel')
                    ->name('excel.')
                    ->group(function () {
                        Route::prefix('import')
                            ->name('import.')
                            ->group(function () {
                                // customer.excel.import = /customer/excel/import
                                Route::get('/', [\App\Http\Controllers\CustomerController::class, 'viewImport'])->name('index');
                                // customer.excel.apply = /customer/excel/import/apply
                                Route::post('/apply', [\App\Http\Controllers\CustomerController::class, 'applyImport'])->name('apply');
                                // customer.excel.save = /customer/excel/import/save
                                Route::post('/save', [\App\Http\Controllers\CustomerController::class, 'saveImport'])->name('save');
                            });

                        // customer.excel.download = /customer/excel/download
                        Route::get('/download', [\App\Http\Controllers\CustomerController::class, 'downloadImportFormat'])->name('download');
                    });
            });
    });
