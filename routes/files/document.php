<?php

use Illuminate\Support\Facades\Route;

Route::prefix('download')
    ->name('download.')
    ->group(function () {
        // download.index => /download/
        Route::get('/', [\App\Http\Controllers\DocumentController::class, 'index'])->name('index');
        // download.download => /download/file/{document}
        Route::get('/file/{document}', [\App\Http\Controllers\DocumentController::class, 'download'])->name('download');
    });

Route::prefix('document')
    ->name('document.')
    ->middleware('auth.role:main.super,main.master,main.admin')
    ->group(function () {
        // document.index => /document/
        Route::get('/', [\App\Http\Controllers\DocumentController::class, 'index'])->name('index');

        // for upload document
        Route::middleware('auth.role:main.super,main.master')
            ->group(function () {
                // document.upload => /document/upload
                Route::get('upload', [\App\Http\Controllers\DocumentController::class, 'upload'])->name('formUpload');
                // document.upload => /document/upload
                Route::post('upload', [\App\Http\Controllers\DocumentController::class, 'upload'])->name('saveUpload');
                // document.remove => /document/remove
                Route::get('remove/{document}', [\App\Http\Controllers\DocumentController::class, 'remove'])->name('remove');
                // document.destroy => /document/destroy
                Route::post('upload/{document}', [\App\Http\Controllers\DocumentController::class, 'remove'])->name('destroy');
            });
    });
