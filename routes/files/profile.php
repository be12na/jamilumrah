<?php

use Illuminate\Support\Facades\Route;

Route::prefix('profile')
    ->name('profile.')
    ->group(function () {
        Route::middleware('auth.role:user.agency')
            ->group(function () {
                // profile.index => /profile/
                Route::get('/', [\App\Http\Controllers\ProfileController::class, 'index'])
                    ->middleware('auth.profile')
                    ->name('index');
                // profile.edit => /profile/edit
                Route::get('/edit', [\App\Http\Controllers\ProfileController::class, 'edit'])->name('edit');
                // profile.update => /profile/update
                Route::post('/update', [\App\Http\Controllers\ProfileController::class, 'update'])->name('update');
            });
    });
