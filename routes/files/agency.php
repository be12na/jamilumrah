<?php

use Illuminate\Support\Facades\Route;

Route::prefix('agency')
    ->name('agency.')
    ->group(function () {
        Route::middleware('auth.role:main.super,main.master,main.admin')
            ->group(function () {
                // agency.index => /agency/
                Route::get('/', [\App\Http\Controllers\AgencyController::class, 'index'])->name('index');
                // agency.activation => /agency/activation
                Route::get('/activation', [\App\Http\Controllers\AgencyController::class, 'index'])->name('activation');
                // agency.dataTable => /agency/datatable
                Route::get('/datatable', [\App\Http\Controllers\AgencyController::class, 'dataTable'])->name('dataTable');
                // agency.detail => /agency/detail/{mainAgent}/{agentRegister}
                Route::get('/detail/{mainAgent}/{agentRegister}', [\App\Http\Controllers\AgencyController::class, 'detail'])->name('detail');
            });

        Route::middleware('auth.role:main.super')
            ->group(function () {
                // agency.approve => /agency/approve/{mainAgent}/{agentRegister}
                Route::get('/approve/{mainAgent}/{agentRegister}', [\App\Http\Controllers\AgencyController::class, 'approve'])->name('approve');
                Route::post('/approve/{mainAgent}/{agentRegister}', [\App\Http\Controllers\AgencyController::class, 'approve']);
                // agency.reject => /agency/reject/{mainAgent}/{agentRegister}
                Route::get('/reject/{mainAgent}/{agentRegister}', [\App\Http\Controllers\AgencyController::class, 'reject'])->name('reject');
                Route::post('/reject/{mainAgent}/{agentRegister}', [\App\Http\Controllers\AgencyController::class, 'reject']);
                // agency.auth => /agency/auth/{mainAgent}
                Route::get('/auth/{mainAgent}', [\App\Http\Controllers\AgencyController::class, 'editAuth'])->name('editAuth');
                Route::post('/auth/{mainAgent}', [\App\Http\Controllers\AgencyController::class, 'editAuth']);

                Route::prefix('deactivate')
                    ->name('deactivate.')
                    ->group(function () {
                        // agency.deactivate.index => /agency/deactivate/{mainAgent}
                        Route::get('/{mainAgent}', [\App\Http\Controllers\AgencyController::class, 'deactivate'])->name('index');
                        // agency.deactivate.select2 => /agency/deactivate/{mainAgent}/select-referral
                        Route::get('/{mainAgent}/select-referral', [\App\Http\Controllers\AgencyController::class, 'deactivate'])->name('select2');
                        // agency.deactivate.confirm => /agency/deactivate/{mainAgent}/confirm
                        Route::get('/{mainAgent}/confirm', [\App\Http\Controllers\AgencyController::class, 'deactivate'])->name('confirm');
                        // agency.deactivate.update => /agency/deactivate/{mainAgent}/update
                        Route::post('/{mainAgent}/update', [\App\Http\Controllers\AgencyController::class, 'deactivate'])->name('update');
                    });
            });
    });
