<?php

use Illuminate\Support\Facades\Route;

Route::prefix('bonus')
    ->name('bonus.')
    ->middleware(['auth.profile', 'auth.has.bank'])
    ->group(function () {
        // bonus.dataTable => /bonus/datatable
        Route::get('/datatable', [\App\Http\Controllers\BonusController::class, 'dataTable'])->name('dataTable');

        Route::middleware('auth.role:user.agency')
            ->group(function () {
                // bonus.index => /bonus/
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'index'])->name('index');
            });

        Route::middleware('auth.role:main.super,main.master,main.admin,branch.admin')
            ->group(function () {
                // bonus.agency => /bonus/agency
                Route::get('/agency', [\App\Http\Controllers\BonusController::class, 'index'])->name('agency');
                // bonus.branch => /bonus/branch
                Route::get('/branch', [\App\Http\Controllers\BonusController::class, 'index'])->name('branch');
            });

        Route::middleware('auth.role:main.super')
            ->group(function () {
                // bonus.transfer => /bonus/transfer
                Route::get('/transfer', [\App\Http\Controllers\BonusController::class, 'transfer'])->name('viewTransfer');
                Route::post('/transfer', [\App\Http\Controllers\BonusController::class, 'transfer'])->name('saveTransfer');
            });
    });
