<?php

use Illuminate\Support\Facades\Route;

Route::prefix('bank')
    ->name('bank.')
    ->middleware(['auth.role:main.super,branch.admin,user.agency', 'auth.profile'])
    ->group(function () {
        // bank.index => /bank
        Route::get('/', [\App\Http\Controllers\UserBankController::class, 'index'])->name('index');

        Route::middleware('auth.bank.action:create')
            ->group(function () {
                // bank.create => /bank/create
                Route::get('/create', [\App\Http\Controllers\UserBankController::class, 'create'])->name('create');
                // bank.store => /bank/store
                Route::post('/store', [\App\Http\Controllers\UserBankController::class, 'store'])->name('store');
            });

        Route::middleware('auth.bank.action:enable')
            ->group(function () {
                // bank.enable => /bank/enable/{userBank}
                Route::get('/enable/{userBank}', [\App\Http\Controllers\UserBankController::class, 'edit'])->name('enable');
                // bank.enable => /bank/enable/{userBank}
                Route::post('/enable/{userBank}', [\App\Http\Controllers\UserBankController::class, 'update'])->name('enabled');
            });

        Route::middleware('auth.role:main.super')
            ->group(function () {
                // bank.disable => /bank/disable/{userBank}
                Route::get('/disable/{userBank}', [\App\Http\Controllers\UserBankController::class, 'edit'])->name('disable');
                // bank.disable => /bank/disable/{userBank}
                Route::post('/disable/{userBank}', [\App\Http\Controllers\UserBankController::class, 'update'])->name('disabled');
            });
    });
