<?php

use Illuminate\Support\Facades\Route;

Route::prefix('network')
    ->name('network.')
    ->middleware(['auth.role:user.agency,branch.admin', 'auth.profile', 'auth.has.bank'])
    ->group(function () {
        // network.index => /network/
        Route::get('/', [\App\Http\Controllers\NetworkController::class, 'index'])->name('index');
        // network.dataTable => /network/datatable
        Route::get('/datatable', [\App\Http\Controllers\NetworkController::class, 'dataTable'])->name('dataTable');
        // network.table => /network/table
        Route::get('/table', [\App\Http\Controllers\NetworkController::class, 'table'])->name('table');
        // network.tree => /network/table/{userTreeId?}
        Route::get('/tree/{userTreeId?}', [\App\Http\Controllers\NetworkController::class, 'tree'])->name('tree');
    });
