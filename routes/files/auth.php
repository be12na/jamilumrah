<?php

use Illuminate\Support\Facades\Route;

Route::prefix('password')
    ->name('password.')
    ->group(function () {
        // password.index => /password/
        Route::get('/', [\App\Http\Controllers\PasswordController::class, 'index'])->name('index');
        // password.update => /password/update
        Route::post('/update', [\App\Http\Controllers\PasswordController::class, 'update'])->name('update');
    });
