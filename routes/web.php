<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

Route::middleware('throttle:byIP20')
    ->group(function () {
        Route::middleware('guest')
            ->group(function () {
                // Route::prefix('reg-agency/{registerReferral?}')
                //     // v-1
                //     ->group(function () {
                //         Route::get('/', [\App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('regAgency');
                //         Route::post('/', [\App\Http\Controllers\Auth\RegisterController::class, 'register']);
                //     });

                Route::prefix('login')
                    ->group(function () {
                        Route::get('/', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
                        Route::post('/', [\App\Http\Controllers\Auth\LoginController::class, 'login']);
                    });

                Route::prefix('forgot')
                    ->name('forgot.')
                    ->group(function () {
                        Route::prefix('username')
                            ->name('username.')
                            ->group(function () {
                                // forgot.username.index
                                Route::get('/', [\App\Http\Controllers\ForgotController::class, 'viewForgotUsername'])->name('index');
                                // forgot.username.submit
                                Route::post('/', [\App\Http\Controllers\ForgotController::class, 'submitForgotUsername'])->name('submit');
                                // forgot.username.select
                                Route::get('/{forgotUsername}', [\App\Http\Controllers\ForgotController::class, 'selectForgotUsername'])->name('select');
                            });

                        Route::prefix('password')
                            ->name('password.')
                            ->group(function () {
                                // forgot.password.index
                                Route::get('/', [\App\Http\Controllers\ForgotController::class, 'viewForgotPassword'])->name('index');
                                // forgot.password.submit
                                Route::post('/', [\App\Http\Controllers\ForgotController::class, 'submitForgotPassword'])->name('submit');
                                Route::prefix('reset/{forgotPasswordToken}')
                                    ->name('reset.')
                                    ->group(function () {
                                        // forgot.password.reset.index
                                        Route::get('/', [\App\Http\Controllers\ForgotController::class, 'resetForgotPassword'])->name('index');
                                        // forgot.password.reset.save
                                        Route::post('/', [\App\Http\Controllers\ForgotController::class, 'resetForgotPassword'])->name('save');
                                    });
                            });
                    });


                // v-1
                // Route::prefix('reg-palmer/{unRegisteredCustomer}')
                //     ->group(function () {
                //         Route::get('/', [\App\Http\Controllers\CustomerController::class, 'toBeAgency'])->name('viewToBeAgency');
                //         Route::post('/', [\App\Http\Controllers\CustomerController::class, 'toBeAgency'])->name('saveToBeAgency');
                //     });

                Route::prefix('register')
                    ->group(function () {
                        // pendaftaran jamaah via link
                        Route::prefix('customer/{registerPalmerByToken?}')
                            ->group(function () {
                                // viewToBePalmer => register/customer/{registerPalmerByToken?}
                                Route::get('/', [\App\Http\Controllers\CustomerController::class, 'toBePalmer'])->name('viewToBePalmer');
                                // saveToBePalmer => register/customer/{registerPalmerByToken?}
                                Route::post('/', [\App\Http\Controllers\CustomerController::class, 'toBePalmer'])->name('saveToBePalmer');
                            });

                        // jamaah yang ingin menjadi agen
                        Route::prefix('to-be-agency/{unRegisteredCustomer}')
                            ->group(function () {
                                // viewToBeAgency => to-bo-agency/{unRegisteredCustomer}
                                Route::get('/', [\App\Http\Controllers\CustomerController::class, 'toBeAgency'])->name('viewToBeAgency');
                                // saveToBeAgency => to-bo-agency/{unRegisteredCustomer}
                                Route::post('/', [\App\Http\Controllers\CustomerController::class, 'toBeAgency'])->name('saveToBeAgency');
                            });
                    });
            });

        Route::prefix('logout')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
                Route::post('/', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);
            });
    });

Route::middleware('throttle:bySession200')
    ->group(function () {
        // selectRegion => /select-region
        Route::get('/select-region', [App\Http\Controllers\RegionController::class, 'region'])->name('selectRegion');
        // selectProvince => /select-province
        Route::get('/select-province', [App\Http\Controllers\RegionController::class, 'province'])->name('selectProvince');
        // selectCity => /select-city
        Route::get('/select-city', [App\Http\Controllers\RegionController::class, 'city'])->name('selectCity');
        // selectBranchByArea => /select-branch-by-area
        Route::get('/select-branch-by-area', [App\Http\Controllers\BranchController::class, 'byArea'])->name('selectBranchByArea');
        // dashboard.data => dashboard/data
        Route::get('/dashboard-data', [App\Http\Controllers\HomeController::class, 'getData'])->name('dashboard.data');
        // programOptions => program-options
        Route::get('/program-options', [\App\Http\Controllers\BranchController::class, 'packageOptions'])->name('programOptions');
    });

Route::middleware('throttle:bySession50')
    ->group(function () {
        Route::get('/', [App\Http\Controllers\HomeController::class, 'welcome'])->name('welcome');
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        Route::middleware('auth')
            ->group(function () {
                // dashboard => dashboard/
                Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])
                    ->middleware(['auth.profile', 'auth.has.bank'])
                    ->name('dashboard');


                include 'files/branch.php';
                include 'files/package.php';
                include 'files/agency.php';
                include 'files/profile.php';
                include 'files/auth.php';
                include 'files/bank.php';
                include 'files/network.php';
                include 'files/customer.php';
                include 'files/bonus.php';
                include 'files/document.php';
            });
    });
