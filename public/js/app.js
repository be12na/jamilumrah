function isEmptyElement( el ){
    return !$.trim(el.html());
}

function validUrl(url)
{
    if ((url == undefined) || (url == '')) {
        return false;
    }

    return ((url.substring(0, 5) == 'http:') || (url.substring(0, 6) == 'https:'));
}

function showMainProcessing()
{
    $('#processing-overlay').addClass('show');
}
function stopMainProcessing()
{
    let processTime;
    const processOverlay = $('#processing-overlay');
    const timeLoading = processOverlay.data('timer');
    let stopProcessTime = (timeLoading != undefined) ? timeLoading : 1000;
    processTime = setTimeout(function() {
        processOverlay.removeClass('show');
        clearTimeout(processTime);
    }, stopProcessTime);
}

function createAlert(msg, c) {
    let ic = 'question-circle';

    if (c == 'danger') {
        ic = 'times-circle';
    } else if (c == 'success') {
        ic = 'check-circle';
    } else if (c == 'warning') {
        ic = 'exclamation-circle';
    } else if (c == 'info') {
        ic = 'info-circle';
    }

    return '<div class="alert alert-' + c + ' alert-dismissible p-2"><div class="d-flex"><i class="flex-grow-0 flex-shrink-0 fs-3 fas fa-' + ic + '"></i><div class="flex-fill mx-2">' + msg + '</div><button type="button" class="btn-close p-0 position-static flex-grow-0 flex-shrink-0 lh-1" data-bs-dismiss="alert" style="background: none; font-size:12px;"><i class="fas fa-times"></i></button></div></div>';
}

function getFormErrorResponse(respon)
{
    if (typeof respon == 'object') {
        const msg = respon.responseJSON ? respon.responseJSON.message : respon.responseText;

        if (msg.substring(1, 4) == 'div') {
            return msg;
        } else {
            return createAlert(msg, 'danger');
        }
    } else {
        return respon;
    }
}

function changeViewPassword(sender, initialize)
{
    if (!(sender instanceof jQuery)) sender = $(sender);
    
    const input = $(sender.data('target'));
    const labelIcon = $('.pwd-icon', sender);
    const openIcon = input.data('icon-open');
    const closeIcon = input.data('icon-close');
    const isOpened = (input.attr('type') == 'text');

    if (initialize != true) {
        if (isOpened) {
            input.attr({type: 'password'});
            labelIcon.removeClass(closeIcon).addClass(openIcon).attr({title: 'Perlihatkan password'});
        } else {
            input.attr({type: 'text'});
            labelIcon.removeClass(openIcon).addClass(closeIcon).attr({title: 'Sembunyikan password'});
        }
    } else {
        labelIcon.removeClass(isOpened ? closeIcon : openIcon).addClass(initialize ? openIcon : closeIcon).attr({title: initialize ? 'Perlihatkan password' : 'Sembunyikan password'});
    }
}

function initPasswordView()
{
    $('.pwd-view:not(.initialized)').each(function(n, p) {
        changeViewPassword(p, true);
    });
}

function submitForm(f, tm)
{
    const frm = $(f);
    const data = frm.serialize();
    const url = frm.attr('action');
    const msg = $(tm);
    showMainProcessing();
    $.post({
        url: url,
        data: data
    }).done(function(respon) {
        window.location.replace(respon);
    }).fail(function(respon) {
        msg.empty().html(getFormErrorResponse(respon));
    }).always(function(respon) {
        stopMainProcessing();
    });

    return false;
}

function submitFormUpload(f, tm)
{
    const frm = $(f);
    const data = new FormData(frm[0]);
    const url = frm.attr('action');
    const msg = $(tm);
    showMainProcessing();
    $.post({
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false
    }).done(function(respon) {
        window.location.replace(respon);
    }).fail(function(respon) {
        msg.empty().html(getFormErrorResponse(respon));
    }).always(function(respon) {
        stopMainProcessing();
    });

    return false;
}

function replaceWindow(url)
{
    showMainProcessing();
    window.location.replace(url);
}

function renderUrlModal(dialog, url, data)
{
    const dt = data || {};
    dialog.empty();

    $.get({
        url: url,
        data: dt
    }).done(function(respon) {
        dialog.html(respon);
        const focusControl = $('.modal-content', dialog).data('onfocus');
        if ((focusControl != undefined) && (focusControl != '')) {
            const control = $(focusControl, dialog).first();
            if (control.length) {
                control[0].focus();
                if (control.is('input')) {
                    control[0].select();
                }
            }
        }
        initPasswordView();
    }).fail(function(respon) {
        let msg = respon;
        if (typeof respon == 'object') {
            msg = respon.responseJSON ? respon.responseJSON.message : respon.responseText;
        }
        
        const content = '<div class="modal-content">' + 
        '<div class="modal-header py-2 px-3">' + 
            '<span class="fw-bold small">Error</span>' + 
            '<button type="button" class="btn-close lh-1" data-bs-dismiss="modal" style="background: none; font-size:12px;"><i class="fa-solid fa-times"></i></button>' + 
        '</div>' + 
        '<div class="modal-body">' + msg + '</div>' +
        '<div class="modal-footer justify-content-center py-1"><button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal"><i class="fa-solid fa-times me-1"></i>Tutup</button></div>' +
        '</div>' + 
        '</div>';
        
        dialog.html(content);
    });
}

function initDatePicker()
{
    const bsDatePricker = $('.bs-date:not(.initialized)');
    
    if (bsDatePricker.length) {
        const lang = $('html').attr('lang');

        $.each(bsDatePricker, function(i, v) {
            const dt = $(v);
            const dtFormat = dt.data('date-format');
            const endDate = dt.data('date-end-date');
            let yRange = dt.data('date-year-range');
            if (!yRange) {
                yRange = '1945:' + (new Date().getFullYear()).toString();
            }
            
            dt.datepicker({
                changeMonth: true,
                changeYear: true,
                regional: lang,
                dateFormat: dtFormat ? dtFormat : ((lang && (lang == 'id')) ? 'd MM yy' : 'yy-mm-dd'),
                minDate: -362 * 100,
                maxDate: endDate ? endDate : 0,
                showOtherMonths: true,
                selectOtherMonths: true,
                yearRange: yRange,
            });
        });

        bsDatePricker.addClass('initialized');
    }
}

$(function() {
    $(document).on('submit', 'form[method="POST"]', function(e) {
        const frm = $(this);
        const frmEnc = frm.attr('enctype');
        let alertC = frm.data('alert-container');

        if ((alertC == undefined) || (alertC == '')) {
            alertC = 'alert-container';
            const c = $('<div/>').attr({id: alertC}).addClass('d-block');
            alertC = '#' + alertC;
            frm.data('alert-container', alertC);
            frm.before(c);
        }

        if (frmEnc == 'multipart/form-data') {
            return submitFormUpload(this, alertC);
        } else {
            return submitForm(this, alertC);
        }
    }).on('select2:open', '.select2bs4.select2-custom', function(e) {
        const x = $('body > .select2-container .select2-search__field');
        if (x.length) {
            x[0].focus();
        }
    }).on('click', 'a[href]:not([target]),button[data-href]', function(e) {
        const target = $(e.target);
        let url;

        if (target.is('A') || target.is('BUTTON')) {
            url = target.attr('href') || target.data('href');
        } else {
            let anc = target.closest('a[href]');
            if (anc.length == 0) {
                anc = target.closest('button[data-href]');
            }
            
            url = anc.length ? (anc.attr('href') || anc.data('href')) : undefined;
        }

        if (validUrl(url)) {
            replaceWindow(url);
            return false;
        }
    }).on('click', '.reload-modal', function(e) {
        const me = $(this);
        const mdl = me.closest('.modal');
        if (!mdl.hasClass('show')) {
            mdl.modal('show');
        }

        const url = me.data('modal-url');
        if ((url != undefined) && (url != '') && (url != '#')) {
            renderUrlModal($('.modal-dialog', mdl), url);
        }
    }).on('change', '.checked-hide, .checked-show', function(e) {
        const me = $(this);
        const target = $(me.data('target'));
        if (target.length) {
            const isCheckShow = me.hasClass('checked-show');
            const isChecked = me.is(':checked');
            const callBack = me.data('callback');

            if (isChecked) {
                if (isCheckShow) {
                    target.removeClass('d-none');
                } else {
                    target.addClass('d-none');
                }
            } else {
                if (isCheckShow) {
                    target.addClass('d-none');
                } else {
                    target.removeClass('d-none');
                }
            }

            if (callBack) {
                window[callBack](me);
            }
        }
    }).on('click', '.pwd-view', function(e) {
        changeViewPassword(this, false);
    });

    $('.modal').on('show.bs.modal', function(e) {
        const target = $(e.relatedTarget);
        const url = target.data('modal-url');
        if ((url != undefined) && (url != '') && (url != '#')) {
            renderUrlModal($('.modal-dialog', $(this)), url);
        }
    });
    
    initPasswordView();
    initDatePicker();

    $('.autoload-container').each(function(c) {
        const me = $(this);
        const url = me.data('autoload-url');

        me.html('-');
        if (url) {
            $.get({
                url: url
            }).done(function(respon) {
                me.html(respon);
            }).fail(function(respon) {
                me.html('<span class="text-danger">Error</span>');
            });
        }
    });

    stopMainProcessing();
});