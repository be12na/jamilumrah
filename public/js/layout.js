function clearMiniSidebarLink(sdBarMini, miniItem, level)
{
    let miniLink = miniItem.children('.sidebar-link');
    if (miniLink.length) {
        miniLink = $(miniLink[0]);
        miniLink.removeClass('collapsed').removeAttr('data-bs-toggle').removeAttr('data-bs-target');
    }
    
    const linkText = $(':last-child', miniLink);
    let title = '';
    if (linkText.length) {
        title = linkText.text();
        miniLink.attr('title', title);
        if (level == 0) {
            linkText.remove();
        }
    }

    level = level + 1;

    const miniSub = miniItem.children('.sidebar-menu');
    if (miniSub.length) {
        miniSub.removeClass('collapse show').addClass('shadow-sm').removeAttr('id').removeAttr('data-bs-parent');
        const miniTheme = sdBarMini.data('mini-sub-theme');
        if (miniTheme) {
            miniSub.addClass(miniTheme);
        }

        const miniItems = miniSub.children('.sidebar-item');

        $.each(miniItems, function(k, v) {
            clearMiniSidebarLink(sdBarMini, $(v), level);
        });

        if (title != '') {
            miniSub.prepend($('<li class="sidebar-item fw-bold"></li>').append('<span class="sidebar-link menu-link">' + title + '</span>'));
        }
    }
}

$(function() {
    const sdBar = $('.sidebar');
    if (sdBar.length) {
        const sdBarMini = sdBar.clone().removeClass('sidebar').addClass('sidebar-mini').removeAttr('id');
        const imgBrand = $('.sidebar-brand-image', sdBarMini);
        if (imgBrand.length) {
            const imgMiniUrl = sdBarMini.data('mini-brand-image');
            if (imgMiniUrl) {
                imgBrand.prop({src: imgMiniUrl});
            }
        }

        $.each(sdBarMini.children('.sidebar-menu'), function(k, v) {
            const sbm = $(v).removeAttr('id');
            $.each(sbm.children('.sidebar-item'), function(k, v) {
                clearMiniSidebarLink(sdBarMini, $(v), 0);
            });
        });

        sdBar.after(sdBarMini.removeAttr('data-mini-brand-image').removeAttr('data-mini-sub-theme'));
    }
    
    $('.sidebar-toggler, .sidebar-backdrop').on('click', function() {
        $('body').toggleClass('sidebar-toggled');
    });

    $('.sidebar-link').each(function(k) {
        const a = $(this);
        const i = a.children('.sidebar-icon');
        if (i.length > 0) {
            a.css({'padding-left': 0});
        }
    });

    $('.sidebar-mini .sidebar-item').each(function(k) {
        const m = $(this);
        const s = m.children('.sidebar-menu');
        if (s.length > 0) {
            const a = m.children('.sidebar-link');
            const w = Math.ceil(s.width()) + 30;
            m.css({'--sidebar-menu-width': w + 'px'});
            s.css({width: 0, padding: 0});
            a.on('click', function(e) {
                $('.sidebar-mini .sidebar-link.open').not(a).removeClass('open');
                $('.sidebar-mini .sidebar-menu.show').not(s).removeClass('show');
                s.toggleClass('show');
                if (s.hasClass('show')) {
                    $(this).addClass('open');
                } else {
                    $(this).removeClass('open');
                }
                return false;
            });
        }
    });
    
    $(document).on('click', function(e) {
        const sp = $(e.target).closest('.sidebar-item');
        if (sp.length == 0) {
            $('.sidebar-mini .sidebar-menu.show').removeClass('show');
            $('.sidebar-mini .sidebar-link.open').removeClass('open');
        }
    });
});