<form class="modal-content" method="POST" action="{{ route('bonus.saveTransfer') }}" data-alert-container="#alert-from-container" data-onfocus="#account_no">
    @include('partials.modals.modal-header', ['modalTitle' => 'Konfirmasi Transfer'])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        <input type="hidden" name="bonus_type" value="{{ $bonusType }}">
        @foreach ($checks as $check)
            <input type="hidden" name="checks[]" value="{{ $check }}">
        @endforeach
        <div class="d-block">
            Benarkah data yang dipilih adalah data yang sudah ditransfer ?
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-check"></i>
            Ya
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Tidak
        </button>
    </div>
</form>