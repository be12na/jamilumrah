@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Ujroh', 'Admin Cabang'],
    'docTitle' => 'Ujroh'
])

@section('content')
@include('partials.alert')
<div class="d-block small">
    <table class="table table-nowrap table-hover table-striped" id="table">
        <thead class="text-center align-middle bg-gray-100 bg-gradient">
            <tr>
                @if ($user->main_user)
                <th class="border-end d-none">
                    <input type="checkbox" class="mx-3" id="check-all" title="Pilih Semua" autocomplete="off" onclick="selectCheck($(this));">
                </th>
                @endif
                <th class="border-end">Tanggal</th>
                @if ($user->main_user)
                <th class="border-end">Nama Cabang</th>
                @endif
                <th class="border-end">Jamaah</th>
                <th class="border-end">Ujroh</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="col-auto d-none" id="button-control">
    @if ($user->main_user)
        <div class="btn-group me-1 d-none" id="btn-action">
            <button type="button" id="btn-submit-check" class="btn btn-sm btn-primary" title="Submit">
                <i class="fa-solid fa-save me-1" style="line-height: inherit"></i>
                Submit
            </button>
            <button type="button" id="btn-cancel" class="btn btn-sm btn-warning" title="Batal" onclick="transferAction('hide');">
                <i class="fa-solid fa-times me-1" style="line-height: inherit"></i>
                Batal
            </button>
        </div>
        <div class="btn-group me-1 d-none" id="btn-transfer">
            <button type="button" class="btn btn-sm btn-glow" title="Transfer" onclick="transferAction('show');">
                Transfer
            </button>
        </div>
    @endif
    <div class="btn-group">
        <button type="button" class="btn btn-sm btn-glow" title="Refresh" onclick="refreshTable();">
            <i class="fa-solid fa-rotate" style="line-height: inherit"></i>
        </button>
    </div>
</div>
<div class="col-auto d-none" id="filter-control">
    <div class="input-group input-group-sm">
        <label for="filter-status" class="input-group-text">Status</label>
        <select id="filter-status" class="form-select">
            <option value="-1">-- Semua --</option>
            @foreach (BONUS_STATUS_LIST as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection

@if ($user->main_user)
@push('topContent')
    @include('partials.modals.modal', [
        'bsModalId' => 'modal-transfer',
        'scrollable' => true,
    ]);
@endpush
@endif

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    let table;

    @if ($user->main_user)
    let showHide = 'hide';

    function showHideCheck() {
        const th = $('table.dataTable thead > tr > :first-child');
        const td = $('table.dataTable tbody > tr > :first-child:not([colspan])');
        const b1 = $('#btn-action');
        const b2 = $('#btn-transfer');
        if (showHide === 'show') {
            th.removeClass('d-none');
            td.removeClass('d-none');
            b1.removeClass('d-none');
            b2.addClass('d-none');
        } else if (showHide === 'hide') {
            th.addClass('d-none');
            td.addClass('d-none');
            b1.addClass('d-none');
            b2.removeClass('d-none');
        }
    }

    function selectCheck(ch)
    {
        const ca = $('#check-all');
        const tc = $('table.dataTable tbody .check-row');
        if (!ch.hasClass('check-row')) {
            tc.prop({checked: ca.is(':checked')}).trigger('change');
        } else {
            const checked = $('table.dataTable tbody .check-row:checked');
            ca.prop({checked: (checked.length == tc.length)});
        }
    }

    function transferAction(am)
    {
        showHide = am;
        showHideCheck();
    }
    @endif

    function refreshTable()
    {
        table.ajax.reload();
    }

    $(function() {
        table = $('#table').DataTable({
            dom: '<"row justify-content-between g-2 mb-2"<"col-auto"<"#dt-control.row g-2"<"col-auto"l>>><"col-auto"f>>r<"table-responsive border mb-3"t><"datatable-paging-info row g-2"<"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-start"i><"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-end"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("bonus.dataTable") }}',
                data: function(d) {
                    d.bonus_type = 'branch',
                    d.bonus_status = $('#filter-status').val()
                }
            },
            deferLoading: 50,
            search: {
                return: true
            },
            pagingType: 'full_numbers',
            lengthChange: false,
            language: {!! json_encode(__('datatable')) !!},
            order: [@if ($user->main_user) 1 @else 0 @endif, 'asc'],
            columns: [
                @if ($user->main_user)
                {data: 'check', className: 'dt-body-center', orderable: false, searchable: false},
                @endif
                {data: 'bonus_date'},
                @if ($user->main_user)
                {data: 'branch_name'},
                @endif
                {data: 'customer_name'},
                {data: 'bonus', className: 'dt-body-right'},
                {data: 'status', className: 'dt-body-center', orderable: false, searchable: false},
            ],
            @if ($user->main_user)
            drawCallback: function( settings ) {
                showHideCheck();
                const ca = $('#check-all');
                ca.prop({checked: false});
                selectCheck(ca);
            },
            @endif
        });

        @if ($user->main_user)
        $(document).on('click', 'table.dataTable tbody .check-row', function(e) {
            selectCheck($(this));
        }).on('change', 'table.dataTable tbody .check-row', function(e) {
            const me = $(this);
            const tr = me.closest('tr');
            if (me.is(':checked')) {
                tr.addClass('selected');
            } else {
                tr.removeClass('selected');
            }
        });

        $('#btn-submit-check').on('click', function(e) {
            const checks = $('table.dataTable tbody .check-row:checked');
            let arrCheck = [];
            $.each(checks, function(k, v) {
                arrCheck.push($(v).val());
            });

            const mdl = $('#modal-transfer');
            mdl.modal('show');

            const p = arrCheck.length ? '&ids=' + arrCheck.join(',') : '';
            let url = '{{ route("bonus.viewTransfer") }}?bonus_type=branch' + p;
            
            renderUrlModal($('.modal-dialog', mdl), url);
        });
        @endif

        $('#table_processing.dataTables_processing')
        .removeClass('card')
        .html('<div><i class="fa-solid fa-spinner fa-spin fs-3"></i><span class="text-center fa-beat-fade mt-2">Processing...</span></div>');

        const fc = $('#button-control');
        $('#dt-control').prepend(fc);
        fc.removeClass('d-none').after($('#filter-control').removeClass('d-none'));

        $('#filter-status').on('change', function(e) {
            refreshTable();
        }).change();
    });
</script>
@endpush
