<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Umroh Family') }}</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png">
    @if (isLive())
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/landing.css') }}">
</head>
<body>
    <div class="d-flex align-items-center landing-banner mb-3 mb-md-4 mb-lg-5" style="--landing-bg-banner:url({{ asset('images/bg-image.png'); }})">
        <div class="d-block w-100 text-center text-light user-select-none lh-1">
            <div class="text-1 mb-3">{{ config('app.name') }}</div>
            <div class="text-2 mb-3">Easy Way to Baitullah</div>
            <div>
                <a href="#detail" class="btn btn-secondary px-3">Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container mb-4">
        <div class="mb-3 mb-md-4 mb-lg-5" id="detail">
            <div class="row g-3">
                <div class="col-lg-6 col-xl-8">
                    <div class="mb-3 fs-1">Ezkia Barokah Abadi</div>
                    <div class="mb-3 fs-5">Travel Perjalanan Umrah Terbaik Di Indonesia</div>
                    <div class="mb-3">
                        Ezkia Barokah Abadi merupakan perusahaan yang bergerak di bidang Travel dan penyelenggaraan Umrah dan Haji di Indonesia. Kami berkomitmen untuk memberikan berbagai macam kemudahan kepada calon jama'ah agar dapat berangkat ke Tanah Suci. Dimulai dari pendaftaran, pilihan pembayaran sampai persiapan keberangkatan umrah maupun haji.
                    </div>
                    <div class="mb-3">
                        Seiring perkembangannya, Ezkia Barokah Abadi akan selalu meningkatkan kualitas pelayanan yang diberikan kepada jama'ah agar jama'ah selalu khusu saat melakukan ibadah.
                    </div>
                    <div>
                        Sahabat Terbaik Menuju Baitullah, Aman Dan Terpercaya Untuk Keluarga Anda
                    </div>
                </div>
                <div class="col-lg-6 col-xl-4 d-flex align-items-center justify-content-center">
                    <img src="{{ asset('images/img-umroh.png') }}" style="width: 90%; height: auto;">
                </div>
            </div>
        </div>
        <div class="mb-3 mb-md-4 mb-lg-5 text-center">
            <div class="mb-3 mb-md-4 fs-4 py-3">
                <div>Mengapa Kami Adalah Pilihan Terbaik Bagi Anda?</div>
                <div>Karena Kami Mampu Memberikan Solusi Terhadap Apapun Kendala Anda</div>
            </div>
        </div>
        <div class="row gx-2 gx-md-3 gy-5 mb-4">
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="flex-fill box-info bg-light rounded-3">
                    <i class="far fa-thumbs-up icon"></i>
                    <div class="box-title">Di Jamin Berangkat</div>
                    <div>Alhamdulillah sampai saat ini 100% keberangkatan.</div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="flex-fill box-info bg-light rounded-3">
                    <i class="far fa-gem icon"></i>
                    <div class="box-title">Biaya Terjangkau</div>
                    <div>Terjangkau semua kalangan & no hidden fee, dengan tetap mengutamakan kualitas pelayanan & kenyamanan.</div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="flex-fill box-info bg-light rounded-3">
                    <i class="far fa-map icon"></i>
                    <div class="box-title">Pembimbing Profesional</div>
                    <div>Pembimbing dan Muthawwif profesional. Untuk kekhusyuan ibadah umrah Anda.</div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="flex-fill box-info bg-light rounded-3">
                    <i class="far fa-check-square icon"></i>
                    <div class="box-title">Perizinan Lengkap</div>
                    <div>Kami telah melewati semua tahapan perizinan resmi sesuai dengan peraturan dan undang-undang yang berlaku.</div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="flex-fill box-info bg-light rounded-3">
                    <i class="fa fa-umbrella icon"></i>
                    <div class="box-title">Aman & Terpercaya</div>
                    <div>Program dan Skema Pembayaran kami adalah cara paling aman dan nyaman bagi Jamaah saat ini. menjawab keresahan masyarakat akan hadirnya Biro travel Umrah yang amanah dan terpercaya.</div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="flex-fill box-info bg-light rounded-3">
                    <i class="fa fa-book-reader icon"></i>
                    <div class="box-title">Sesuai Syariat</div>
                    <div>Alhamdulillah, semua Program dan Skema Pembayaran kami juga telah sesuai dengan prinsip-prinsip syar'i berlandaskan pada fatwa-fatwa DSN_MUI.</div>
                </div>
            </div>
        </div>
        <div class="mb-5 fs-2 text-center tanpa">
            <span>Tanpa Riba</span>
            <span class="tanpa-separator"></span>
            <span>Tanpa Agunan</span>
            <span class="tanpa-separator"></span>
            <span>Tanpa Denda</span>
        </div>
        <div class="btn-group w-100 dual-button">
            <a class="btn btn-lg btn-dark" href="{{ $urlRegister }}">
                <i class="far fa-registered small"></i>
                Register
            </a>
            <div class="dual-separator">OR</div>
            <a class="btn btn-lg btn-success" href="{{ route('login') }}">
                <i class="far fa-hand-point-right small"></i>
                Login
            </a>
        </div>
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>