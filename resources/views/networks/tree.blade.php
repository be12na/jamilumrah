@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Jaringan', 'Struktur Pohon'],
    'docTitle' => 'Jaringan'
])

@section('content')
<div class="trees-container border">
    <div class="trees">
        {!! $treeStructure !!}
    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('css/tree-structure.css') }}">
@endpush
