@php
    $user = isset($user) ? $user : Auth::user();
    $branch = $user->branch;
    $admin = $user->branch_user_admin ? $user : $branch->admin;
    $link = $admin->referral_link_url;
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Dashboard'],
    'docTitle' => 'Dashboard'
])

@section('content')
@include('partials.alert')
<div class="row g-2 g-md-3 mb-2 mb-md-3">
    <div class="col-lg-4 d-flex">
        <div class="flex-fill d-flex p-3 border rounded-3 bg-light bg-gradient shadow-sm">
            <div class="flex-fill row g-2">
                <div class="col-sm-5 col-md-4 col-lg-3 d-flex align-items-center justify-content-center justify-content-sm-start">
                    <span class="fa-solid fa-city fs-1"></span>
                </div>
                <div class="col-sm-7 col-md-8 col-lg-9 d-flex align-items-center">
                    <div class="flex-fill row g-2">
                        <div class="col-12 text-center text-sm-end text-lg-start fw-bold">Kantor Perwakilan</div>
                        <div class="col-12 text-center text-sm-end text-lg-start">{{ $branch->name }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 d-flex">
        <div class="flex-fill d-flex p-3 border rounded-3 bg-light bg-gradient shadow-sm">
            <div class="flex-fill row g-2">
                <div class="col-md-2 d-flex align-items-center justify-content-center justify-content-md-start">
                    <span class="fa-solid fa-share-nodes fs-1"></span>
                </div>
                <div class="col-md-10 d-flex align-items-center">
                    @include('partials.share-link', [
                        'mode' => 'html',
                        'link' => $link
                    ])
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row g-2 g-md-3 mb-3">
    <div class="col-md-6 d-flex bg-white">
        <div class="flex-fill rounded-3 bg-gradient-lighten bg-primary text-light p-3 d-flex flex-column justify-content-between shadow-sm">
            <div class="d-flex flex-nowrap justify-content-between align-items-end flex-shrink-0 pb-2 border-bottom mb-3" style="--bs-border-color:var(--bs-gray-500);">
                <i class="fa-solid fa-users fs-1"></i>
                <span class="fw-bold text-end lh-1">Jamaah</span>
            </div>
            <div class="d-block">
                <div class="row g-1 mb-2">
                    <div class="col-3 text-center">
                        <div>Transfer</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=transfer">0</div>
                    </div>
                    <div class="col-3 text-center">
                        <div>Baru</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=new">0</div>
                    </div>
                    <div class="col-3 text-center">
                        <div>Aktif</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=active">0</div>
                    </div>
                    <div class="col-3 text-center">
                        <div>Jumlah</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=total">0</div>
                    </div>
                </div>
                <div class="row g-1">
                    <div class="col-3">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('customer.index') }}?filter={{ CUSTOMER_STATUS_TRANSFER }}">Lihat >>></button>
                    </div>
                    <div class="col-3">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('customer.index') }}?filter={{ CUSTOMER_STATUS_REGISTER }}">Lihat >>></button>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('customer.index') }}?filter={{ CUSTOMER_STATUS_APPROVED }}">Lihat >>></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 d-flex bg-white">
        <div class="flex-fill rounded-3 bg-gradient-lighten bg-success text-light p-3 d-flex flex-column justify-content-between shadow-sm">
            <div class="d-flex flex-nowrap justify-content-between align-items-end flex-shrink-0 pb-2 border-bottom mb-3" style="--bs-border-color:var(--bs-gray-500);">
                <i class="fa-solid fa-coins fs-1"></i>
                <span class="fw-bold text-end lh-1">Ujroh</span>
            </div>
            <div class="d-block text-center">
                <div class="row g-1 mb-2">
                    <div class="col-4">
                        <div>Pending</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=bonus&scope=pending">0</div>
                    </div>
                    <div class="col-4">
                        <div>Diterima</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=bonus&scope=transferred">0</div>
                    </div>
                    <div class="col-4">
                        <div>Total</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=bonus&scope=total">0</div>
                    </div>
                </div>
                <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('bonus.index') }}">Lihat >>></button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    @include('partials.share-link', [
        'mode' => 'style',
        'link' => $link
    ])
@endpush

@push('scripts')
    @include('partials.share-link', [
        'mode' => 'script',
        'link' => $link
    ])
@endpush
