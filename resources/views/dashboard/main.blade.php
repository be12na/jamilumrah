@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Dashboard'],
    'docTitle' => 'Dashboard'
])

@section('content')
@include('partials.alert')
<div class="row g-2 mb-3">
    <div class="col-sm-6 col-md-4 col-lg-3 d-flex bg-white">
        <div class="flex-fill rounded-3 bg-gradient-lighten bg-primary text-light p-3 d-flex flex-column justify-content-between shadow-sm">
            <div class="d-flex flex-nowrap justify-content-between align-items-end flex-shrink-0 pb-2 border-bottom mb-3" style="--bs-border-color:var(--bs-gray-500);">
                <i class="fa-solid fa-code-branch fs-1"></i>
                <span class="fw-bold text-end lh-1">Cabang Perwakilan</span>
            </div>
            <div class="d-block text-center">
                <div class="row g-1 mb-2">
                    <div class="col-6">
                        <div>Aktif</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=branch&scope=active">0</div>
                    </div>
                    <div class="col-6">
                        <div>Jumlah</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=branch&scope=total">0</div>
                    </div>
                </div>
                <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('branch.index') }}">Lihat >>></button>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3 d-flex bg-white">
        <div class="flex-fill rounded-3 bg-gradient-lighten bg-success text-light p-3 d-flex flex-column justify-content-between shadow-sm">
            <div class="d-flex flex-nowrap justify-content-between align-items-end flex-shrink-0 pb-2 border-bottom mb-3" style="--bs-border-color:var(--bs-gray-500);">
                <i class="fa-solid fa-users-line fs-1"></i>
                <span class="fw-bold text-end lh-1">Agen</span>
            </div>
            <div class="d-block text-center">
                <div class="row g-1 mb-2">
                    <div class="col-4">
                        <div>Aktifasi</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=agency&scope=activation">0</div>
                    </div>
                    <div class="col-4 text-center">
                        <div>Aktif</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=agency&scope=active">0</div>
                    </div>
                    <div class="col-4 text-center">
                        <div>Jumlah</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=agency&scope=total">0</div>
                    </div>
                </div>
                <div class="row g-1">
                    <div class="col-4">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('agency.activation') }}">Lihat >>></button>
                    </div>
                    <div class="col-8">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('agency.index') }}">Lihat >>></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-lg-6 d-flex bg-white">
        <div class="flex-fill rounded-3 bg-gradient-lighten bg-info p-3 d-flex flex-column justify-content-between shadow-sm">
            <div class="d-flex flex-nowrap justify-content-between align-items-end flex-shrink-0 pb-2 border-bottom mb-3" style="--bs-border-color:var(--bs-gray-600);">
                <i class="fa-solid fa-users fs-1"></i>
                <span class="fw-bold text-end lh-1">Jamaah</span>
            </div>
            <div class="d-block">
                <div class="row g-1 mb-2">
                    <div class="col-3 text-center">
                        <div>Transfer</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=transfer">0</div>
                    </div>
                    <div class="col-3 text-center">
                        <div>Baru</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=new">0</div>
                    </div>
                    <div class="col-3 text-center">
                        <div>Aktif</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=active">0</div>
                    </div>
                    <div class="col-3 text-center">
                        <div>Jumlah</div>
                        <div class="autoload-container" data-autoload-url="{{ route('dashboard.data') }}?data=customer&scope=total">0</div>
                    </div>
                </div>
                <div class="row g-1 text-light">
                    <div class="col-3">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('customer.index') }}?filter={{ CUSTOMER_STATUS_TRANSFER }}">Lihat >>></button>
                    </div>
                    <div class="col-3">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('customer.index') }}?filter={{ CUSTOMER_STATUS_REGISTER }}">Lihat >>></button>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-xs btn-shortcut" data-href="{{ route('customer.index') }}?filter={{ CUSTOMER_STATUS_APPROVED }}">Lihat >>></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<style>
    .btn-shortcut {
        --bs-btn-color: var(--bs-gray-100);
        --bs-btn-bg: rgba(var(--bs-dark-rgb), 0.5);
        --bs-btn-hover-bg: rgba(var(--bs-dark-rgb), 0.6);
        --bs-btn-active-bg: rgba(var(--bs-dark-rgb), 0.7);
        --bs-btn-border-color: rgba(var(--bs-dark-rgb), 0.5);
        --bs-btn-hover-border-color: rgba(var(--bs-dark-rgb), 0.6);
        --bs-btn-active-border-color: rgba(var(--bs-dark-rgb), 0.7);
        display: block;
        width: 100%;
    }
</style>
@endpush
