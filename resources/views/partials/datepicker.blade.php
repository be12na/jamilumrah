@php
    $isPlugin = isset($isPlugin) ? ($isPlugin === true) : false;
    $isJs = ($isPlugin && (isset($isJs) ? ($isJs === true) : false));
    $isCss = ($isPlugin && !$isJs);
@endphp
@if (!$isPlugin)
    @php
        $format = 'd F Y';
        $dateValue = isset($dateValue) ? translateDatetime($dateValue, $format) : '';
        $endDate = (isset($endDate) && !empty($endDate)) ? translateDatetime($endDate, $format) : '';
        $inputDateId = isset($dateId) ? $dateId : 'date-' . mt_rand(0, 999999);
        $inputDateName = isset($dateName) ? $dateName : '';
        // $datePickerFormat = isset($datePickerFormat) ? $datePickerFormat : 'd MM yyyy';
        $datePickerFormat = isset($datePickerFormat) ? $datePickerFormat : 'd MM yy';
        $inputRequired = (isset($required) && ($required === true)) ? 'required' : '';
        $inputPlaceholder = isset($placeholder) ? $placeholder : 'Tanggal';
        // tambahan
        $yearRange = implode(':', [1930, date('Y')]);
    @endphp

    <div class="input-group">
        <input type="text" id="{{ $inputDateId }}" @if($inputDateName != '') name="{{ $inputDateName }}" @endif class="form-control bg-white bs-date" value="{{ $dateValue }}" data-date-format="{{ $datePickerFormat }}" data-date-end-date="{{ $endDate }}" data-date-year-range="{{ $yearRange }}" placeholder="{{ $inputPlaceholder }}" autocomplete="off" readonly {{ $inputRequired }}>
        <label for="{{ $inputDateId }}" class="input-group-text cursor-pointer">
            <i class="fa-solid fa-calendar-days"></i>
        </label>
    </div>
@else
    @if ($isJs)
        <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-ui/ui/i18n/datepicker-id.js') }}"></script>
    @else
        <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}">
    @endif
@endif