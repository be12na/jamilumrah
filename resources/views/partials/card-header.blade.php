<div class="card-header bg-white bg-gradient-lighten {{ isset($headerClass) ? $headerClass : '' }}">
    <img src="{{ asset('images/logo.png') }}" alt="Logo" style="height: 16px;">
    <span class="ms-2 fw-bold">{{ $cardTitle }}</span>
</div>