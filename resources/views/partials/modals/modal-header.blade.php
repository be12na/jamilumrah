<div class="modal-header bg-white bg-gradient-lighten py-2 px-3">
    <img src="{{ asset('images/logo.png') }}" alt="Logo" style="height: 16px;">
    <span class="ms-2 fw-bold small">{{ $modalTitle }}</span>
    <button type="button" class="btn-close lh-1" data-bs-dismiss="modal" style="background: none; font-size:12px;">
        <i class="fa-solid fa-times"></i>
    </button>
</div>