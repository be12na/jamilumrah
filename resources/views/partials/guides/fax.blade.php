<li>
    <div>Pengisian input <span class="fw-bold">Fax</span> dapat menggunakan format: (<span class="fw-bold">{kode area}</span>) xxxxxxxxxx atau <span class="fw-bold">{kode area}</span>-xxxxxxxxxx.</div>
    <div>Contoh: <span class="fw-bold">(021)</span> xxxxxxxxxx, <span class="fw-bold">021</span>-xxxxxxxxxx.</div>
</li>