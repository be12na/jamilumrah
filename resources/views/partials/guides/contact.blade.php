<li>
    <div>Pengisian input <span class="fw-bold">Telepon / Kontak</span> dapat menggunakan format:</div>
    <div>Handphone: +628xxxxxxxxxx atau 08xxxxxxxxxx.</div>
    <div>Telepon: (<span class="fw-bold">{kode area}</span>) xxxxxxxxxx atau <span class="fw-bold">{kode area}</span>-xxxxxxxxxx.</div>
    <div>Contoh: <span class="fw-bold">+628</span>12xxxxxxxx, <span class="fw-bold">08</span>12xxxxxxxx, <span class="fw-bold">(021)</span> xxxxxxxxxx, <span class="fw-bold">021</span>-xxxxxxxxxx.</div>
</li>