@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => array_merge(['Daftar Agen'], ($register == true) ? ['Aktifasi'] : []),
    'docTitle' => 'Daftar Agen'
])

@section('content')
@include('partials.alert')
<input type="hidden" id="is-register" value="{{ $register ? 1 : 0 }}">
<div class="d-block small">
    <table class="table table-sm table-nowrap table-hover table-striped" id="table">
        <thead class="text-center align-middle bg-gray-100 bg-gradient">
            <tr>
                <th>Username</th>
                <th class="border-start">Nama</th>
                <th class="border-start">Email</th>
                <th class="border-start">Handphone</th>
                <th class="border-start">Referral</th>
                <th class="border-start"></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="btn-group me-2 d-none" id="button-control">
    <button type="button" class="btn btn-sm btn-glow" title="Refresh" onclick="refreshTable();">
        <i class="fa-solid fa-rotate" style="line-height: inherit"></i>
    </button>
</div>
@endsection

@push('topContent')
@include('partials.modals.modal', ['bsModalId' => 'agencyModal', 'scrollable' => true])
@endpush

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    let table;

    function refreshTable()
    {
        table.ajax.reload();
    }

    $(function() {
        table = $('#table').DataTable({
            dom: '<"row g-2 justify-content-between mb-2"<"#dt-control.col-auto d-flex align-items-end flex-wrap"l><"col-auto d-flex align-items-end mw-100"f>>r<"table-responsive border mb-3"t><"datatable-paging-info row g-2"<"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-start"i><"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-end"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("agency.dataTable") }}',
                data: function(d) {
                    d.register = $('#is-register').val()
                }
            },
            deferLoading: 50,
            search: {
                return: true
            },
            pagingType: 'full_numbers',
            lengthChange: false,
            language: {!! json_encode(__('datatable')) !!},
            columnDefs: [
                {orderable: false, searchable: false, targets: [5]},
            ],
            columns: [
                {data: 'username'},
                {data: 'name'},
                {data: 'email'},
                {data: 'phone'},
                {data: 'referral_name'},
                {data: 'action'},
            ],
        });

        $('#table_processing.dataTables_processing')
        .removeClass('card')
        .html('<div><i class="fa-solid fa-spinner fa-spin fs-3"></i><span class="text-center fa-beat-fade mt-2">Processing...</span></div>');

        $('#dt-control').prepend($('#button-control').removeClass('d-none'));

        refreshTable();
    });
</script>
@endpush