@php
    $user = isset($user) ? $user : Auth::user();
    $register = $data->is_register_agent;
@endphp

<div class="modal-content">
    <div class="modal-header py-2 px-3">
        <span class="fw-bold small">Detail Agen</span>
        <button type="button" class="btn-close lh-1" data-bs-dismiss="modal" style="background: none; font-size:12px;">
            <i class="fa-solid fa-times"></i>
        </button>
    </div>
    <div class="modal-body small">
        <div class="row g-2" style="--label-basis: 100px;">
            @if ($data->referral)
                <div class="col-12 d-flex flex-wrap justify-content-between">
                    <div class="fw-bold" style="flex-basis: var(--label-basis);">Referral:</div>
                    <div class="ms-3">{{ $data->referral->name }}</div>
                </div>
            @endif
            <div class="col-12 d-flex flex-wrap justify-content-between">
                <div class="fw-bold" style="flex-basis: var(--label-basis);">Username:</div>
                <div class="ms-3">{{ $data->username }}</div>
            </div>
            <div class="col-12 d-flex flex-wrap justify-content-between">
                <div class="fw-bold" style="flex-basis: var(--label-basis);">Nama:</div>
                <div class="ms-3">{{ $data->name }}</div>
            </div>
            <div class="col-12 d-flex flex-wrap justify-content-between">
                <div class="fw-bold" style="flex-basis: var(--label-basis);">Email:</div>
                <div class="ms-3">{{ $data->email }}</div>
            </div>
            <div class="col-12 d-flex flex-wrap justify-content-between">
                <div class="fw-bold" style="flex-basis: var(--label-basis);">Handphone:</div>
                <div class="ms-3">{{ $data->phone ?? '-' }}</div>
            </div>
        </div>
    </div>
    <div class="modal-footer justify-content-between py-1">
        <div>
            @if ($register)
                @if ($user->main_user_super || $user->main_user_master)
                    <button type="button" class="btn btn-sm btn-primary reload-modal" data-modal-url="{{ route('agency.approve', ['mainAgent' => $data->id, 'agentRegister' => 1]) }}" >
                        <i class="fa-solid fa-handshake"></i>
                        Terima
                    </button>
                    <button type="button" class="btn btn-sm btn-danger reload-modal" data-modal-url="{{ route('agency.reject', ['mainAgent' => $data->id, 'agentRegister' => 1]) }}">
                        <i class="fa-solid fa-handshake-simple-slash"></i>
                        Tolak
                    </button>
                @endif
            @else
                @if ($user->main_user_super)
                    <button type="button" class="btn btn-sm btn-primary reload-modal" data-modal-url="{{ route('agency.editAuth', ['mainAgent' => $data->id]) }}">
                        <i class="fa-solid fa-user-pen"></i>
                        Edit User
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" data-href="{{ route('agency.deactivate.index', ['mainAgent' => $data->id]) }}">
                        <i class="fa-solid fa-user-times"></i>
                        Nonaktifkan
                    </button>
                @endif
            @endif
        </div>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Tutup
        </button>
    </div>
</div>