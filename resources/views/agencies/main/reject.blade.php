<form class="modal-content" method="POST" action="{{ route('agency.reject', ['mainAgent' => $data->id, 'agentRegister' => 1]) }}" data-alert-container="#alert-from-container">
    <div class="modal-header py-2 px-3">
        <span class="fw-bold small">Detail Agen</span>
        <button type="button" class="btn-close lh-1" data-bs-dismiss="modal" style="background: none; font-size:12px;">
            <i class="fa-solid fa-times"></i>
        </button>
    </div>
    <div class="modal-body">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        <h4>Tolak Anggota tersebut sebagai agen ?</h4>
        <div class="d-block">
            <label for="note">Keterangan</label>
            <textarea name="rejected_note" class="form-control" maxlength="250"></textarea>
        </div>
    </div>
    <div class="modal-footer justify-content-between py-1">
        <button type="submit" class="btn btn-sm btn-danger">
            <i class="fa-solid fa-handshake-simple-slash"></i>
            Tolak
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Tutup
        </button>
    </div>
</form>