<form class="modal-content" method="POST" action="{{ route('agency.editAuth', ['mainAgent' => $data->id]) }}" data-alert-container="#alert-auth-container" data-onfocus="#modal-username">
    <div class="modal-header py-2 px-3">
        <span class="fw-bold small">Autentikasi Agen</span>
        <button type="button" class="btn-close lh-1" data-bs-dismiss="modal" style="background: none; font-size:12px;">
            <i class="fa-solid fa-times"></i>
        </button>
    </div>
    <div class="modal-body">
        @csrf
        <div class="d-block" id="alert-auth-container"></div>
        <div class="d-block mb-3">
            <label class="d-block required">Username</label>
            <input type="text" class="form-control" name="username" id="modal-username" value="{{ $data->username }}" placeholder="Username" autocomplete="off" required>
        </div>
        <div class="d-block mb-3">
            <label class="d-block">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
        </div>
        <div class="d-block">
            <label class="d-block">Ketik Ulang Password</label>
            <input type="password" class="form-control" name="password_confirmation" placeholder="Ketik Ulang Password" autocomplete="off">
        </div>
    </div>
    <div class="modal-footer justify-content-between py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-save"></i>
            Simpan
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Tutup
        </button>
    </div>
</form>