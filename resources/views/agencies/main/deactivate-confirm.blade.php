@php
    $hasReferral = !empty($referral);
@endphp

<form class="modal-content" method="POST" action="{{ route('agency.deactivate.update', ['mainAgent' => $agency->id]) }}" data-alert-container="#alert-from-container">
    @include('partials.modals.modal-header', ['modalTitle' => 'Konfirmasi'])
    <div class="modal-body fs-auto">
        @if ($hasReferral)
            @csrf
            <input type="hidden" name="new_referral" value="{{ $referral->id }}">
            <div class="d-block" id="alert-from-container"></div>
            <div class="row g-1 mb-3">
                <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                    <span class="text-wrap me-1">Username</span><span>:</span>
                </div>
                <div class="col-7 col-lg-8">{{ $agency->username }}</div>
                <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                    <span class="text-wrap me-1">Nama</span><span>:</span>
                </div>
                <div class="col-7 col-lg-8"><strong>{{ $agency->name }}</strong></div>
                <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                    <span class="text-wrap me-1">Referral</span><span>:</span>
                </div>
                <div class="col-7 col-lg-8"><strong>{{ $agency->referral->name }}</strong> ({{ $agency->referral->username }})</div>
            </div>
            <div class="row g-1 mb-3">
                <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                    <span class="text-wrap me-1">Referral Level 1</span><span>:</span>
                </div>
                <div class="col-7 col-lg-8"><strong>{{ $referral->name }}</strong> ({{ $referral->username }})</div>
            </div>
            <div class="text-center pt-2 border-top fw-bold text-danger">
                <div>Yakin untuk menonaktifkan agen tersebut ?</div>
            </div>
        @else
            <div class="text-center text-danger fw-bold">Referral baru harus dipilih.</div>
        @endif
    </div>
    <div class="modal-footer justify-content-center py-1">
        @if ($hasReferral)
            <button type="submit" class="btn btn-sm btn-danger">
                <i class="fa-solid fa-user-times"></i>
                Nonaktifkan
            </button>
        @endif
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Batal
        </button>
    </div>
</form>