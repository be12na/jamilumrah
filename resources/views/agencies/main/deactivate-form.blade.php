@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => array_merge(['Agen', 'Penonaktifan']),
    'docTitle' => 'Penonaktifan Agen'
])

@section('content')
@include('partials.alert')
<input type="hidden" name="agency_id" value="{{ $agency->id }}">
<div class="card fs-auto" style="--bs-card-spacer-y:0.5rem; --bs-card-spacer-x:0.5rem; --bs-card-cap-padding-x: var(--bs-card-spacer-x);">
    @include('partials.card-header', ['cardTitle' => 'Opsi Penonaktifan Agen'])
    <div class="card-body">
        <div class="row g-3">
            <div class="col-lg-6">
                <div class="fw-bold mb-2 pb-1 border-bottom">Data Agen</div>
                <div class="row g-1">
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Username</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $agency->username }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Nama</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8"><strong>{{ $agency->name }}</strong></div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Email</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $agency->email }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Handphone</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $agency->phone }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Referral</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8"><strong>{{ $agency->referral->name }}</strong> ({{ $agency->referral->username }})</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Jumlah Member</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">@formatNumber($agency->structure->countDescendants())</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Total Ujroh</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">@formatNumber($agency->summary_bonus->total, 0)</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Ujroh Ditransfer</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">@formatNumber($agency->summary_bonus->transferred, 0)</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Sisa Ujroh</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">@formatNumber($agency->summary_bonus->total - $agency->summary_bonus->transferred, 0)</div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="fw-bold mb-2 pb-1 border-bottom">Options</div>
                <div class="mb-3">
                    <div>Semua agen/member level ke-1 dari <strong>{{ $agency->name }}</strong> akan dipindahkan ke agen referral yang dipilih.</div>
                    <div>Silahkan pilih agen referral baru yang akan menjadi referral bagi agen/member level ke-1 dari <strong>{{ $agency->name }}</strong>.</div>
                </div>
                <div class="mb-3">
                    <label class="d-block required">Referral Level 1</label>
                    <select class="form-select select2bs4 select2-custom" name="new_referral" id="new-referral" autocomplete="off" required></select>
                </div>
                <div class="fw-bold mb-2 pb-1 border-bottom">Perhatian!!!</div>
                <div>Semua bonus yang belum ditransfer akan beralih ke referral yang dipilih.</div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modal-admin">
            <i class="fa-solid fa-user-times me-1"></i>
            Nonaktifkan
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-href="{{ route('agency.index') }}">
            <i class="fa-solid fa-undo me-1"></i>
            Batal
        </button>
    </div>
</div>
@endsection

@push('topContent')
@include('partials.modals.modal', ['bsModalId' => 'modal-admin', 'scrollable' => true])
@endpush

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/i18n/id.js') }}"></script>
@endpush

@push('scripts')
<script>
    let select_referral;
    
    $(function() {
        select_referral = $('#new-referral');
        select_referral.select2({
            theme: 'classic',
            placeholder: '-- Pilih Referral --',
            allowClear: true,
            ajax: {
                url: function(params) {
                    params.current = select_referral.val();
                    
                    return '{{ route("agency.deactivate.select2", ["mainAgent" => $agency->id]) }}';
                },
                data: function (params) {
                    let dt = {
                        search: params.term,
                        current: params.current
                    };

                    return dt;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });

        $('#modal-admin').on('show.bs.modal', function(e) {
            renderUrlModal(
                $('.modal-dialog', $(this)), 
                '{{ route("agency.deactivate.confirm", ["mainAgent" => $agency->id]) }}', 
                {new_referral: select_referral.val()}
            );
        });
    });
</script>
@endpush
