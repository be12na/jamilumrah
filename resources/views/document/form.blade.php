<form class="modal-content" method="POST" action="{{ route('document.saveUpload') }}" data-alert-container="#alert-from-container" data-onfocus="#input-name" enctype="multipart/form-data">
    @include('partials.modals.modal-header', ['modalTitle' => 'Upload Dokumen'])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        <div class="row g-2">
            <div class="col-12">
                <label class="d-block required">Nama</label>
                <input type="text" id="input-name" name="name" class="form-control" value="" placeholder="Nama Dokumen" autocomplete="off" required>
            </div>
            <div class="col-12">
                <input type="file" name="doc_file" class="form-control" placeholder="File Dokumen" accept=".pdf,.doc,.docx" autocomplete="off" required>
            </div>
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-save"></i>
            Simpan
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Batal
        </button>
    </div>
</form>