@php
    $user = isset($user) ? $user : Auth::user();
    $title = $user->main_user ? 'Dokumen' : 'Download';
    $downloadName = $user->main_user ? 'document.download' : 'download.download';
    $canCRUD = ($user->main_user_super || $user->main_user_master);
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => [$title],
    'docTitle' => $title
])

@section('content')
@include('partials.alert')
@if ($canCRUD)
<div class="d-block mb-3 pb-2 border-bottom">
    <button type="button" class="btn btn-sm btn-glow" data-bs-toggle="modal" data-bs-target="#doc-modal" data-modal-url="{{ route('document.formUpload') }}" title="Tambah">
        <i class="fa-solid fa-plus" style="line-height: inherit"></i>
        Upload Dokumen
    </button>
</div>
@endif

<ul class="mb-0">
    @foreach ($documents as $document)
        <li class="ps-3 py-2">
            <span class="me-3">{{ $document->name }}</span>
            <span class="small">
                <a href="{{ $document->download_url }}" target="_blank" @if ($canCRUD) class="me-3" @endif>Download</a>
                @if ($canCRUD)
                <a href="#" class="text-danger" data-bs-toggle="modal" data-bs-target="#doc-modal" data-modal-url="{{ route('document.remove', ['document' => $document->code]) }}">Hapus</a>
                @endif
            </span>
        </li>
    @endforeach
</ul>
@endsection

@if ($canCRUD)
    @push('topContent')
        @include('partials.modals.modal', ['bsModalId' => 'doc-modal', 'scrollable' => true])        
    @endpush
@endif
