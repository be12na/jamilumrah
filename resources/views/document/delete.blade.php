<form class="modal-content" method="POST" action="{{ route('document.destroy', ['document' => $document->id]) }}" data-alert-container="#alert-from-container" data-onfocus="#input-name" enctype="multipart/form-data">
    @include('partials.modals.modal-header', ['modalTitle' => 'Konfirmasi'])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        <div class="text-danger fs-5 text-center">
            Hapus Dokumen <span class="fw-bold">{{ $document->name }}</span> ?
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-trash"></i>
            Hapus
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Batal
        </button>
    </div>
</form>