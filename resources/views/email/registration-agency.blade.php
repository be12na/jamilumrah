<table style="width:auto; border:none; background-color:#003300; color:#f8f9fa; line-height:1.5; border-radius:10px;">
    <tr>
        <td style="padding:8px;" colspan="4"></td>
    </tr>
    <tr>
        <td style="padding:8px 32px;" colspan="4">
            <img src="{{ asset('images/brand.png') }}" alt="{{ env('APP_NAME') }}" style="height:40px; width:auto;">
        </td>
    </tr>
    <tr>
        <td style="padding:0 32px;" colspan="4">
            <div>Selamat datang <span style="font-weight:600;">{{ $agency->name }}</span></div>
        </td>
    </tr>
    <tr>
        <td style="padding:0 32px;" colspan="4">
            <div>Berikut adalah data akun anda:</div>
        </td>
    </tr>
    <tr>
        <td style="padding:8px 32px;" colspan="4"></td>
    </tr>
    <tr>
        <td style="padding-left:32px;"></td>
        <td style="width: 140px;">
            <div>Referral</div>
        </td>
        <td style="width:8px;">
            <div>:</div>
        </td>
        <td style="padding-right:32px;">
            <div>{{ $agency->referral->name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><div>Username</div></td>
        <td><div>:</div></td>
        <td style="padding-right:32px;">
            <div>{{ $agency->username }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><div>Nama</div></td>
        <td><div>:</div></td>
        <td style="padding-right:32px;">
            <div>{{ $agency->name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><div>Handphone</div></td>
        <td><div>:</div></td>
        <td style="padding-right:32px;">
            <div>{{ $agency->phone }}</div>
        </td>
    </tr>
    <tr>
        <td style="padding:16px 32px;" colspan="4">
            <div>Silahkan tunggu konfirmasi dari admin kami. Terima kasih !</div>
        </td>
    </tr>
</table>