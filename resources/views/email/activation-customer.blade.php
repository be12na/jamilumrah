<table
    style="
        width: auto;
        border: none;
        background-color: #003300;
        color: #f8f9fa;
        line-height: 1.5;
        border-radius: 10px;
    "
>
    <tr>
        <td style="padding: 8px"></td>
    </tr>
    <tr>
        <td style="padding: 8px 32px">
            <img
                src="{{ asset('images/brand.png') }}"
                alt="{{ env('APP_NAME') }}"
                style="height: 40px; width: auto"
            />
        </td>
    </tr>
    <tr>
        <td style="padding: 0 32px">
            <div>
                Selamat datang
                <span style="font-weight: 600">{{ $customer->name }}</span>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 32px"></td>
    </tr>
    <tr>
        <td style="padding: 0 32px">
            <div>
                Selamat anda telah berhasil diterima menjadi jamaah kami pada kantor perwakilan <b>{{ $customer->branch->name }}</b>.
            </div>
            <div>
                Dengan menjadi jamaah kami, anda dapat menjadi agen kami untuk membawa dan mendaftarkan jamaah baru kepada kami.
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 16px 32px">
            <div style="margin-bottom: 20px">
                Jika anda berminat untuk menjadi agen kami, silahkan klik link berikut:
            </div>
            <a
                href="{{ route('viewToBeAgency', ['unRegisteredCustomer' => $customer->register_token]) }}"
                style="
                    padding: 6px 10px;
                    background:#0d6efd 
                        linear-gradient(
                            to bottom,
                            rgba(230, 230, 230, 0.1) 0%,
                            rgba(0, 0, 0, 0.1) 100%
                        );
                    color: #f8f9fa;
                    text-decoration: none;
                "
                >Mendaftar</a
            >
        </td>
    </tr>
</table>
