<table style="width:auto; border:none; background-color:#003300; line-height:1.5; border-radius:10px;">
    <tr>
        <td style="padding:8px;" colspan="4"></td>
    </tr>
    <tr>
        <td style="padding:8px 32px;" colspan="4">
            <img src="{{ asset('images/brand.png') }}" alt="{{ env('APP_NAME') }}" style="height:40px; width:auto;">
        </td>
    </tr>
    <tr>
        <td style="padding:0 32px; color:#f8f9fa;" colspan="4">
            <div>Selamat datang <span style="font-weight:600;">{{ $customer->name }}</span></div>
        </td>
    </tr>
    <tr>
        <td style="padding:0 32px; color:#f8f9fa;" colspan="4">
            <div>Berikut adalah data kepesertaan anda sebagai jamaah kami:</div>
        </td>
    </tr>
    <tr>
        <td style="padding:8px 32px;" colspan="4"></td>
    </tr>
    <tr>
        <td style="padding-left:32px;"></td>
        <td style="vertical-align:top; width: 140px; color:#f8f9fa;"><div>Kantor Perwakilan</div></td>
        <td style="vertical-align:top; width:8px; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div style="font-weight: 600; color:#f8f9fa;">{{ $customer->branch->name }}</div>
            <div style="margin-bottom: 4px; color:#f8f9fa;">{{ $customer->branch->complete_address }}</div>
            @if ($customer->branch->leader_name)
                <div style="margin-bottom: 4px; color:#f8f9fa;">
                    <span style="margin-right: 2px;">Pimpinan:</span>
                    <span>{{ $customer->branch->leader_name }} ({{ $customer->branch->leader_phone }})</span>
                </div>
            @endif
            <div style="color:#f8f9fa;">
                <span style="margin-right: 2px;">Admin KP:</span>
                <span>{{ $customer->branch->admin->name }} ({{ $customer->branch->admin->phone }})</span>
            </div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Program</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->package->name }}</div>
            <div>Harga Rp @formatNumber($customer->package->price, 0)</div>
            @if ($customer->package->description)
                <div style="margin-top: 4px;">{{ $customer->package->description }}</div>
            @endif
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>No. Identitas</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->identity }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Nama</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa; color:#f8f9fa;"><div>Jenis Kelamin</div></td>
        <td style="vertical-align:top; color:#f8f9fa; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->gender_name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Tempat, Tgl Lahir</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->birth_city }}, @translateDatetime($customer->birth_date, 'd F Y')</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Alamat</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->complete_address }}</div>
        </td>
    </tr>
    {{-- <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Nama Ayah</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->father_name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Pendidikan</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->study_name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Pekerjaan</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->profession }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Pendamping</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->full_companion }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Golongan Darah</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->blood_id }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Merokok</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->smoking ? 'Ya' : 'Tidak' }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Warga Negara</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->citizen }}</div>
        </td>
    </tr> --}}
    <tr>
        <td style="padding:16px 32px; color:#f8f9fa;" colspan="4">
            <div>Pihak Kantor Perwakilan yang dipilih akan segera menghubungi anda untuk proses lebih lanjut.</div>
            <div>Jika ada ketidaksesuaian data, silahkan hubungi agen kami yang telah mendaftarkan anda. Terima kasih !</div>
        </td>
    </tr>
</table>