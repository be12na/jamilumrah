<table style="width:auto; border:none; background-color:#003300; color:#f8f9fa; line-height:1.5; border-radius:10px;">
    <tr>
        <td style="padding:8px;" colspan="4"></td>
    </tr>
    <tr>
        <td style="padding:8px 32px;" colspan="4">
            <img src="{{ asset('images/brand.png') }}" alt="{{ env('APP_NAME') }}" style="height:40px; width:auto;">
        </td>
    </tr>
    <tr>
        <td style="padding:0 32px;" colspan="4">
            <div>Kami informasikan kepada Kantor Cabang Perwakilan <b>{{ config('app.name') }}</b> cabang <b>{{ $customer->branch->name }}</b>, bahwa ada jamaah baru yang mendaftar.</div>
            <div>Berikut adalah data jamaah yang mendaftar:</div>
        </td>
    </tr>
    <tr>
        <td style="padding:8px 32px;" colspan="4"></td>
    </tr>
    <tr>
        <td style="padding-left:32px;"></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Program</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->package->name }}</div>
            <div>Harga Rp @formatNumber($customer->package->price, 0)</div>
            @if ($customer->package->description)
                <div style="margin-top: 4px;">{{ $customer->package->description }}</div>
            @endif
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>No. Identitas</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->identity }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Nama</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa; color:#f8f9fa;"><div>Jenis Kelamin</div></td>
        <td style="vertical-align:top; color:#f8f9fa; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->gender_name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Tempat, Tgl Lahir</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->birth_city }}, @translateDatetime($customer->birth_date, 'd F Y')</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Alamat</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->complete_address }}</div>
        </td>
    </tr>
    {{-- <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Nama Ayah</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->father_name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Pendidikan</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->study_name }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Pekerjaan</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->profession }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Pendamping</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->full_companion }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Golongan Darah</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->blood_id }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Merokok</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->smoking ? 'Ya' : 'Tidak' }}</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>Warga Negara</div></td>
        <td style="vertical-align:top; color:#f8f9fa;"><div>:</div></td>
        <td style="vertical-align:top; padding-right:32px; color:#f8f9fa;">
            <div>{{ $customer->citizen }}</div>
        </td>
    </tr> --}}
    <tr>
        <td style="padding:16px 32px; color:#f8f9fa;" colspan="4">
            <div>Kami harap calon jamaah tersebut segera diproses. Terima Kasih !</div>
        </td>
    </tr>
</table>