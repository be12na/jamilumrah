@php
    $akunText = ($users->count() > 1) ? 'akun - akun' : 'akun';
@endphp
<table
    style="
        width: auto;
        border: none;
        background-color: #f4f4f4;
        line-height: 1.5;
        border-radius: 10px;
    "
>
    <tr>
        <td style="padding: 8px"></td>
    </tr>
    <tr>
        <td style="padding: 8px 32px">
            <img
                src="{{ asset('images/brand-dark.png') }}"
                alt="{{ env('APP_NAME') }}"
                style="height: 40px; width: auto"
            />
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 32px"></td>
    </tr>
    <tr>
        <td style="padding: 0 32px">
            <div style="margin-bottom: 20px;">
                Anda telah melakukan permintaan daftar akun yang menggunakan email <b>{{ $email }}</b>
            </div>
            @if ($users->count() > 1)
                <div style="margin-bottom: 10px;">
                    Berikut adalah daftar {{ $akunText }} anda dan pilih salah satu dari tautan tersebut untuk masuk ke system kami.
                </div>
                <ul style="display:block; margin:0;">
                    @foreach ($users as $user)
                        <li>
                            <a href="{{ route('forgot.username.select', ['forgotUsername' => $user->username]) }}">
                                {{ $user->name }} ({{ $user->username }})
                            </a>
                        </li>
                    @endforeach
                </ul>
            @else
                @php
                    $user = $users->first();
                @endphp
                <div style="margin-bottom:10px;">Klik pada tautan berikut untuk masuk ke system {{ config('app.name') }}</div>
                <a href="{{ route('forgot.username.select', ['forgotUsername' => $user->username]) }}">
                    {{ $user->name }} ({{ $user->username }})
                </a>
            @endif
        </td>
    </tr>
    <tr>
        <td style="padding: 16px 32px">
            <div>
                Mohon abaikan email ini jika anda tidak merasa memilikinya.
            </div>
        </td>
    </tr>
</table>
