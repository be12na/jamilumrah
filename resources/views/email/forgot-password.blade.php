<table
    style="
        width: auto;
        border: none;
        background-color: #f4f4f4;
        line-height: 1.5;
        border-radius: 10px;
    "
>
    <tr>
        <td style="padding: 8px"></td>
    </tr>
    <tr>
        <td style="padding: 8px 32px">
            <img
                src="{{ asset('images/brand-dark.png') }}"
                alt="{{ env('APP_NAME') }}"
                style="height: 40px; width: auto"
            />
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 32px"></td>
    </tr>
    <tr>
        <td style="padding: 0 32px">
            <div style="margin-bottom: 20px;">
                Anda telah melakukan permintaan atur ulang password untuk akun <b>{{ $user->username }}</b>
            </div>
            <div style="margin-bottom:10px;">Klik pada tautan berikut untuk mengatur ulang password anda.</div>
            <div class="margin-bottom:10px;">
                <a href="{{ route('forgot.password.reset.index', ['forgotPasswordToken' => $user->password_token]) }}" style="display:inline-block; padding: 4px 8px; border-radius:3px; background: rgba(3, 28, 65, 0.75) linear-gradient(to bottom, rgba(255, 255, 255, 0.35), rgba(255, 255, 255, 0)); color: #f8f9fa; text-decoration: none; margin-right:20px;">
                    Atur Ulang Password
                </a>
            </div>
            <div>Tautan ini hanya berlaku sampai dengan {{ $user->password_exp_at->format('d M Y H:i') }}</div>
        </td>
    </tr>
    <tr>
        <td style="padding: 16px 32px">
            <div>
                Mohon abaikan email ini jika anda tidak merasa memilikinya.
            </div>
        </td>
    </tr>
</table>
