@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Password'],
    'docTitle' => 'Password'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
<div class="row justify-content-center">
    <div class="col-auto">
        <form class="card mb-3 fs-auto shadow-sm" method="POST" action="{{ route('password.update') }}" data-alert-container="#alert-container">
            <div class="card-header bg-secondary bg-gradient-darken text-light">
                <span class="">Ganti Password</span>
            </div>
            <div class="card-body">
                @csrf
                <label class="d-block required">Password Lama</label>
                <input type="password" name="old_password" class="form-control" placeholder="Password Lama" autocomplete="off" required autofocus>
            </div>
            <div class="card-body border-top">
                <div class="mb-2">
                    <label class="d-block required">Password Baru</label>
                    <input type="password" name="new_password" class="form-control" placeholder="Password Baru" autocomplete="off" required>
                </div>
                <div class="mb-2">
                    <label class="d-block required">Ketik Ulang Password Baru</label>
                    <input type="password" name="new_password_confirmation" class="form-control" placeholder="Ketik Ulang Password Baru" autocomplete="off" required>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-center">
                <button type="submit" class="btn btn-sm btn-secondary">
                    <i class="fa-solid fa-save me-1"></i>
                    Simpan
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
