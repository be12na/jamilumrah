@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('login') }}" data-alert-container="#alert-container" class="w-100 px-2 px-md-3 px-lg-4 px-xl-5">
    <div class="d-block" id="alert-container">
        @include('partials.alert')
    </div>
    @csrf
    <div class="mb-3">
        <label for="username" class="d-block mb-1">Username</label>
        <div class="input-group">
            <label for="username" class="input-group-text justify-content-center" style="width:44px;">
                <i class="fa fa-user"></i>
            </label>
            <input id="username" type="text" class="form-control" name="username" placeholder="Username" value="{{ session()->pull('loginFailed') }}" autocomplete="off" required autofocus>
        </div>
    </div>
    <div class="mb-3">
        <label for="password" class="d-block mb-1">Password</label>
        <div class="input-group">
            <label for="password" class="input-group-text justify-content-center" style="width:44px;">
                <i class="fa fa-key"></i>
            </label>
            <input id="password" type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" data-icon-open="fa fa-eye" data-icon-close="fa fa-eye-slash" required>
            <label for="password" class="input-group-text cursor-pointer pwd-view justify-content-center" data-target="#password" style="width:44px;">
                <i class="pwd-icon"></i>
            </label>
        </div>
    </div>
    <div class="d-flex align-items-center justify-content-between flex-wrap mb-3 pb-2 border-bottom border-light">
        <div class="form-check py-2">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="remember">
                Ingatkan Saya
            </label>
        </div>
        <button type="submit" class="btn btn-primary">
            <i class="fa-solid fa-sign-in me-1"></i>Masuk
        </button>
    </div>
    <div class="text-center text-wrap">
        <div class="d-block mb-2">
            Lupa <a href="{{ route('forgot.username.index') }}">Username</a> atau <a href="{{ route('forgot.password.index') }}">Password</a> ?
        </div>
        <div>Ingin <a href="{{ route('viewToBePalmer') }}">Mendaftar</a> Umrah?</div>
    </div>
</form>
@endsection

@section('scripts')
<script>
    $(function() {
        const usrname = $('#username');
        if (usrname.val().length) {
            usrname[0].select();
        }
    });
</script>
@endsection