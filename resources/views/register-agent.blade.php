@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('regAgency', ['registerReferral' => $referral->username]) }}" data-alert-container="#alert-container" class="w-100 px-2 px-md-3 px-lg-4 px-xl-5" style="--required-color: #FF3F3F;">
    <div class="d-block" id="alert-container">
        @include('partials.alert')
    </div>
    @csrf
    <input type="hidden" name="referral_id" value="{{ $referral->id }}">
    <div class="mb-2 text-center">
        <span>Referral: </span>
        <span>{{ $referral->name }}</span>
    </div>
    <div class="mb-2">
        <label for="name" class="d-block required">Nama <span class="small fst-italic">(Sesuai Kartu Identitas)</span></label>
        <input id="name" type="text" class="form-control form-control-sm rounded-0" name="name" placeholder="Nama" value="{{ old('name') }}" autocomplete="off" required autofocus>
    </div>
    <div class="mb-2">
        <label for="email" class="d-block required">Email</label>
        <input id="email" type="text" class="form-control form-control-sm rounded-0" name="email" placeholder="Email" value="{{ old('email') }}" autocomplete="off" required>
    </div>
    <div class="mb-2">
        <label for="phone" class="d-block required">No. Handphone</label>
        <input id="phone" type="text" class="form-control form-control-sm rounded-0" name="phone" placeholder="No. Handphone" value="{{ old('phone') }}" autocomplete="off" required>
    </div>
    <div class="mb-2">
        <label for="username" class="d-block required">Username</label>
        <input id="username" type="text" class="form-control form-control-sm rounded-0" name="username" placeholder="Username" value="{{ old('username') }}" autocomplete="off" required>
    </div>
    <div class="mb-2">
        <label for="password" class="d-block required">Password</label>
        <input id="password" type="password" class="form-control form-control-sm rounded-0" name="password" placeholder="Password" autocomplete="off" required>
    </div>
    <div class="mb-3">
        <label for="password" class="d-block required">Ulangi Password</label>
        <input id="password" type="password" class="form-control form-control-sm rounded-0" name="password_confirmation" placeholder="Ulangi Password" autocomplete="off" required>
    </div>
    <div class="mb-2">
        <button type="submit" class="btn btn-primary d-block w-100">
            <i class="fa-solid fa-user-plus me-1"></i>
            Registrasi
        </button>
    </div>
    <div class="text-center text-wrap">
        Sudah punya akun ? <a href="{{ route('login') }}">Login</a>
    </div>
</form>
@endsection
