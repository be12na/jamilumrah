@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Master Data', 'Cabang Perwakilan'],
    'docTitle' => 'Master Data'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
<div class="d-block small">
    <table class="table table-sm table-nowrap table-hover table-striped" id="table">
        <thead class="text-center align-middle bg-gray-100 bg-gradient">
            <tr>
                <th>Nama</th>
                <th class="border-start">Alamat</th>
                <th class="border-start">Telepon / Fax</th>
                <th class="border-start">Pimpinan</th>
                <th class="border-start">Admin</th>
                <th class="border-start">Aktif</th>
                <th class="border-start"></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="col-auto d-none" id="button-control">
    <div class="btn-group">
        @if ($user->main_user_super)
        <button type="button" class="btn btn-sm btn-glow" data-href="{{ route('branch.create') }}" title="Tambah">
            <i class="fa-solid fa-plus" style="line-height: inherit"></i>
            Tambah
        </button>
        @endif
        <button type="button" class="btn btn-sm btn-glow" title="Refresh" onclick="refreshTable();">
            <i class="fa-solid fa-rotate" style="line-height: inherit"></i>
        </button>
    </div>
</div>
@endsection

@push('topContent')
@include('partials.modals.modal', ['bsModalId' => 'modal-admin', 'scrollable' => true])
@endpush

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
<style>
    table.dataTable > tbody {
        border-top-width: 0 !important;
    }
    table.dataTable .column-address {
        min-width: 120px;
    }
</style>
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    let table;

    function refreshTable()
    {
        table.ajax.reload();
    }

    $(function() {
        table = $('#table').DataTable({
            dom: '<"row g-2 justify-content-between mb-2"<"#dt-control.col-auto d-flex align-items-end flex-wrap"l><"col-auto d-flex align-items-end mw-100"f>>r<"table-responsive border mb-3"t><"datatable-paging-info row g-2"<"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-start"i><"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-end"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("branch.dataTable") }}',
            },
            deferLoading: 50,
            search: {
                return: true
            },
            pagingType: 'full_numbers',
            lengthChange: false,
            language: {!! json_encode(__('datatable')) !!},
            columns: [
                {data: 'name'},
                {data: 'full_address', searchable: false, orderable: false, className: 'text-wrap column-address'},
                {data: 'phone', searchable: false, orderable: false},
                {data: 'leader', searchable: false, orderable: false},
                {data: 'administrator', searchable: false, orderable: false},
                {data: 'is_active', searchable: false, orderable: false, className: 'dt-body-center'},
                {data: 'view', searchable: false, orderable: false, className: 'dt-body-center'},
                {data: 'fax', visible: false, searchable: false, orderable: false},
                {data: 'address', visible: false},
                {data: 'village_name', visible: false},
                {data: 'district_name', visible: false},
                {data: 'city_name', visible: false},
                {data: 'province_name', visible: false},
            ],
        });

        $('#table_processing.dataTables_processing')
        .removeClass('card')
        .html('<div><i class="fa-solid fa-spinner fa-spin fs-3"></i><span class="text-center fa-beat-fade mt-2">Processing...</span></div>');

        $('#dt-control').append($('#button-control').removeClass('d-none'));

        refreshTable();
    });
</script>
@endpush