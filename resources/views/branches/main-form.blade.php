@php
    $user = isset($user) ? $user : Auth::user();
    $branch = optional($data);
    $admin = optional($branch->admin);
@endphp

@extends('layouts.app', [
    'user' => $user,
    'bodyClass' => 'select2-40',
    'breadCrumbItems' => ['Daftar Cabang', !empty($data) ? 'Edit' : 'Tambah'],
    'docTitle' => 'Daftar Cabang'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
<form class="mb-3" method="POST" action="{{ $postUrl }}" data-alert-container="#alert-container">
    @csrf
    <div class="row g-2 mb-3">
        <div class="col-12">
            <label for="name" class="required">Nama Cabang</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $branch->name }}" placeholder="Nama Cabang" autocomplete="off" required autofocus>
        </div>
        <div class="col-md-6">
            <label for="leader_name" class="required">Nama Pimpinan</label>
            <input type="text" name="leader_name" id="leader_name" class="form-control" value="{{ $branch->leader_name }}" placeholder="Nama Pimpinan" autocomplete="off" required>
        </div>
        <div class="col-md-6">
            <label for="leader_phone" class="required">Handphone Pimpinan</label>
            <input type="text" name="leader_phone" id="leader_phone" class="form-control" value="{{ $branch->leader_phone }}" placeholder="Handphone Pimpinan" autocomplete="off" required>
        </div>
        <div class="col-12">
            <label for="address" class="required">Alamat</label>
            <input type="text" name="address" id="address" class="form-control" value="{{ $branch->address }}" placeholder="Alamat" required autocomplete="off">
        </div>
        <div class="col-12">
            <label class="d-block required">Pilih Daerah</label>
            <select class="form-select select2bs4 select2-custom" name="village_id" id="daerah-id" autocomplete="off" required>
                @if (!empty($data))
                    <option value="{{ $branch->village_id }}">
                        {{ $branch->village_name }}, Kec. {{ $branch->district_name }}, {{ $branch->city_name }}, {{ $branch->province_name }}
                    </option>
                @endif
            </select>
            <div class="d-block" id="daerah-text"></div>
        </div>
        <div class="col-sm-4 col-lg-3">
            <label for="pos_code">Kode Pos</label>
            <input type="text" name="pos_code" id="pos_code" class="form-control" value="{{ $branch->pos_code }}" placeholder="Kode Pos" autocomplete="off">
        </div>
    </div>
    <div class="row g-2 mb-3">
        <div class="col-sm-6 col-lg-4">
            <label for="phone" class="required">Telepon / Kontak</label>
            <input type="text" name="phone" id="phone" class="form-control" value="{{ $branch->phone }}" placeholder="Telepon" autocomplete="off" required>
        </div>
        <div class="col-sm-6 col-lg-4">
            <label for="fax">Fax</label>
            <input type="text" name="fax" id="fax" class="form-control" value="{{ $branch->fax }}" placeholder="Fax" autocomplete="off">
        </div>
        <div class="col-sm-6">
            <label class="d-block required">Email</label>
            <input type="email" name="email" class="form-control" value="{{ $admin->email }}" placeholder="Email" autocomplete="off" required>
        </div>
        @if (!empty($data))
            <div class="col-sm-6 d-flex align-items-end justify-content-sm-center">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="is_active" value="1" id="checkAktif" autocomplete="off" @if($branch->is_active === true) checked @endif>
                    <label class="form-check-label" for="checkAktif">Aktif</label>
                </div>
            </div>
        @endif
    </div>
    <div class="mb-3 border-bottom"></div>
    <div class="mb-3 fw-bold">Administrator</div>
    <div class="row g-2 mb-3">
        <div class="col-md-6">
            <label class="d-block required">Nama Administrator</label>
            <input type="text" name="admin_name" class="form-control" value="{{ $admin->name }}" placeholder="Nama Administrator" autocomplete="off" required>
        </div>
        <div class="col-md-6">
            <label class="d-block required">Handphone</label>
            <input type="text" name="admin_phone" class="form-control" value="{{ $admin->phone }}" placeholder="Handphone Administrator" autocomplete="off" required>
        </div>
        <div class="col-12">
            <label class="d-block required">Username</label>
            <input type="text" name="username" class="form-control" value="{{ $admin->username }}" placeholder="Username" autocomplete="off" required>
        </div>
        <div class="col-sm-6">
            <label class="d-block {{ $branch->has_admin ? '' : 'required' }}">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" @if(!$branch->has_admin) required @endif>
        </div>
        <div class="col-sm-6">
            <label class="d-block {{ $branch->has_admin ? '' : 'required' }}">Ketik Ulang Password</label>
            <input type="password" name="password_confirmation" class="form-control" placeholder="Ketik Ulang Password" autocomplete="off" @if(!$branch->has_admin) required @endif>
        </div>
    </div>
    <div class="pt-2 border-top">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-save me-1"></i>
            Simpan
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-href="{{ route('branch.index') }}">
            <i class="fa-solid fa-undo me-1"></i>
            Batal
        </button>
    </div>
</form>
@include('partials.guides.guide-list', [
    'guideList' => [
        view('partials.guides.required')->render(),
        view('partials.guides.select-area')->render(),
        view('partials.guides.contact')->render(),
        view('partials.guides.handphone')->render(),
        view('partials.guides.fax')->render(),
    ],
])
@endsection

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/i18n/id.js') }}"></script>
@endpush

@push('scripts')
<script>
    let select_daerah;
    $(function() {
        select_daerah = $('#daerah-id');
        select_daerah.select2({
            theme: 'classic',
            placeholder: '-- Pilih Daerah --',
            allowClear: true,
            ajax: {
                url: function(params) {
                    params.current = select_daerah.val();
                    
                    return '{{ route("selectRegion") }}';
                },
                data: function (params) {
                    let dt = {
                        search: params.term,
                        current: params.current
                    };

                    return dt;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        }).on('select2:select', function (e) {
            const data = e.params.data;
            const txt = $('#daerah-text');

            txt.html(data.text);

            if (data.text != '') {
                txt.addClass('mt-2');
            } else {
                txt.removeClass('mt-2');
            }
        });
    });
</script>
@endpush
