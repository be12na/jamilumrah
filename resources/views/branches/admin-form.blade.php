@php
    $admin = optional($branch->admin);
@endphp

<form class="modal-content" method="POST" action="{{ route('branch.admin.save', ['mainBranch' => $branch->id]) }}" data-alert-container="#alert-from-container" data-onfocus="#input-name">
    @include('partials.modals.modal-header', ['modalTitle' => $modalTitle])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        <div class="fw-bold text-center mb-3">Administrator Cabang {{ $branch->name }}</div>
        <div class="row g-2">
            <div class="col-12">
                <label class="d-block required">Nama</label>
                <input type="text" name="name" class="form-control" value="{{ $admin->name }}" placeholder="Nama" autocomplete="off" required>
            </div>
            <div class="col-12">
                <label class="d-block required">Email</label>
                <input type="email" name="email" class="form-control" value="{{ $admin->email }}" placeholder="Email" autocomplete="off" required>
            </div>
            <div class="col-12">
                <label class="d-block required">Handphone</label>
                <input type="text" name="phone" class="form-control" value="{{ $admin->phone }}" placeholder="Handphone" autocomplete="off" required>
            </div>
            <div class="col-12">
                <label class="d-block required">Username</label>
                <input type="text" name="username" class="form-control" value="{{ $admin->username }}" placeholder="Username" autocomplete="off" required>
            </div>
            <div class="col-sm-6">
                <label class="d-block {{ $branch->has_admin ? '' : 'required' }}">Password</label>
                <div class="input-group">
                    <input type="password" id="adm-password" name="password" class="form-control" placeholder="Password" autocomplete="off" data-icon-open="fa fa-eye" data-icon-close="fa fa-eye-slash" @if(!$branch->has_admin) required @endif>
                    <label for="adm-password" class="input-group-text cursor-pointer pwd-view justify-content-center" data-target="#adm-password" style="width:44px;">
                        <i class="pwd-icon"></i>
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <label class="d-block {{ $branch->has_admin ? '' : 'required' }}">Ketik Ulang Password</label>
                <div class="input-group">
                    <input type="password" id="adm-confirm-password" name="password_confirmation" class="form-control" placeholder="Ketik Ulang Password" autocomplete="off" data-icon-open="fa fa-eye" data-icon-close="fa fa-eye-slash" @if(!$branch->has_admin) required @endif>
                    <label for="adm-confirm-password" class="input-group-text cursor-pointer pwd-view justify-content-center" data-target="#adm-confirm-password" style="width:44px;">
                        <i class="pwd-icon"></i>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-save"></i>
            Simpan
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Batal
        </button>
    </div>
</form>