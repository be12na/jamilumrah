<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Umroh Family') }}</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png">
    @if (isLive())
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">    
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}">
</head>
<body style="--bs-body-color: var(--bs-white); --bs-body-font-family: sans-serif; --image-background: url('{{ asset('images/bg-image.png') }}'); --bs-link-color: var(--bs-light); --bs-link-hover-color: var(--bs-primary);">
    <div class="auth-container">
        <div class="flex-fill d-none d-md-flex flex-column align-items-center justify-content-center">
            <div class="fs-1">#UmrohNewNormal</div>
            <div>Solusi Ke Tanah Suci Tidak Lagi Mimpi</div>
        </div>
        <div class="auth-box p-3 bg-dark" style="--bs-bg-opacity:0.85;">
            <a href="{{ route('home') }}" class="text-center mb-3 pb-2 text-decoration-none">
                <img src="{{ asset('images/brand.png') }}">
            </a>
            @yield('content')
        </div>
    </div>
    <div id="processing-overlay" class="show" data-timer="500" style="--process-timer:500;">
        <img src="{{ asset('images/brand.png') }}" alt="Loading..." class="fa-bounce">
        <div class="text-gray-200 text-center mt-1 fs-5 fa-beat-fade">Loading...</div>
    </div>
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    @yield('scripts')
</body>
</html>