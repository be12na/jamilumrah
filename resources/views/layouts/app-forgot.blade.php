<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        {{ config('app.name', 'Umroh Family') }}
        @if (isset($docTitle) && !empty($docTitle))
            @php
                if (is_array($docTitle)) $docTitle = implode(' ', $docTitle);
            @endphp
            | {{ $docTitle }}
        @endif
    </title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
    {{-- @if (isLive())
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @endif --}}
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    @stack('vendorCSS')
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <style>
        .main-forgot {
            min-height: 100vh;
            background-color: var(--bs-body-bg);
            background-image: url("{{ asset('images/bg-image.png') }}");
            background-position: top left;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
    @stack('styles')
</head>

<body>
    <div class="main-forgot @yield('mainCssClass')">
        @yield('content')
    </div>
    
    @stack('topContent')

    <div id="processing-overlay" class="show" data-timer="500" style="--process-timer:500;">
        <img src="{{ asset('images/brand.png') }}" alt="Loading..." class="fa-bounce">
        <div class="text-gray-200 text-center mt-1 fs-5 fa-beat-fade">Loading...</div>
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    @stack('vendorJS')
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
