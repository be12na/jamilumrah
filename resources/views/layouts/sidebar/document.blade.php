@php
    $user = isset($user) ? $user : Auth::user();
    $labelDoc = $user->main_user ? 'Dokumen' : 'Download';
    $docIndexRoute = $user->main_user ? 'document.index' : 'download.index';
@endphp

<li class="sidebar-item">
    <a href="{{ route($docIndexRoute) }}" class="sidebar-link menu-link @menuActive([$docIndexRoute])">
        <span class="sidebar-icon fa-solid fa-download"></span>
        <span class="flex-fill">{{ $labelDoc }}</span>
    </a>
</li>