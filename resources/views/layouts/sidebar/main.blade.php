@php
    $user = isset($user) ? $user : Auth::user();
@endphp
<ul class="sidebar-menu py-3" id="sidebarMenu">
    <li class="sidebar-item">
        <a href="{{ route('dashboard') }}" class="sidebar-link menu-link @menuActive('dashboard')">
            <span class="sidebar-icon fa-solid fa-home"></span>
            <span class="flex-fill">Dashboard</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['branch', 'package'])" data-bs-toggle="collapse" data-bs-target="#sidebar-master">
            <span class="sidebar-icon fa-solid fa-envelopes-bulk"></span>
            <span class="flex-fill">Master Data</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['branch', 'package'])" id="sidebar-master" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('branch.index') }}" class="sidebar-link menu-link @menuActive(['branch'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Cabang Perwakilan</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('package.index') }}" class="sidebar-link menu-link @menuActive(['package'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Paket Program</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['agency'])" data-bs-toggle="collapse" data-bs-target="#sidebar-agency">
            <span class="sidebar-icon fa-solid fa-users-line"></span>
            <span class="flex-fill">Agen</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['agency'])" id="sidebar-agency" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('agency.index') }}" class="sidebar-link menu-link @menuActive(['agency.index', 'agency.deactivate.index'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Daftar</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('agency.activation') }}" class="sidebar-link menu-link @menuActive(['agency.activation'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Aktifasi</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['customer'])" data-bs-toggle="collapse" data-bs-target="#sidebar-customer">
            <span class="sidebar-icon fa-solid fa-users"></span>
            <span class="flex-fill">Jamaah</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['customer'])" id="sidebar-customer" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('customer.index') }}" class="sidebar-link menu-link @menuActive(['customer.index', 'customer.detail'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Daftar</span>
                </a>
            </li>
            @if ($user->main_user_super)
                <li class="sidebar-item">
                    <a href="{{ route('customer.excel.import.index') }}" class="sidebar-link menu-link @menuActive(['customer.excel.import.index'])">
                        <span class="sidebar-icon"></span>
                        <span class="flex-fill">Import</span>
                    </a>
                </li>
            @endif
        </ul>
    </li>
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['bonus'])" data-bs-toggle="collapse" data-bs-target="#sidebar-bonus">
            <span class="sidebar-icon fa-solid fa-coins"></span>
            <span class="flex-fill">Ujroh</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['bonus'])" id="sidebar-bonus" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('bonus.agency') }}" class="sidebar-link menu-link @menuActive(['bonus.agency'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Agen</span>
                </a>
                <a href="{{ route('bonus.branch') }}" class="sidebar-link menu-link @menuActive(['bonus.branch'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Admin Cabang</span>
                </a>
            </li>
        </ul>
    </li>
    @include('layouts.sidebar.document')
</ul>