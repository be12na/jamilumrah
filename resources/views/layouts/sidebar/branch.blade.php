@php
    $user = isset($user) ? $user : Auth::user();
@endphp
<ul class="sidebar-menu menu-bg-info menu-text-light py-3" id="sidebarMenu">
    <li class="sidebar-item">
        <a href="{{ route('dashboard') }}" class="sidebar-link menu-link @menuActive('dashboard')">
            <span class="sidebar-icon fa-solid fa-home"></span>
            <span class="flex-fill">Dashboard</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['network'])" data-bs-toggle="collapse" data-bs-target="#sidebar-network">
            <span class="sidebar-icon fa-solid fa-network-wired"></span>
            <span class="flex-fill">Jaringan</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['network'])" id="sidebar-network" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('network.index') }}" class="sidebar-link menu-link @menuActive(['network.index'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Anggota</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('network.tree') }}" class="sidebar-link menu-link @menuActive(['network.tree'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Struktur Pohon</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['customer'])" data-bs-toggle="collapse" data-bs-target="#sidebar-customer">
            <span class="sidebar-icon fa-solid fa-users"></span>
            <span class="flex-fill">Jamaah</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['customer'])" id="sidebar-customer" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('customer.index') }}" class="sidebar-link menu-link @menuActive(['customer.index', 'customer.detail'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Daftar</span>
                </a>
            </li>
        </ul>
    </li>
    {{-- <li class="sidebar-item">
        <a href="{{ route('bonus.index') }}" class="sidebar-link menu-link @menuActive('bonus.index')">
            <span class="sidebar-icon fa-solid fa-coins"></span>
            <span class="flex-fill">Ujroh</span>
        </a>
    </li> --}}
    <li class="sidebar-item">
        <a href="javascript:;" class="sidebar-link menu-link @sidebarActiveOrCollapsed(['bonus'])" data-bs-toggle="collapse" data-bs-target="#sidebar-bonus">
            <span class="sidebar-icon fa-solid fa-coins"></span>
            <span class="flex-fill">Ujroh</span>
        </a>
        <ul class="sidebar-menu collapse @sidebarShow(['bonus'])" id="sidebar-bonus" data-bs-parent="#sidebarMenu">
            <li class="sidebar-item">
                <a href="{{ route('bonus.agency') }}" class="sidebar-link menu-link @menuActive(['bonus.agency'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Agen</span>
                </a>
                <a href="{{ route('bonus.branch') }}" class="sidebar-link menu-link @menuActive(['bonus.branch'])">
                    <span class="sidebar-icon"></span>
                    <span class="flex-fill">Admin Cabang</span>
                </a>
            </li>
        </ul>
    </li>
    @include('layouts.sidebar.document')
</ul>