@php
    $user = isset($user) ? $user : Auth::user();
@endphp
<nav class="navbar navbar-expand-md {{ isset($navbarTheme) ? $navbarTheme : 'navbar-dark bg-dark' }} border-bottom">
    <div class="container-fluid px-3 px-md-4">
        <button class="navbar-toggler sidebar-toggler" type="button">
            <span class="fa-solid fa-bars"></span>
        </button>
        <a class="navbar-brand d-md-none" href="{{ route('home') }}">
            {{-- <img src="{{ asset('images/brand.png') }}" style="max-height: 30px;" alt="{{ config('app.name') }}"> --}}
            <span class="fw-bold">{{ config('app.name') }}</span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="fa-solid fa-ellipsis-vertical"></span>
        </button>

        <div class="collapse menu-text-dark menu-bg-secondary navbar-collapse" id="navbarContent">
            <ul class="navbar-nav ms-auto" style="--bs-nav-link-padding-x: 0.5rem">
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link menu-link @menuActive(['profile', 'bank.index', 'password.index']) dropdown-toggle d-flex justify-content-between align-items-center" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ $user->name }}
                    </a>

                    <div class="custom-dropdown dropdown-menu dropdown-menu-end rounded-0 rounded-md py-0 py-md-1" aria-labelledby="navbarDropdown">
                        @if ($user->agency_user)
                            <a class="dropdown-item menu-link @menuActive(['profile'])" href="{{ route('profile.index') }}">Profile</a>
                        @endif
                        @hasProfile($user)
                            <a class="dropdown-item menu-link @menuActive('bank.index')" href="{{ route('bank.index') }}">Bank</a>
                        @endhasProfile
                        @if ($user->agency_user || hasProfile($user))
                            <div class="dropdown-divider my-1"></div>
                        @endif
                        <a class="dropdown-item menu-link @menuActive('password.index')" href="{{ route('password.index') }}">Password</a>
                        <div class="dropdown-divider my-1"></div>
                        <a class="dropdown-item menu-link" href="{{ route('logout') }}">Keluar</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>