@php
    $user = isset($user) ? $user : Auth::user();
    $theme = isset($sidebarTheme) ? $sidebarTheme : 'bg-white menu-bg-light menu-text-dark';
    $miniTheme = isset($sidebarMiniTheme) ? $sidebarMiniTheme : 'menu-bg-white menu-text-dark';
    $imgBrand = isset($sidebarBrandImg) ? $sidebarBrandImg : asset('images/brand-dark.png');
@endphp
<div class="sidebar-backdrop"></div>
<div class="sidebar g-2 {{ $theme }}" data-mini-brand-image="{{ asset('images/logo.png') }}" data-mini-sub-theme="{{ $miniTheme }}">
    <a class="sidebar-brand g-0 text-center" href="{{ route('welcome') }}" style="--brand-img-height:40px;">
        <img src="{{ $imgBrand }}" alt="{{ config('app.name') }}" class="sidebar-brand-image">
    </a>
    <div class="mt-2 border-bottom" style="--bs-border-color: var(--bs-gray-500);"></div>
    @if ($user->main_user)
        @include('layouts.sidebar.main', ['user' => $user])
    @elseif ($user->branch_user)
        @include('layouts.sidebar.branch', ['user' => $user])
    @elseif ($user->agency_user)
        @include('layouts.sidebar.agency', ['user' => $user])
    @endif
</div>
