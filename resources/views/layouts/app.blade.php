<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        {{ config('app.name', 'Umroh Family') }}
        @if (isset($docTitle) && !empty($docTitle))
            @php
                if (is_array($docTitle)) {
                    $docTitle = implode(' ', $docTitle);
                }
            @endphp
            | {{ $docTitle }}
        @endif
    </title>
    <link rel="icon" href="{{ asset('images/favicon.png?v=' . $vFile) }}" type="image/png">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png?v=' . $vFile) }}" type="image/x-icon">
    {{-- @if (isLive())
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @endif --}}
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    @stack('vendorCSS')
    <link rel="stylesheet" href="{{ asset('css/styles.css?v=' . $vFile) }}">
    <link rel="stylesheet" href="{{ asset('css/layout.css?v=' . $vFile) }}">
    <link rel="stylesheet" href="{{ asset('css/menu-theme.css?v=' . $vFile) }}">
    @stack('styles')
</head>

@php
    $user = isset($user) ? $user : Auth::user();
    $sidebarTheme = 'bg-gray-800 menu-bg-info menu-text-light';
    $sidebarMiniTheme = 'menu-bg-info menu-text-light';
    $sidebarSubBg = 'var(--bs-dark)';
    $sidebarBrandImg = asset('images/brand.png');
    $navbarTheme = 'navbar-light bg-light';
    $breadcrumbTheme = 'bg-light';
    $bodyClass = isset($bodyClass) ? $bodyClass : '';
    // --bs-body-font-family: sans-serif;
@endphp

<body class="{{ $bodyClass }}"
    style="--sidebar-mini-width: 80px; --sidebar-mini-sub-bg: {{ $sidebarSubBg }}; --layout-gutter-x: 3rem;">
    @include('layouts.sidebar', [
        'sidebarTheme' => $sidebarTheme,
        'sidebarMiniTheme' => $sidebarMiniTheme,
        'user' => $user,
    ])
    <div id="app" class="layout">
        <div class="position-sticky sticky-top">
            @include('layouts.navbar', ['navbarTheme' => $navbarTheme, 'user' => $user])
            <div
                class="d-flex justify-content-center justify-content-md-start small py-2 px-3 px-md-4 border-bottom shadow-sm {{ $breadcrumbTheme }}">
                <ul class="breadcrumb justify-content-center text-center mb-0">
                    @php
                        $breadCrumbItems = isset($breadCrumbItems) && is_array($breadCrumbItems) ? $breadCrumbItems : [];
                    @endphp
                    <li class="breadcrumb-item @if (count($breadCrumbItems) == 0) active @endif">
                        {{ config('app.name') }}</li>
                    @for ($b = 0; $b < count($breadCrumbItems); $b++)
                        <li class="breadcrumb-item @if ($b == count($breadCrumbItems) - 1) active @endif">
                            {{ $breadCrumbItems[$b] }}</li>
                    @endfor
                </ul>
            </div>
        </div>
        <main class="main-layout py-4 mb-3 mb-md-0">
            <div class="container-fluid px-3 px-md-4">
                @yield('content')
            </div>
        </main>
    </div>

    @stack('topContent')

    <div id="processing-overlay" class="show" data-timer="500" style="--process-timer:500;">
        <img src="{{ asset('images/brand-dark.png') }}" alt="Loading..." class="fa-bounce">
        <div class="text-gray-200 text-center mt-1 fs-5 fa-beat-fade">Loading...</div>
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    @stack('vendorJS')
    <script src="{{ asset('js/app.js?v=' . $vFile) }}"></script>
    <script src="{{ asset('js/layout.js?v=' . $vFile) }}"></script>
    @stack('scripts')
</body>

</html>
