@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Master Data', 'Paket Program'],
    'docTitle' => 'Master Data'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
<div class="d-block small">
    <table class="table table-sm table-nowrap table-hover table-striped" id="table">
        <thead class="text-center align-middle bg-gray-100 bg-gradient">
            <tr>
                <th>Kode</th>
                <th class="border-start">Nama</th>
                <th class="border-start">Keterangan</th>
                <th class="border-start">Harga</th>
                <th class="border-start">Aktif</th>
                <th class="border-start"></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="col-auto d-none" id="button-control">
    <div class="btn-group">
        @if ($user->main_user_super)
        <button type="button" class="btn btn-sm btn-glow" data-href="{{ route('package.create') }}" title="Tambah">
            <i class="fa-solid fa-plus" style="line-height: inherit"></i>
            Tambah
        </button>
        @endif
        <button type="button" class="btn btn-sm btn-glow" title="Refresh" onclick="refreshTable();">
            <i class="fa-solid fa-rotate" style="line-height: inherit"></i>
        </button>
    </div>
</div>
@endsection

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    let table;

    function refreshTable()
    {
        table.ajax.reload();
    }

    $(function() {
        table = $('#table').DataTable({
            dom: '<"row g-2 justify-content-between mb-2"<"#dt-control.col-auto d-flex align-items-end flex-wrap"l><"col-auto d-flex align-items-end mw-100"f>>r<"table-responsive border mb-3"t><"datatable-paging-info row g-2"<"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-start"i><"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-end"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("package.dataTable") }}',
            },
            deferLoading: 50,
            info: false,
            search: {
                return: true
            },
            paging: false,
            lengthChange: false,
            language: {!! json_encode(__('datatable')) !!},
            columns: [
                {data: 'code'},
                {data: 'name'},
                {data: 'description', orderable: false, className: 'text-wrap'},
                {data: 'price', searchable: false, className: 'dt-body-right'},
                {data: 'is_active', searchable: false, orderable: false, className: 'dt-body-center'},
                {data: 'view', searchable: false, orderable: false, className: 'dt-body-center'},
            ],
        });

        $('#table_processing.dataTables_processing')
        .removeClass('card')
        .html('<div><i class="fa-solid fa-spinner fa-spin fs-3"></i><span class="text-center fa-beat-fade mt-2">Processing...</span></div>');

        $('#dt-control').append($('#button-control').removeClass('d-none'));

        refreshTable();
    });
</script>
@endpush