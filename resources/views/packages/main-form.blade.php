@php
    $user = isset($user) ? $user : Auth::user();
    $package = optional($data);
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Daftar Paket', !empty($data) ? 'Edit' : 'Tambah'],
    'docTitle' => 'Daftar Paket'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
<form class="mb-3" method="POST" action="{{ $postUrl }}" data-alert-container="#alert-container">
    @csrf
    <div class="row g-2 mb-3">
        <div class="col-md-6">
            <label for="code" class="required">Kode</label>
            <input type="text" name="code" id="code" class="form-control" value="{{ $package->code }}" placeholder="Kode" autocomplete="off" required autofocus>
        </div>
        <div class="col-md-6">
            <label for="name" class="required">Nama</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $package->name }}" placeholder="Nama Paket" required autocomplete="off">
        </div>
        <div class="col-sm-6 col-md-4">
            <label for="price" class="required">Harga</label>
            <div class="input-group">
                <label for="price" class="input-group-text">Rp</label>
                <input type="number" name="price" id="price" class="form-control" min="0" value="{{ $package->price ?? 0 }}" placeholder="Harga" autocomplete="off">
            </div>
        </div>
        <div class="col-12">
            <label for="phone">Keterangan</label>
            <textarea name="description" id="description" class="form-control" autocomplete="off">{{ $package->description }}</textarea>
        </div>
    </div>
    <div class="pt-2 border-top">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-save me-1"></i>
            Simpan
        </button>
        <button type="submit" class="btn btn-sm btn-warning" data-href="{{ route('package.index') }}">
            <i class="fa-solid fa-undo me-1"></i>
            Batal
        </button>
    </div>
</form>
<div class="small">
    <div class="mb-1 fw-bold">Petunjuk & Keterangan:</div>
    <ul>
        <li>
            <span class="text-danger me-1">*</span>
            <span>Wajib diisi</span>
        </li>
        <li>
            <div><span class="fw-bold">Harga Paket</span> harus diisi dengan bilangan bulat tanpa menggunakan tanda desimal atau pemisah bilangan ribuan.</div>
        </li>
    </ul>
</div>
@endsection
