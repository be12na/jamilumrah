<form class="modal-content" method="POST" action="{{ route('customer.saveTransfer', ['customerTransferable' => $customer->id]) }}" data-alert-container="#alert-transfer-container" enctype="multipart/form-data">
    @include('partials.modals.modal-header', ['modalTitle' => 'Transfer Jamaah'])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-transfer-container">
            @include('partials.alert')
        </div>
        <div class="row g-2">
            <div class="col-md-6">
                <label class="d-block required">Jenis Pembayaran</label>
                <select name="payment_method" class="form-select" autocomplete="off" required>
                    <option value="">-- Jenis Pembayaran --</option>
                    @foreach (PAYMENT_METHODS as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-12">
                <label class="d-block required">Rekening Tujuan</label>
                <select id="main-bank" name="main_bank_id" class="form-select" autocomplete="off" onchange="bankText($(this));" required>
                    <option value="">-- Pilih Rekening --</option>
                    @foreach ($mainBanks as $mainBank)
                        <option value="{{ $mainBank->id }}" data-bank-code="{{ $mainBank->bank->code }}" data-bank-name="{{ $mainBank->bank->name }}" data-bank-account="{{ $mainBank->account_no }}" data-bank-owner="{{ $mainBank->account_name }}">{{ $mainBank->bank->code }} - {{ $mainBank->account_no }} ({{ $mainBank->account_name }})</option>
                    @endforeach
                </select>
                <div class="mt-2" id="bank-text"></div>
            </div>
            <div class="col-12">
                <label class="d-block">Keterangan Pembayaran</label>
                <textarea name="payment_note" class="form-control" placeholder="Keterangan Pembayaran" autocomplete="off">{{ $customer->payment_note }}</textarea>
            </div>
            {{-- <div class="col-12">
                <label class="required">Bukti Transfer</label>
                <label class="d-flex align-items-center justify-content-center p-2 border rounded bg-gray-300" style="cursor: pointer; min-height:150px;">
                    <input type="file" name="image" class="d-none" id="image" accept="image/jpeg,image/png" onchange="previewImage(this, '#preview-image')">
                    <img alt="Bukti Transfer" id="preview-image" src="" style="height:auto; width:auto; max-width:100%;">
                </label>
            </div> --}}
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-save"></i>
            {{-- Upload dan Simpan --}}
            Submit
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Batal
        </button>
    </div>
</form>

<script>
    $(function() {
        bankText($('#main-bank'));
    });
</script>
