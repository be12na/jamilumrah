@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Jamaah', 'Import'],
    'docTitle' => 'Jamaah'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
<div class="mb-3 pb-1 border-bottom">
    <div class="fw-bold text-decoration-underline">Import Jamaah</div>
    <div class="d-block small fst-italic">
        <span class="d-inline-block me-2">Import data jamaah baru dengan format yang sudah disediakan.</span>
        <span class="d-inline-block me-2">
            Download Format: 
            <a href="javascript:;" onclick="window.open('{{ route('customer.excel.download', ['format' => 'xlsx']) }}');">XLSX</a>
            atau
            <a href="javascript:;" onclick="window.open('{{ route('customer.excel.download', ['format' => 'csv']) }}');">CSV</a>
        </span>
    </div>
</div>
<form class="input-group mb-2" method="GET" action="{{ route('customer.excel.import.apply') }}" enctype="multipart/form-data" id="form-excel">
    @csrf
    <input class="form-control form-control-sm" name="excel" id="upload-file" type="file" id="importFile" accept=".xlsx,.csv" autocomplete="off">
    <button type="button" class="btn btn-sm btn-primary" id="btn-upload">Tampilkan</button>
</form>
<form method="POST" action="{{ route('customer.excel.import.save') }}" data-alert-container="#alert-container">
    @csrf
    <div class="mb-3 p-2 p-md-3 border rounded-3 bg-light">
        @include('customers.register-select-branch', [
            'mode' => 'html',
            'selectedBranch' => null,
        ])
        @include('customers.register-select-package', [
            'mode' => 'html'
        ])
        <div class="mb-2">
            <label class="d-block required">Agen</label>
            <select class="form-select select2bs4 select2-custom" name="agency_id" id="agency-id" autocomplete="off" required>
                <option value="-1">Cabang Yang Dipilih</option>
                @foreach ($agencies as $agency)
                    <option value="{{ $agency->id }}">{{ $agency->name }} ({{ $agency->username }})</option>
                @endforeach
            </select>
            <div class="d-block d-sm-none small" id="daerah-text"></div>
        </div>
        <div class="mb-2">
            <label class="d-block required">Daerah Jamaah</label>
            <select class="form-select select2bs4 select2-custom" name="village_id" id="daerah-id" autocomplete="off" required></select>
            <div class="d-block d-sm-none small" id="daerah-text"></div>
        </div>
    </div>
    <div class="mb-3">
        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
    </div>
    <div class="dataTables_wrapper dt-bootstrap5 no-footer small">
        <div class="mb-3 border table-responsive">
            <table class="table table-striped dataTable" id="table">
                <thead class="text-center align-middle bg-gray-100 bg-gradient mb-2">
                    <tr>
                        <th>Identitas</th>
                        <th class="border-start">Nama</th>
                        <th class="border-start">Jns. Kelamin</th>
                        <th class="border-start">Tmp. Lahir</th>
                        <th class="border-start">Tgl. Lahir</th>
                        <th class="border-start">Email</th>
                        <th class="border-start">Handphone</th>
                        <th class="border-start">Alamat</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="mt-3">
        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
    </div>
</form>
@endsection

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker-1.9.0/css/bootstrap-datepicker.standalone.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
<style>
    #table > tbody .input-group {
        flex-wrap: nowrap;
    }
    #table > tbody .form-control {
        min-width: 200px;
    }
    #table > tbody .form-select {
        min-width: 160px;
    }
</style>
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/bootstrap-datepicker-1.9.0/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-datepicker-1.9.0/locales/bootstrap-datepicker.id.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/i18n/id.js') }}"></script>
@endpush

@push('scripts')
@include('customers.register-select-branch', [
    'mode' => 'script',
])
@include('customers.register-select-package', [
    'mode' => 'script'
])
<script>
    let select_daerah;

    $(function() {
        $('#agency-id').select2({
            theme: 'classic',
            placeholder: '-- Pilih Agen --',
        });

        select_daerah = $('#daerah-id');
        select_daerah.select2({
            theme: 'classic',
            placeholder: '-- Pilih Daerah --',
            allowClear: true,
            ajax: {
                url: function(params) {
                    params.current = select_daerah.val();
                    
                    return '{{ route("selectRegion") }}';
                },
                data: function (params) {
                    let dt = {
                        search: params.term,
                        current: params.current
                    };

                    return dt;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        }).on('select2:select', function (e) {
            const data = e.params.data;
            const txt = $('#daerah-text');

            txt.html(data.text);

            if (data.text != '') {
                txt.addClass('mt-2');
            } else {
                txt.removeClass('mt-2');
            }
        });

        $('#upload-file').on('change', function(e) {
            $('#table tbody').empty();
        });

        $('#btn-upload').on('click', function(e) {
            const frm = $('#form-excel');
            const data = new FormData(frm[0]);
            const url = frm.attr('action');
            const tableBody = $('#table tbody');
            const msg = $('#alert-container');

            tableBody.empty();
            msg.empty();
            
            $.post({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(respon) {
                tableBody.html(respon);
                initDatePicker();
            }).fail(function(respon) {
                msg.html(getFormErrorResponse(respon));
            });
        });
    });
</script>
@endpush
