@php
    $user = isset($user) ? $user : Auth::user();
    $isShowPayment = !$user->agency_user && !in_array($customer->status, [CUSTOMER_STATUS_REGISTER, CUSTOMER_STATUS_CANCELED]);
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Jamaah'],
    'docTitle' => 'Jamaah',
])

@section('content')
    @include('partials.alert')
    <div class="card fs-auto"
        style="--bs-card-spacer-y:0.5rem; --bs-card-spacer-x:0.5rem; --bs-card-cap-padding-x: var(--bs-card-spacer-x);">
        @include('partials.card-header', ['cardTitle' => 'Detail Jamaah'])
        <div class="card-body">
            <div class="row g-2 mb-3 text-center text-md-start">
                <div class="{{ !$user->branch_user ? 'col-12' : 'col-md-auto' }}">
                    <div class="{{ $user->branch_user ? 'border bg-light bg-gradient p-3' : '' }}">
                        @if (!$user->branch_user)
                            <div class="mb-2">
                                <div class="fw-bold">Kantor Perwakilan {{ $customer->branch->name }}</div>
                                @if ($customer->branch->leader_name)
                                    <div class="d-block">
                                        <span class="d-inline-block me-2">Pimpinan:</span>
                                        <span class="d-inline-block">
                                            {{ $customer->branch->leader_name }}
                                            @if ($customer->branch->leader_phone)
                                                ({{ $customer->branch->leader_phone }})
                                            @endif
                                        </span>
                                    </div>
                                @endif
                                @if ($customer->branch->admin)
                                    <div class="d-block">
                                        <span class="d-inline-block me-2">Admin KP:</span>
                                        <span class="d-inline-block">
                                            {{ $customer->branch->admin->name }}
                                            @if ($customer->branch->admin->phone)
                                                ({{ $customer->branch->admin->phone }})
                                            @endif
                                        </span>
                                    </div>
                                @endif
                                <div>
                                    <span class="d-inline-block me-2">Alamat:</span>
                                    <span class="d-inline-block">{{ $customer->detail_complete_address }}</span>
                                </div>
                            </div>
                        @endif
                        <div class="mb-2">
                            <div class="fw-bold">
                                Paket Program: {{ $customer->package->name }}
                            </div>
                            <div>Harga: Rp @formatNumber($customer->package->price, 0)</div>
                            @if ($customer->package->description)
                                <div>Keterangan: {{ $customer->package->description }}</div>
                            @endif
                        </div>
                        @if ($isShowPayment)
                            <div class="mb-2">
                                <span class="p-1 bg-primary text-light">
                                    {{ Arr::get(PAYMENT_METHODS, $customer->payment_method, 'None') }}
                                </span>
                            </div>
                        @endif
                        <div>
                            @php
                                $statusList = customerStatusList($user);
                                $status = Arr::get($statusList, $customer->status, ['-', '']);
                                $statusName = $status[0];
                                $statusClass = $status[1];
                            @endphp
                            <span class="p-1 {{ $statusClass }}">{{ $statusName }}</span>
                        </div>
                    </div>
                </div>
                @if ($isShowPayment)
                    <div class="col-12">
                        {{-- @if (!empty($customer->payment_note))
                    <div class="d-block mb-2">
                        Keterangan: {{ $customer->payment_note }}
                    </div>
                @endif --}}
                        {{-- <div class="d-block mb-2">
                    <button type="button" class="btn btn-sm btn-primary collapsed" data-bs-toggle="collapse" data-bs-target="#viewPayment">Lihat Pembayaran</button>
                </div>
                <div class="collapse" id="viewPayment">
                    <div class="d-block mb-2">
                        <span class="d-inline-block me-2">Rekening Tujuan: </span><span class="d-inline-block">{{ $customer->mainBank ? $customer->mainBank->bank->name . ', ' . $customer->mainBank->account_no . ' (' . $customer->mainBank->account_name . ')' : '-' }}</span>
                    </div>
                    <div class="border p-1 p-md-2 bg-white text-center">
                        <img src="{{ $customer->payment_image_url }}" alt="Image Bukti Transfer" style="width:auto; max-width:100%;">
                    </div>
                </div> --}}
                        <div class="d-block">
                            <span class="d-inline-block me-2">Pembayaran: </span><span
                                class="d-inline-block">{{ $customer->mainBank ? $customer->mainBank->bank->name . ', ' . $customer->mainBank->account_no . ' (' . $customer->mainBank->account_name . ')' : '-' }}</span>
                        </div>
                        @if (!empty($customer->payment_note))
                            <div class="d-block mb-2">
                                Keterangan: {{ $customer->payment_note }}
                            </div>
                        @endif
                    </div>
                @endif
                @if (!$user->agency_user)
                    <div class="col-12 fw-bold">
                        <div class="border-bottom">
                            Agen :
                            {{ mainAgency($customer->agency) ? 'Kantor Pusat' : $customer->agency->name }}
                        </div>
                    </div>
                @endif
            </div>
            <div class="border-bottom mb-2 fw-bold">Biodata Jamaah</div>
            <div class="row g-1 g-md-2 mb-3">
                <div class="col-lg-6">
                    <div class="row g-1">
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">No. Identitas</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->identity }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Nama</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->name }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Jns. Kelamin</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->gender_name }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Tempat Lahir</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->birth_city }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Tanggal Lahir</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">@translateDatetime($customer->birth_date, 'd F Y')</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Email</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->email }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Handphone</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->phone }}</div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row g-1">
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Alamat</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->address }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Desa / Kelurahan</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->village_name }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Kecamatan</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->district_name }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Kota / Kabupaten</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->city_name }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Propinsi</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->province_name }}</div>
                        <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                            <span class="text-wrap me-1">Kode Pos</span><span>:</span>
                        </div>
                        <div class="col-7 col-lg-8">{{ $customer->pos_code ?? '-' }}</div>
                    </div>
                </div>
                {{-- <div class="col-lg-6">
                <div class="row g-1">
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Nama Ayah</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->father_name }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Pendidikan</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->study_name }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Pekerjaan / Profesi</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->profession ?? '-' }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Pendamping</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->full_companion }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Gol. Darah</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->blood_id }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Merokok</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->smoking ? 'Ya' : 'Tidak' }}</div>
                    <div class="col-5 col-lg-4 d-flex flex-nowrap justify-content-between">
                        <span class="text-wrap me-1">Warga Negara</span><span>:</span>
                    </div>
                    <div class="col-7 col-lg-8">{{ $customer->citizen }}</div>
                </div>
            </div> --}}
            </div>
        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-sm btn-warning" data-href="{{ route('customer.index') }}">
                <i class="fa-solid fa-arrow-left me-1"></i>
                Kembali
            </button>
            @if ($user->branch_user_admin)
                @if ($customer->is_transferable)
                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                        data-bs-target="#my-transfer-modal"
                        data-modal-url="{{ route('customer.transfer', ['customerTransferable' => $customer->id]) }}">
                        <i class="fa-solid fa-money-bill-transfer me-1"></i>
                        Transfer
                    </button>
                @endif
            @elseif ($user->main_user)
                @if ($user->main_user_super)
                    @if ($customer->is_approving)
                        <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                            data-bs-target="#my-action-modal"
                            data-modal-url="{{ route('customer.approve.view', ['customerApprove' => $customer->id]) }}">
                            <i class="fa-solid fa-handshake-simple me-1"></i>
                            Terima
                        </button>
                        <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal"
                            data-bs-target="#my-action-modal"
                            data-modal-url="{{ route('customer.reject.view', ['customerReject' => $customer->id]) }}">
                            <i class="fa-solid fa-handshake-simple-slash me-1"></i>
                            Tolak
                        </button>
                    @else
                        @if ($customer->is_transferable)
                            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                                data-bs-target="#my-transfer-modal"
                                data-modal-url="{{ route('customer.transfer', ['customerTransferable' => $customer->id]) }}">
                                <i class="fa-solid fa-money-bill-transfer me-1"></i>
                                Transfer
                            </button>
                        @endif
                        @if ($customer->is_cancelable)
                            <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal"
                                data-bs-target="#my-action-modal"
                                data-modal-url="{{ route('customer.cancel.view', ['customerCancel' => $customer->id]) }}">
                                <i class="fa-solid fa-trash me-1"></i>
                                Batalkan
                            </button>
                        @endif
                    @endif
                @endif
            @endif

            @if (($user->main_user_super || $user->branch_user_admin) && $customer->is_resend_to_be_agency)
                <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                    data-bs-target="#my-resend-modal"
                    data-modal-url="{{ route('customer.resend.toBeAgency', ['unRegisteredCustomerId' => $customer->id]) }}">
                    <i class="fa-solid fa-paper-plane me-1"></i>
                    Kirim Ulang Aktifasi Agen
                </button>
            @endif

            @if ($user->main_user_super && $customer->is_deletable)
                <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal"
                    data-bs-target="#my-action-modal"
                    data-modal-url="{{ route('customer.remove.view', ['customerRemove' => $customer->id]) }}">
                    <i class="fa-solid fa-trash me-1"></i>
                    Hapus
                </button>
            @endif

            {{-- @if (canEditCustomer($customer, $user) && $customer->is_editable)
        <button type="button" class="btn btn-sm btn-info" data-href="{{ route('customer.edit', ['customerEditable' => $customer->id]) }}">
            <i class="fa-solid fa-pencil me-1"></i>
            Edit
        </button>
        @endif --}}
        </div>
    </div>
@endsection

@push('topContent')
    @if (($user->branch_user_admin || $user->main_user_super) && $customer->is_transferable)
        @include('partials.modals.modal-lg', ['bsModalId' => 'my-transfer-modal', 'scrollable' => true])
    @endif

    @if ($user->main_user_super && ($customer->is_approving || $customer->is_cancelable || $customer->is_deletable))
        @include('partials.modals.modal', ['bsModalId' => 'my-action-modal', 'scrollable' => true])
    @endif

    @if (($user->main_user_super || $user->branch_user_admin) && $customer->is_resend_to_be_agency)
        @include('partials.modals.modal', ['bsModalId' => 'my-resend-modal', 'scrollable' => true])
    @endif
@endpush

@if ($user->branch_user_admin && $customer->is_transferable)
    @push('scripts')
        <script>
            function previewImage(obj, target) {
                const [file] = obj.files;
                if (file) {
                    $(target).attr('src', URL.createObjectURL(file));
                }
            }

            function bankText(select) {
                const opt = $(':selected', select);
                const bt = $('#bank-text');
                bt.empty();

                if (select.val()) {
                    const bn = opt.data('bank-name');
                    const accno = opt.data('bank-account');
                    const accnm = opt.data('bank-owner');

                    bt.html(
                        '<div>' + bn + '</div>' + '<div>' + accno + '</div>' + '<div>' + accnm + '</div>'
                    );
                }
            }
        </script>
    @endpush
@endif
