<form class="modal-content" method="POST" action="{{ route('customer.resend.toBeAgency', ['unRegisteredCustomerId' => $customer->id]) }}" data-alert-container="#alert-resend-container">
    @include('partials.modals.modal-header', ['modalTitle' => 'Konfirmasi'])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-resend-container">
            @include('partials.alert')
        </div>
        <div class="text-center fw-bold mb-3">
            Kirim ulang Aktifasi menjadi agen {{ config('app.name') }} ?
        </div>
        <div class="d-block">
            <label class="d-block required">Email Tujuan</label>
            <input type="email" name="email" class="form-control" placeholder="Email Tujuan" value="{{ $customer->email }}" autocomplete="off" required>
            <div class="small fst-italic">Email jamaah akan diupdate dengan email tujuan</div>
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa-solid fa-paper-plane"></i>
            Kirim Ulang
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Tutup
        </button>
    </div>
</form>