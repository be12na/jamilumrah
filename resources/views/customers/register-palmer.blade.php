<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Umroh Family') }}</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png">
    @if (isLive())
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker-1.9.0/css/bootstrap-datepicker.standalone.min.css') }}"> --}}
    @include('partials.datepicker', ['isPlugin' => true, 'isJs' => false])
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <style>
        body {
            height: 100vh;
            overflow: hidden;
            background-image: url("{{ asset('images/bg-image.png') }}");
            background-repeat: no-repeat;
            background-position: bottom left;
            background-size: cover;
        }
        .main-container {
            display: block;
            height: 100%;
            width: 100%;
            overflow: auto;
        }
    </style>
</head>

@php
    $selectionBranch = ($agency->main_user || $agency->agency_user);
@endphp

<body class="select2-40">
    <div class="main-container">
        <div class="container py-3 py-md-4">
            <div class="row g-2 justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <div class="p-3 px-lg-4 px-xl-5 border rounded-top bg-light" style="--bs-border-radius:var(--bs-border-radius-xl); --bs-bg-opacity:0.95;">
                        <form method="POST" action="{{ route('saveToBePalmer', ['registerPalmerByToken' => $agency->link_token]) }}" data-alert-container="#alert-container" style="--required-color: #FF3F3F;">
                            <div class="d-block" id="alert-container">
                                @include('partials.alert')
                            </div>
                            @csrf
                            <div class="text-center mb-2">
                                <a href="{{ route('home') }}" class="d-inline-block text-decoration-none">
                                    <img src="{{ asset('images/brand-dark.png') }}" style="max-height:50px;">
                                </a>
                            </div>
                            <div class="text-center border-bottom border-secondary mb-3 fs-5 fw-bold">Form Pendaftaran Jamaah</div>
                            @if ($agency->agency_user)
                                <div class="mb-3">
                                    <label class="d-block">Agen Anda</label>
                                    <input type="text" class="form-control" value="{{ $agency->name }}" readonly>
                                </div>
                            @endif
                            @include('customers.register-select-branch', [
                                'mode' => 'html',
                                'selectedBranch' => $selectedBranch
                            ])
                            @include('customers.register-select-package', [
                                'mode' => 'html'
                            ])
                            <div class="border-bottom mb-2 fw-bold">Data Jamaah</div>
                            <div class="row g-2 mb-3">
                                <div class="col-md-6 col-lg-4">
                                    <label class="d-block required">No. Identitas</label>
                                    <input type="text" class="form-control" name="identity" placeholder="No. Identitas" autocomplete="off" required autofocus>
                                </div>
                                <div class="col-md-6 col-lg-8">
                                    <label class="d-block required">Nama Lengkap</label>
                                    <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" autocomplete="off" required>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <label class="d-block required">Jns. Kelamin</label>
                                    <select name="gender" class="form-select" autocomplete="off" required>
                                        @foreach (GENDERS as $LP => $gender)
                                        <option value="{{ $LP }}">{{ $gender }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <label class="d-block required">Tmp. Lahir</label>
                                    <input type="text" class="form-control" name="birth_city" placeholder="Tempat Lahir" autocomplete="off" required>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <label class="d-block required">Tgl. Lahir</label>
                                    @include('partials.datepicker', [
                                        'dateId' => 'birth-date',
                                        'dateName' => 'birth_date',
                                        'dateValue' => null,
                                        'endDate' => \Carbon\Carbon::today(),
                                        'required' => true,
                                        'placeholder' => 'Tgl. Lahir',
                                    ])
                                </div>
                            </div>
                            <div class="border-bottom mb-2 fw-bold">Kontak dan Informasi</div>
                            <div class="row g-2 mb-3">
                                <div class="col-md-6">
                                    <label class="d-block required">Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email" autocomplete="off" required>
                                </div>
                                <div class="col-md-6">
                                    <label class="d-block required">Handphone</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Handphone" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="border-bottom mb-2 fw-bold">Alamat / Tempat Tinggal</div>
                            <div class="row g-2 mb-3">
                                <div class="col-12">
                                    <label class="d-block required">Alamat</label>
                                    <input type="text" class="form-control" name="address" placeholder="Alamat" autocomplete="off" required>
                                </div>
                                <div class="col-12">
                                    <label class="d-block required">Pilih Daerah</label>
                                    <select class="form-select select2bs4 select2-custom" name="village_id" id="daerah-id" autocomplete="off" required>
                                    </select>
                                    <div class="d-block d-sm-none small" id="daerah-text"></div>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-2">
                                    <label class="d-block">Kode Pos</label>
                                    <input type="text" name="pos_code" class="form-control" placeholder="Kode Pos" autocomplete="off">
                                </div>
                            </div>
                            {{-- <div class="border-bottom mb-2 fw-bold">Informasi Lain - Lain</div>
                            <div class="row g-2 mb-2">
                                <div class="col-12">
                                    <label class="d-block required">Nama Ayah</label>
                                    <input type="text" name="father_name" class="form-control" placeholder="Nama Ayah" autocomplete="off" required>
                                </div>
                                <div class="col-sm-6">
                                    <label class="d-block required">Pendidikan</label>
                                    <select name="school_id" class="form-select" autocomplete="off">
                                        <option class="">-- Pendidikan --</option>
                                        @foreach (SCHOOL_LEVELS as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label class="d-block">Pekerjaan / Profesi</label>
                                    <input type="text" name="profession" class="form-control" placeholder="Pekerjaan / Profesi" autocomplete="off">
                                </div>
                            </div>
                            <div class="row g-2 mb-2">
                                <div class="col-sm-6">
                                    <label class="d-block required">Pendamping</label>
                                    <select name="companion_id" id="companion-id" class="form-select" autocomplete="off">
                                        @foreach (COMPANION_RELATIONSHIP as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6" id="input-companion">
                                    <label class="d-block required">Nama Pendamping</label>
                                    <input type="text" name="companion" class="form-control" placeholder="Nama Pendamping" autocomplete="off">
                                </div>
                            </div>
                            <div class="mb-2">
                                <label class="d-block required">Golongan Darah</label>
                                <select name="blood_id" class="form-select w-auto" autocomplete="off">
                                    @foreach (BLOODS as $blood)
                                        <option value="{{ $blood }}">{{ $blood }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="smoking" id="smoking" value="1" autocomplete="off">
                                    <label class="form-check-label" for="smoking">Merokok</label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="mb-2">
                                    <div class="form-check">
                                        <input class="form-check-input checked-hide" type="checkbox" name="is_wni" value="1" id="checkWNI" data-target="#inputWNA" data-callback="requireCountry" autocomplete="off" checked>
                                        <label class="form-check-label" for="checkWNI">Warga Negara Indonesia</label>
                                    </div>
                                </div>
                                <div id="inputWNA">
                                    <label class="d-block required">Negara Asal</label>
                                    <input type="text" name="wna_country" id="wna-country" class="form-control" placeholder="Negara Asal" autocomplete="off">
                                </div>
                            </div> --}}
                            <div class="d-block pt-2 border-top border-secondary">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="fa-solid fa-file-circle-check me-1"></i>
                                    Mendaftar
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="p-2 p-md-3 border rounded-bottom bg-white" style="--bs-border-radius:var(--bs-border-radius-xl);">
                        @include('partials.guides.guide-list', [
                            'guideList' => [
                                view('partials.guides.required')->render(),
                                '<div>Untuk pengisian <span class="fw-bold">Alamat</span> tidak perlu disertakan nama <span class="fw-bold">Desa / Kelurahan</span>, <span class="fw-bold">Kecamatan</span>, <span class="fw-bold">Kota / Kabupaten</span> dan <span class="fw-bold">Propinsi</span></div>',
                                view('partials.guides.select-area')->render(),
                                view('partials.guides.handphone')->render(),
                            ],
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="processing-overlay" class="show" data-timer="500" style="--process-timer:500;">
        <img src="{{ asset('images/brand.png') }}" alt="Loading..." class="fa-bounce">
        <div class="text-gray-200 text-center mt-1 fs-5 fa-beat-fade">Loading...</div>
    </div>
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/bootstrap-datepicker-1.9.0/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-datepicker-1.9.0/locales/bootstrap-datepicker.id.min.js') }}"></script> --}}
    
    @include('partials.datepicker', ['isPlugin' => true, 'isJs' => true])

    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/i18n/id.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    @include('customers.register-select-branch', [
        'mode' => 'script',
        'selectedBranch' => $selectedBranch
    ])
    @include('customers.register-select-package', [
        'mode' => 'script'
    ])

    <script>
        let select_daerah;

        function requireCountry(obj)
        {
            const wna = $('#wna-country');
            if (obj.is(':checked')) {
                wna.attr('required', null);
            } else {
                wna.attr('required', 'required');
            }
        }

        $(function() {
            select_daerah = $('#daerah-id');
            select_daerah.select2({
                theme: 'classic',
                placeholder: '-- Pilih Daerah --',
                allowClear: true,
                ajax: {
                    url: function(params) {
                        params.current = select_daerah.val();
                        
                        return '{{ route("selectRegion") }}';
                    },
                    data: function (params) {
                        let dt = {
                            search: params.term,
                            current: params.current
                        };

                        return dt;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            }).on('select2:select', function (e) {
                const data = e.params.data;
                const txt = $('#daerah-text');

                txt.html(data.text);

                if (data.text != '') {
                    txt.addClass('mt-2');
                } else {
                    txt.removeClass('mt-2');
                }
            });

            $('#companion-id').on('change', function(e) {
                const me = $(this);
                const req = (me.val() > {{ COMPANION_NONE }});
                const target = $('#input-companion');

                if (req) {
                    target.removeClass('d-none');
                } else {
                    target.addClass('d-none');
                }

                $('input[name="companion"]', target).attr('required', req ? 'required' : null);
            }).change();

            $('#checkWNI').change();
        });
    </script>
</body>
</html>
