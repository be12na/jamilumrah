@php
    // $requireNotes = in_array($actionId, [CUSTOMER_STATUS_REJECTED, CUSTOMER_STATUS_CANCELED]);
    $requireNotes = $actionId == CUSTOMER_STATUS_REJECTED;
    $btnText = 'Terima';
    $btnClass = 'btn-primary';
    $btnIcon = 'fa-handshake-simple';
    $actionMessage = 'Terima';

    if ($actionId == CUSTOMER_STATUS_REJECTED) {
        $btnText = 'Tolak';
        $btnClass = 'btn-danger';
        $btnIcon = 'fa-handshake-simple-slash';
        $actionMessage = 'Tolak';
    } elseif ($actionId == CUSTOMER_STATUS_CANCELED) {
        $btnText = 'Batalkan';
        $btnClass = 'btn-danger';
        $btnIcon = 'fa-trash';
        $actionMessage = 'Batalkan';
    } elseif ($actionId == CUSTOMER_STATUS_DELETED) {
        $btnText = 'Hapus';
        $btnClass = 'btn-danger';
        $btnIcon = 'fa-trash';
        $actionMessage = 'Hapus data';
    }
@endphp

<form class="modal-content" method="POST" action="{{ $postUrl }}" data-alert-container="#alert-action-container">
    @include('partials.modals.modal-header', ['modalTitle' => $modalTitle])
    <div class="modal-body fs-auto">
        @csrf
        <input type="hidden" name="action_id" value="{{ $actionId }}">
        <div class="d-block" id="alert-action-container">
            @include('partials.alert')
        </div>
        <div class="text-center {{ $requireNotes ? 'text-danger mb-3' : 'text-primary' }}">
            {{ $actionMessage }} calon jamaah atas nama <b>{{ $customer->name }}</b> sebagai jamaah?
        </div>
        @if ($requireNotes)
            <label class="d-block required">Keterangan</label>
            <textarea name="notes" class="form-control" placeholder="Berikan keterangan / alasan" autocomplete="off" required></textarea>
        @endif
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm {{ $btnClass }}">
            <i class="fa-solid {{ $btnIcon }}"></i>
            {{ $btnText }}
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Tutup
        </button>
    </div>
</form>
