@php
    $isScript = (isset($mode) && ($mode === 'script'));
    $selectionBranch = (!isset($selectedBranch) || empty($selectedBranch));
@endphp
@if ($isScript)
    <script>
        let select_branch;
        @if ($selectionBranch)
            let select_province, select_city;
            let province_name = '';
            let city_name = '';
            
            function renderBranches()
            {
                select_branch.html('<option value="" data-branch-address="" data-branch-contact="">-- Pilih Cabang --</option>');

                $.get({
                    url: '{{ route("selectBranchByArea") }}',
                    data: {
                        province: province_name,
                        city: city_name
                    }
                }).done(function(respon) {
                    select_branch.html(respon);
                });
            }
        @endif
        
        $(function() {
            select_branch = $('#branch-id');
            @if ($selectionBranch)
                select_province = $('#branch-province');
                select_city = $('#branch-city');
                select_province.select2({
                    theme: 'classic',
                    placeholder: '-- Pilih Propinsi --',
                    allowClear: true,
                    ajax: {
                        url: function(params) {
                            params.current = select_province.val();
                            
                            return '{{ route("selectProvince") }}';
                        },
                        data: function (params) {
                            let dt = {
                                search: params.term,
                                current: params.current
                            };

                            return dt;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                }).on('select2:select', function (e) {
                    const data = e.params.data;
                    city_name = '';
                    province_name = data.text;
                    select_city.empty().trigger({type: 'select2:clear'});
                }).on('select2:clear', function(e) {
                    city_name = '';
                    province_name = '';
                    select_city.empty().trigger({type: 'select2:clear'});
                });

                select_city.select2({
                    theme: 'classic',
                    placeholder: '-- Pilih Kota / Kab --',
                    allowClear: true,
                    ajax: {
                        url: function(params) {
                            params.parent = select_province.val();
                            params.current = select_city.val();
                            
                            return '{{ route("selectCity") }}';
                        },
                        data: function (params) {
                            let dt = {
                                search: params.term,
                                current: params.current,
                                parent: params.parent,
                            };

                            return dt;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                }).on('select2:select', function (e) {
                    const data = e.params.data;
                    city_name = data.text;
                    renderBranches();
                }).on('select2:clear', function(e) {
                    city_name = '';
                    renderBranches();
                });
            @endif

            select_branch.on('change', function(e) {
                const me = $(this);
                const opt = me.is('SELECT') ? $(':selected', me) : me;
                const leaderName = opt.data('branch-leader-name');
                
                $('#branch-address').html(opt.data('branch-address'));
                $('#leader-name').html(leaderName);
                $('#leader-phone').html(opt.data('branch-leader-phone'));
                $('#admin-name').html(opt.data('branch-admin-name'));
                $('#admin-phone').html(opt.data('branch-admin-phone'));

                const leaderContainer = $('#branch-leader-container');
                if (leaderName) {
                    leaderContainer.removeClass('d-none');
                } else {
                    leaderContainer.addClass('d-none');
                }

                const info = $('#branch-info');
                if (me.val()) {
                    info.removeClass('d-none');
                } else {
                    info.addClass('d-none');
                }
            }).change();
        });
    </script>
@else
    @if ($selectionBranch)
        <div class="fw-bold mb-2">Pilihan Cabang</div>
    @endif
    <div class="row g-2 mb-2">
        @if ($selectionBranch)
            <div class="col-md-6">
                <label class="d-block">Propinsi</label>
                <select class="form-select select2bs4 select2-custom" id="branch-province"></select>
            </div>
            <div class="col-md-6" id="branch-city-container">
                <label class="d-block">Kota / Kabupaten</label>
                <select class="form-select select2bs4 select2-custom" id="branch-city"></select>
            </div>
            <div class="col-12">
                <label class="d-block required">Cabang Perwakilan</label>
                <select name="branch_id" id="branch-id" class="form-select" autocomplete="off" required>
                    <option value="" data-branch-address="" data-branch-contact="">-- Pilih Cabang --</option>
                </select>
            </div>
        @endif
        <div class="col-12 d-none" id="branch-info">
            <div class="d-block bg-light p-2 rounded-3">
                @if (!$selectionBranch)
                    <div class="mb-2">
                        <label class="d-block required">Cabang Perwakilan</label>
                        <input type="hidden" id="branch-id" value="{{ $selectedBranch->id }}" data-branch-address="{{ $selectedBranch->complete_address }}" data-branch-leader-name="{{ $selectedBranch->leader_name }}" data-branch-leader-phone="{{ $selectedBranch->leader_phone }}" data-branch-admin-name="{{ optional($selectedBranch->admin)->name }}" data-branch-admin-phone="{{ optional($selectedBranch->admin)->phone }}">
                        <input type="text" class="form-control" value="{{ $selectedBranch->name }}" readonly>
                    </div>
                @endif
                <div class="mb-2" id="branch-leader-container">
                    <div class="fw-bold">Pimpinan:</div>
                    <div class="d-block">
                        <span class="d-inline-block me-2" id="leader-name"></span>
                        <span class="d-inline-block" id="leader-phone"></span>
                    </div>
                </div>
                <div class="mb-2">
                    <div class="fw-bold">Admin KP:</div>
                    <div class="d-block">
                        <span class="d-inline-block me-2" id="admin-name"></span>
                        <span class="d-inline-block" id="admin-phone"></span>
                    </div>
                </div>
                <div class="fw-bold">Alamat:</div>
                <div id="branch-address"></div>
            </div>
        </div>
    </div>
@endif