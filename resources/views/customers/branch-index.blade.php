@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Jamaah'],
    'docTitle' => 'Jamaah'
])

@section('content')
@include('partials.alert')
<div class="d-block small">
    <table class="table table-sm table-nowrap table-hover table-striped" id="table">
        <thead class="text-center align-middle bg-gray-100 bg-gradient">
            <tr>
                <th>Jamaah</th>
                <th class="border-start">Program</th>
                <th class="border-start">Agen</th>
                <th class="border-start">Email</th>
                <th class="border-start">Handphone</th>
                <th class="border-start">Status</th>
                <th class="border-start"></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="col-auto d-none" id="button-control">
    <div class="btn-group">
        <button type="button" class="btn btn-sm btn-glow" title="Refresh" onclick="refreshTable();">
            <i class="fa-solid fa-rotate" style="line-height: inherit"></i>
        </button>
    </div>
</div>
<div class="col-auto d-none" id="filter-control">
    <div class="input-group input-group-sm">
        <label for="filter-status" class="input-group-text">Status</label>
        <select id="filter-status" class="form-select">
            <option value="-1">-- Semua --</option>
            @foreach (customerStatusList($user) as $key => $value)
                <option value="{{ $key }}" @optionSelected($key, $currentStatus)>{{ $value[0] }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    let table;

    function refreshTable()
    {
        table.ajax.reload();
    }

    $(function() {
        table = $('#table').DataTable({
            dom: '<"row justify-content-between g-2 mb-2"<"col-auto"<"#dt-control.row g-2"<"col-auto"l>>><"col-auto"f>>r<"table-responsive border mb-3"t><"datatable-paging-info row g-2"<"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-start"i><"col-md-6 d-flex align-items-center justify-content-center justify-content-sm-end"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("customer.dataTable") }}',
                data: function(d) {
                    d.status_id = $('#filter-status').val()
                }
            },
            deferLoading: 50,
            search: {
                return: true
            },
            pagingType: 'full_numbers',
            lengthChange: false,
            language: {!! json_encode(__('datatable')) !!},
            columnDefs: [
                {orderable: false, searchable: false, className: 'dt-body-center', targets: [5, 6]},
            ],
            columns: [
                {data: 'name'},
                {data: 'package_name'},
                {data: 'agency_name'},
                {data: 'email'},
                {data: 'phone'},
                {data: 'status'},
                {data: 'view'},
            ],
        });

        $('#table_processing.dataTables_processing')
        .removeClass('card')
        .html('<div><i class="fa-solid fa-spinner fa-spin fs-3"></i><span class="text-center fa-beat-fade mt-2">Processing...</span></div>');

        const fc = $('#button-control');
        $('#dt-control').prepend(fc);
        fc.removeClass('d-none').after($('#filter-control').removeClass('d-none'));

        $('#filter-status').on('change', function(e) {
            refreshTable();
        }).change();
    });
</script>
@endpush
