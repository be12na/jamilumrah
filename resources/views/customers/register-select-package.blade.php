@php
    $packages = \App\Models\Package::query()->byActive()->orderBy('price', 'desc')->get();
    $isScript = (isset($mode) && ($mode === 'script'));
@endphp
@if ($isScript)
    <script>
        $(function() {
            $('#branch-package').on('change', function(e) {
                const me = $(this);
                const opt = $(':selected', me);
                const pkgCont = $('#description-container');
                pkgCont.removeClass('d-none');
                $('#package-price').html(opt.data('package-price'));

                const info = $('#package-info');
                if (me.val()) {
                    info.removeClass('d-none');

                    const des = opt.data('package-description');
                    $('#package-description').html(des);
                    if (!des) {
                        pkgCont.addClass('d-none');
                    }
                } else {
                    info.addClass('d-none');
                }
            }).change();
        });
    </script>
@else
    <div class="mb-3">
        <label class="d-block required">Paket Program</label>
        <select name="package_id" id="branch-package" class="form-select" autocomplete="off" required>
            <option value="">-- Pilih Paket Program --</option>
            @foreach ($packages as $package)
                <option value="{{ $package->id }}" data-package-description="{{ $package->description }}" data-package-price="Rp @formatNumber($package->price, 0)">{{ $package->name }}</option>
            @endforeach
        </select>
        <div class="mt-2 d-none bg-light p-2 rounded-3" id="package-info">
            <div class="d-block mb-2">
                <span class="d-inline-block me-2 fw-bold">Harga:</span>
                <span class="d-inline-block" id="package-price"></span>
            </div>
            <div id="description-container">
                <div class="fw-bold">Keterangan:</div>
                <div class="d-block" id="package-description"></div>
            </div>
        </div>
    </div>
@endif