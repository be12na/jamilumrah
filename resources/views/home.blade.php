@php
    $user = isset($user) ? $user : Auth::user();
@endphp

@if ($user->main_user)
    @include('dashboard.main', ['user' => $user])
@elseif ($user->branch_user)
    @include('dashboard.branch', ['user' => $user])
@elseif ($user->agency_user)
    @include('dashboard.agency', ['user' => $user])
@endif
