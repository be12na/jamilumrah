@extends('layouts.app-forgot', [
    'docTitle' => 'Lupa Password'
])

@section('mainCssClass', 'd-flex flex-column align-items-center pt-3 pt-sm-4 pt-sm-5')

@section('content')
<div class="container">
    <div class="row g-0 justify-content-center">
        <div class="col-sm-8 col-md-6">
            <div class="bg-light border border-secondary rounded-3 p-3 p-lg-4">
                <div class="border-bottom text-center mb-3 pb-2">
                    <a href="{{ route('login') }}">
                        <img src="{{ asset('images/brand-dark.png') }}" style="width: auto; height: auto; max-height: 60px;">
                    </a>
                </div>
                <form method="POST" action="{{ route('forgot.password.reset.save', ['forgotPasswordToken' => $user->password_token]) }}" data-alert-container="#forgot-container">
                    @csrf
                    <div class="d-block" id="forgot-container">
                        @include('partials.alert')
                    </div>
                    <div class="text-center fw-bold mb-3">Atur Ulang Password</div>
                    <div class="mb-2">
                        <label class="d-block">Password Baru</label>
                        <input type="password" name="password" class="form-control" placeholder="Password Baru" autocomplete="off" autofocus required>
                    </div>
                    <div class="mb-3">
                        <label class="d-block">Ketik Ulang Password</label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Ketik Ulang Password" autocomplete="off" required>
                    </div>
                    <div class="btn-group w-100 dual-button mb-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <div class="dual-separator">OR</div>
                        <a class="btn btn-secondary" href="{{ route('login') }}">Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
