@extends('layouts.app-forgot', [
    'docTitle' => 'Lupa Password'
])

@section('mainCssClass', 'd-flex flex-column align-items-center pt-3 pt-sm-4 pt-sm-5')

@section('content')
<div class="container">
    <div class="row g-0 justify-content-center">
        <div class="col-sm-8 col-md-6">
            <div class="bg-light border border-secondary rounded-3 p-3 p-lg-4">
                <div class="border-bottom text-center mb-3 pb-2">
                    <a href="{{ route('login') }}">
                        <img src="{{ asset('images/brand-dark.png') }}" style="width: auto; height: auto; max-height: 60px;">
                    </a>
                </div>
                <form method="POST" action="{{ route('forgot.password.submit') }}" data-alert-container="#forgot-container">
                    @csrf
                    <div class="d-block" id="forgot-container">
                        @include('partials.alert')
                    </div>
                    <div class="mb-2">
                        <label class="d-block">Alamat Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Alamat Email" autocomplete="off" autofocus required>
                    </div>
                    <div class="mb-3">
                        <label class="d-block">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" required>
                    </div>
                    <div class="btn-group w-100 dual-button mb-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <div class="dual-separator">OR</div>
                        <a class="btn btn-secondary" href="{{ route('login') }}">Login</a>
                    </div>
                    <div class="text-center small">
                        Kami akan mengirimkan tautan untuk mengatur ulang password anda melalui email.
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
