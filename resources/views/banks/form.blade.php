<form class="modal-content" method="POST" action="{{ $postUrl }}" data-alert-container="#alert-from-container" data-onfocus="#account_no">
    @include('partials.modals.modal-header', ['modalTitle' => $modalTitle])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        @if ($formMode == 'create')
            <div class="row g-2">
                <div class="col-12">
                    <label class="d-block required">Bank</label>
                    <select name="bank_id" class="form-select" autocomplete="off" required>
                        <option value="">-- Pilih Bank --</option>
                        @foreach ($banks as $bank)
                            <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-12">
                    <label class="d-block required">No. Rekening</label>
                    <input type="text" name="account_no" id="account_no" class="form-control" autocomplete="off" required>
                </div>
                <div class="col-12">
                    <label class="d-block required">Pemilik Rekening</label>
                    <input type="text" name="account_name" class="form-control" autocomplete="off" required>
                </div>
            </div>
        @else
            <div class="d-block text-center">
                <div class="mb-3">
                    <img src="{{ $userBank->bank->logo_url }}" alt="Logo" style="height: 50px;">
                </div>
                <div class="fw-bold">{{ $userBank->bank_name }}</div>
                <div>{{ $userBank->account_no }}</div>
                <div>{{ $userBank->account_name }}</div>
                <div class="mt-3 fw-bold">{{ ($formMode == 'enable') ? '' : 'Non ' }} Aktifkan rekening bank ?</div>
            </div>
        @endif
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-{{ ($formMode == 'disable') ? 'danger' : 'primary' }}">
            <i class="fa-solid fa-{{ ($formMode == 'create') ? 'save' : (($formMode == 'enable') ? 'circle-check' : 'ban') }}"></i>
            {{ ($formMode == 'create') ? 'Simpan' : ((($formMode == 'enable') ? '' : 'Non ') . 'Aktifkan') }}
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa-solid fa-times"></i>
            Batal
        </button>
    </div>
</form>