@php
    $user = isset($user) ? $user : Auth::user();
    $banks = $user->user_banks;
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => ['Rekening Bank'],
    'docTitle' => 'Rekening Bank'
])

@section('content')
@include('partials.alert')

@canBankAction('create', $user)
<div class="d-block mb-2">
    <button type="button" class="btn btn-sm btn-glow" data-bs-toggle="modal" data-bs-target="#my-modal" data-modal-url="{{ route('bank.create') }}">
        <i class="fa-solid fa-plus"></i>
        Tambah
    </button>
</div>
@endcanBankAction

<table class="table mb-2 fs-auto" id="table">
    <thead class="d-none">
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($banks as $bank)
            <tr>
                <td>
                    <div class="row g-2">
                        <div class="col-lg-2 position-relative">
                            <div class="text-center text-sm-start">
                                <img src="{{ $bank->bank->logo_url }}" alt="Logo" style="height:auto; width:auto; max-width:80%; max-height:50px;">
                            </div>
                        </div>
                        @php
                            $color = !$bank->is_active ? 'text-danger' : '';
                        @endphp
                        <div class="col-lg-10 text-center text-sm-start ">
                            <div class="{{ $color }}">
                                <div class="fw-bold">{{ $bank->bank_name }}</div>
                                <div>{{ $bank->account_no }}</div>
                                <div>{{ $bank->account_name }}</div>
                            </div>
                            @if (!$bank->is_active)
                                @canBankAction('enable', $user)
                                <div class="mt-1">
                                    <a href="javascript:;" class="link-primary" data-bs-toggle="modal" data-bs-target="#my-modal" data-modal-url="{{ route('bank.enable', ['userBank' => $bank->id]) }}">Aktifkan</a>
                                </div>
                                @endcanBankAction
                            @else
                                @if ($user->main_user)
                                    @canBankAction('disable', $user)
                                    <div class="mt-1">
                                        <a href="javascript:;" class="link-secondary" data-bs-toggle="modal" data-bs-target="#my-modal" data-modal-url="{{ route('bank.disable', ['userBank' => $bank->id]) }}">Non Aktifkan</a>
                                    </div>
                                    @endcanBankAction
                                @endif
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@if (canBankAction('create', $user) || canBankAction('enable', $user) || canBankAction('disable', $user))
@push('topContent')
    @include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true])
@endpush
@endif

@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">
@endpush

@push('styles')
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    $(function() {
        $('#table').DataTable({
            dom: 'lfr<"table-responsive"t>ip',
            processing: false,
            serverSide: false,
            searching: false,
            ordering: false,
            paging: false,
            info: false,
            lengthChange: false,
            language: {!! json_encode(__('datatable')) !!},
        });
    });
</script>
@endpush
