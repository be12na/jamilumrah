@php
    $user = isset($user) ? $user : Auth::user();
    $isEdit = isset($isEdit) ? $isEdit : false;
    $breadCrumbItems = ['Profile'];
    if ($isEdit === true) {
        $breadCrumbItems[] = 'Edit';
    }
@endphp

@extends('layouts.app', [
    'user' => $user,
    'breadCrumbItems' => $breadCrumbItems,
    'docTitle' => 'Profile'
])

@section('content')
<div class="d-block" id="alert-container">
    @include('partials.alert')
</div>
@if ($isEdit === true)
    <form class="card mb-3 fs-auto" method="POST" action="{{ route('profile.update') }}" data-alert-container="#alert-container">
        <div class="card-body">
            @csrf
            <div class="row g-2">
                <div class="col-md-6">
                    <label class="d-block required">No. Identitas</label>
                    <input type="text" class="form-control" name="identity" value="{{ $user->identity }}" autocomplete="off" required autofocus>
                </div>
                <div class="col-md-6">
                    <label class="d-block required">Nama</label>
                    <input type="text" class="form-control" name="name" value="{{ $user->name }}" autocomplete="off" required>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <label class="d-block required">Jns. Kelamin</label>
                    <select class="form-select" name="gender" autocomplete="off" required>
                        <option value="">-- Jns. Kelamin --</option>
                        @foreach (GENDERS as $key => $gender)
                            <option value="{{ $key }}" @optionSelected($key, $user->gender)>{{ $gender }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <label class="d-block required">Tgl. Lahir</label>
                    @include('partials.datepicker', [
                        'dateId' => 'birth-date',
                        'dateName' => 'birth_date',
                        'dateValue' => $user->birth_date,
                        'endDate' => \Carbon\Carbon::today(),
                        'required' => true,
                    ])
                </div>
                <div class="col-12">
                    <label class="d-block required">Alamat</label>
                    <input type="text" class="form-control" name="address" value="{{ $user->address }}" autocomplete="off" required>
                </div>
                <div class="col-12">
                    <label class="d-block required">Pilih Daerah</label>
                    <select class="form-select select2bs4 select2-custom" name="village_id" id="daerah-id" autocomplete="off" required>
                        @if (!empty($user->village_id))
                            <option value="{{ $user->village_id }}">
                                {{ $user->village_name }}, Kec. {{ $user->district_name }}, {{ $user->city_name }}, {{ $user->province_name }}
                            </option>
                        @endif
                    </select>
                    <div class="d-block d-sm-none" id="daerah-text"></div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-2">
                    <label class="d-block">Kode Pos</label>
                    <input type="text" name="pos_code" class="form-control" value="{{ $user->pos_code }}" placeholder="Kode Pos" autocomplete="off">
                </div>
                <div class="col-sm-6 col-md-8 col-lg-4">
                    <label class="d-block required">Handphone</label>
                    <input type="text" name="phone" class="form-control" value="{{ $user->phone }}" placeholder="Handphone" autocomplete="off">
                </div>
                <div class="col-lg-6">
                    <label class="d-block required">Email</label>
                    <input type="text" name="email" class="form-control" value="{{ $user->email }}" placeholder="Email" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                <i class="fa-solid fa-save me-1"></i>
                Simpan
            </button>
            @if ($user->has_profile)
                <button type="button" class="btn btn-sm btn-warning" data-href="{{ route('profile.index') }}">
                    <i class="fa-solid fa-undo me-1"></i>
                    Batal
                </button>
            @endif
        </div>
    </form>

    @include('partials.guides.guide-list', [
        'guideList' => [
            view('partials.guides.required')->render(),
            view('partials.guides.select-area')->render(),
            view('partials.guides.handphone')->render(),
        ],
    ])
@else
    <div class="card fs-auto">
        <div class="card-header">
            <button type="button" class="btn btn-sm btn-primary" data-href="{{ route('profile.edit') }}">
                <i class="fa-solid fa-pencil me-1"></i>Edit
            </button>
        </div>
        <div class="card-body">
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">No. Identitas</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->identity }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Nama</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->name }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Jns. Kelamin</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->gender_name }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Tgl. Lahir</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">@translateDatetime($user->birth_date, 'd F Y')</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Alamat</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->address }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Desa / Kelurahan</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->village_name }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Kecamatan</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->district_name }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Kota / Kabupaten</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->city_name }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Propinsi</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->province_name }}</div>
                </div>
            </div>
            <div class="row g-1 mb-2">
                <div class="col-auto flex-basis basis-120px basis-sm-150px d-flex justify-content-between">
                    <span class="pe-2">Kode Pos</span><span>:</span>
                </div>
                <div class="col-auto">
                    <div class="ps-3 ps-sm-0">{{ $user->pos_code ?? '-' }}</div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

@if ($isEdit === true)
@push('vendorCSS')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker-1.9.0/css/bootstrap-datepicker.standalone.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
@endpush

@push('vendorJS')
<script src="{{ asset('vendor/bootstrap-datepicker-1.9.0/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-datepicker-1.9.0/locales/bootstrap-datepicker.id.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/i18n/id.js') }}"></script>
@endpush

@push('scripts')
<script>
    let select_daerah;
    $(function() {
        select_daerah = $('#daerah-id');
        select_daerah.select2({
            theme: 'classic',
            placeholder: '-- Pilih Daerah --',
            allowClear: true,
            ajax: {
                url: function(params) {
                    params.current = select_daerah.val();
                    
                    return '{{ route("selectRegion") }}';
                },
                data: function (params) {
                    let dt = {
                        search: params.term,
                        current: params.current
                    };

                    return dt;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        }).on('select2:select', function (e) {
            const data = e.params.data;
            const txt = $('#daerah-text');

            txt.html(data.text);

            if (data.text != '') {
                txt.addClass('mt-2');
            } else {
                txt.removeClass('mt-2');
            }
        });
    });
</script>
@endpush
@endif