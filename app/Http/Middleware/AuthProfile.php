<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (!hasProfile($user)) {
            $message = 'Anda belum melengkapi profile anda. Silahkan lengkapi profile anda terlebih dahulu untuk dapat melakukan proses lebih lanjut.';

            if ($request->ajax()) {
                $route = route('profile.edit');

                $message = "<div><span class=\"fw-bold text-danger me-2\">{$message}</span><a href=\"{$route}\" class=\"link-primary\">Edit Profile</a>.</div>";
                return ajaxError($message, 403);
            }

            return pageError(
                $message,
                'profile.edit',
                [],
                'warning'
            );
        }

        return $next($request);
    }
}
