<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthHasBank
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if (!$user->has_active_bank) {
            $message = 'Anda tidak memiliki rekening bank yang aktif.';

            if ($request->ajax()) {
                $route = route('bank.index');

                $message = "<div><span class=\"fw-bold text-danger me-2\">{$message}</span><a href=\"{$route}\" class=\"link-primary\">Bank</a>.</div>";
                return ajaxError($message, 403);
            }

            return pageError(
                $message,
                'bank.index',
                [],
                'warning'
            );
        }

        return $next($request);
    }
}
