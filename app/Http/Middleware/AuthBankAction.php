<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthBankAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $action)
    {
        $user = $request->user();
        if (!canBankAction($action, $user)) {
            if ($request->ajax()) {
                return ajaxError('<h5 class="text-danger">' . MESSAGE_403 . '</h5>', 403);
            }

            return pageError(MESSAGE_403, 'dashboard');
        }

        return $next($request);
    }
}
