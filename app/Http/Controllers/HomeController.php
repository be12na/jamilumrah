<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Dashboard\AgencyTrait;
use App\Http\Controllers\Traits\Dashboard\BranchTrait;
use App\Http\Controllers\Traits\Dashboard\MainTrait;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use MainTrait, BranchTrait, AgencyTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->route()->getName() == 'home') {
            return redirect()->route(auth()->check() ? 'dashboard' : 'login');
        }

        return view('home', ['user' => $request->user()]);
    }

    public function welcome()
    {
        return redirect()->route(auth()->check() ? 'dashboard' : 'login');
    }

    public function getData(Request $request)
    {
        $user = $request->user();

        if ($user->main_user) {
            return $this->dataMainDashboard($request);
        } elseif ($user->branch_user) {
            return $this->dataBranchDashboard($request);
        }

        return $this->dataAgencyDashboard($request);
    }
}
