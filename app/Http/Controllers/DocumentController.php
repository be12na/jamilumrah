<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        return view('document.index', [
            'user' => $user,
            'documents' => Document::query()->orderBy('name')->get(),
        ]);
    }

    public function upload(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return view('document.form');
        }

        $values = $request->except(['_token']);

        $validator = Validator::make($values, [
            'name' => ['required', 'string', 'max:100', 'unique:documents,name'],
            'doc_file' => ['required', 'file', 'mimetypes:application/pdf,application/msword,application/vnd.ms-office,application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'max:1024'],
        ], [
            'doc_file.mimetypes' => ':attribute yang dapat di-upload hanya file dengan format PDF dan MS-Word (.doc, .docx).',
            'doc_file.max' => ':attribute tidak boleh lebih dari :max kb (1 mb).',
        ], [
            'name' => 'Nama Dokumen',
            'doc_file' => 'File Dokumen',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator, 'danger', 'Proses Upload Gagal.'), 400);
        }

        $isUploaded = false;
        $uploadedFile = null;
        $code = uniqid();

        DB::beginTransaction();
        try {
            $file = $request->file('doc_file');
            $ext = $file->extension();
            $filename = $code . ".{$ext}";
            $file->storePubliclyAs('/', $filename, Document::DOC_DISK);
            $values['code'] = $code;
            $values['filename'] = $filename;
            $isUploaded = true;
            $uploadedFile = $filename;

            Document::create($values);

            session([
                'message' => "Dokumen telah berhasil diunggah.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('document.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            if ($isUploaded && !empty($uploadedFile)) {
                Storage::disk(Document::DOC_DISK)->delete($uploadedFile);
            }

            return response(errorMessageContent($msg), 500);
        }
    }

    public function remove(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return view('document.delete', [
                'document' => $request->document,
            ]);
        }

        $document = $request->document;
        $filename = $document->filename;

        DB::beginTransaction();
        try {
            $document->delete();
            Storage::disk(Document::DOC_DISK)->delete($filename);

            DB::commit();

            session([
                'message' => "Dokumen telah berhasil diunggah.",
                'messageClass' => 'success'
            ]);

            return response(route('document.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();

            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
