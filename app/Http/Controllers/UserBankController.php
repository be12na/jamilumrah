<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\UserBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\Rules\Unique;

class UserBankController extends Controller
{
    private string $cantDisable = 'Anda hanya dapat membuat rekening baru untuk menonaktifkan rekening bank yang sudah terdaftar.';
    private string $mustHaveOne = 'Harus ada 1 rekening bank yang aktif.';

    public function index(Request $request)
    {
        return view('banks.index', [
            'user' => $request->user()
        ]);
    }

    public function create(Request $request)
    {
        return view('banks.form', [
            'user' => $request->user(),
            'banks' => Bank::query()->byActive(true)->orderBy('code')->get(),
            'modalTitle' => 'Tambah Rekening Bank',
            'formMode' => 'create',
            'postUrl' => route('bank.store'),
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $values = $request->except(['_token']);
        $userBankUserId = $user->user_bank_user_id;
        $bankType = $user->user_bank_type;
        $uniqueAccount = new Unique('user_banks', 'account_no');
        $uniqueAccount = $uniqueAccount
            ->where('user_id', $userBankUserId)
            ->where('bank_type', $bankType)
            ->where('bank_id', $values['bank_id']);
        $bankExists = new Exists('banks', 'id');
        $bankExists = $bankExists->where('is_active', 1);

        $validator = Validator::make($values, [
            'bank_id' => ['required', $bankExists],
            'account_no' => ['required', 'max:50', $uniqueAccount],
            'account_name' => ['required', 'string', 'max:100'],
        ], [], [
            'bank_id' => 'Bank',
            'account_no' => 'No. Rekening',
            'account_name' => 'Nama Pemilik',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator, 'danger'), 400);
        }

        $selectedBank = Bank::query()->byId($values['bank_id'])->byActive()->first();
        $values['bank_type'] = $bankType;
        $values['user_id'] = $userBankUserId;
        $values['bank_name'] = $selectedBank->name;
        $values['is_active'] = true;

        $userBanks = $user->user_banks;
        $disabledIds = [];

        if (($user->agency_user || $user->branch_user) && $userBanks->isNotEmpty()) {
            $disabledIds = $userBanks->pluck('id')->toArray();
        }

        DB::beginTransaction();
        try {
            if (!$user->main_user && !empty($disabledIds)) {
                DB::table('user_banks')->whereIn('id', $disabledIds)->update(['is_active' => false]);
            }

            UserBank::create($values);

            DB::commit();

            session([
                'message' => "Rekening bank berhasil ditambahkan.",
                'messageClass' => 'success',
            ]);

            return response(route('bank.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function edit(Request $request)
    {
        $user = $request->user();
        $routeName = $request->route()->getName();
        $isEnable = ($routeName == 'bank.enable');
        $userBanks = $user->user_banks;
        $userBank = $request->userBank;

        // cant disable
        if (($user->agency_user || $user->branch_user) && !$isEnable) {
            return response('<h4 class="text-center">' . $this->cantDisable . '</h4>', 403);
        }

        // must have one active account
        if ($user->main_user && !$isEnable) {
            $countAccount = $userBanks->count();
            $countDisabled = $userBanks->where('is_active', '=', false)->count();

            if ($countAccount - 1 == $countDisabled) {
                return response('<h4 class="text-center">' . $this->mustHaveOne . '</h4>', 400);
            }
        }

        $formMode = $isEnable ? 'enable' : 'disable';
        $title = $isEnable ? '' : ' Non ';

        return view('banks.form', [
            'user' => $user,
            'userBank' => $userBank,
            'modalTitle' => "{$title}Aktifkan Rekening Bank",
            'formMode' => $formMode,
            'postUrl' => route($routeName . 'd', ['userBank' => $userBank->id]),
        ]);
    }

    public function update(Request $request)
    {
        $userBank = $request->userBank;
        $isEnable = ($request->route()->getName() == 'bank.enabled');
        $msg = $isEnable ? 'diaktifkan' : 'dinonaktifkan';

        // nothing process
        if (($isEnable && $userBank->is_active) || (!$isEnable && !$userBank->is_active)) {
            session([
                'message' => "Rekening bank berhasil {$msg}.",
                'messageClass' => 'success',
            ]);

            return response(route('bank.index'), 200);
        }

        $user = $request->user();

        // cant disable
        if (($user->agency_user || $user->branch_user) && !$isEnable) {
            return response(errorMessageContent($this->cantDisable), 403);
        }

        $userBanks = $user->user_banks;

        // must have one active account
        if ($user->main_user && !$isEnable) {
            $countAccount = $userBanks->count();
            $countDisabled = $userBanks->where('is_active', '=', false)->count();

            if ($countAccount - 1 == $countDisabled) {
                return response(errorMessageContent($this->mustHaveOne), 400);
            }
        }

        $userBankUserIds = $user->main_user ? [] : $userBanks->whereNotIn('id', [$userBank->id])->pluck('id')->toArray();

        $values = [
            'is_active' => $isEnable,
        ];

        DB::beginTransaction();
        try {
            if ($user->main_user) {
                $userBank->update($values);
            } else {
                if (!empty($userBankUserIds) && $isEnable) {
                    DB::table('user_banks')->whereIn('id', $userBankUserIds)->update([
                        'is_active' => false,
                    ]);
                }

                $userBank->update($values);
            }

            DB::commit();

            session([
                'message' => "Rekening bank berhasil {$msg}.",
                'messageClass' => 'success',
            ]);

            return response(route('bank.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
