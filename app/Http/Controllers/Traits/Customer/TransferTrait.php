<?php

namespace App\Http\Controllers\Traits\Customer;

use App\Models\UserBank;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

trait TransferTrait
{
    private function getActiveMainBanks(): EloquentCollection
    {
        return UserBank::query()->byMain()->byActive()->orderBy('is_active', 'desc')->orderBy('bank_name')->get();
    }

    public function transfer(Request $request)
    {
        $user = $request->user();
        $customer = $request->customerTransferable;
        $mainBanks = $this->getActiveMainBanks();

        return view('customers.transfer', [
            'user' => $user,
            'customer' => $customer,
            'mainBanks' => $mainBanks,
        ]);
    }

    public function saveTransfer(Request $request)
    {
        $user = $request->user();
        $customer = $request->customerTransferable;
        $values = $request->except(['_token']);

        // $uploadRequire = true;
        // if ($customer->status == CUSTOMER_STATUS_REJECTED) {
        //     $uploadRequire = ($values['payment_method'] != $customer->payment_method);
        // }

        $mainBanks = $this->getActiveMainBanks();
        $mainBankIds = $mainBanks->isNotEmpty() ? $mainBanks->pluck('id')->toArray() : [-99999999999];

        $validator = Validator::make($values, [
            'payment_method' => ['required', 'in:' . implode(',', array_keys(PAYMENT_METHODS))],
            'main_bank_id' => ['required', 'in:' . implode(',', $mainBankIds)],
            'payment_note' => ['nullable', 'string', 'max:250'],
            // 'image' => [$uploadRequire ? 'required' : 'nullable', 'image', 'mimetypes:image/jpeg,image/png', 'max:512'],
        ], [
            'image.required' => ':attribute harus dilampirkan.',
        ], [
            'payment_method' => 'Jenis Pembayaran',
            'main_bank_id' => 'Rekening Tujuan',
            'payment_note' => 'Keterangan Pembayaran',
            'image' => 'Bukti Transfer',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator, 'danger', 'Proses Upload Gagal.'), 400);
        }

        $isUploaded = false;
        $uploadedFile = null;
        $oldImage = $customer->payment_image;

        DB::beginTransaction();
        try {
            // if ($uploadRequire || $request->hasFile('image')) {
            //     $file = $request->file('image');
            //     $ext = $file->extension();
            //     $filename = uniqid() . ".{$ext}";
            //     $file->storePubliclyAs('/', $filename, $customer->image_disk);
            //     $values['payment_image'] = $filename;

            //     $isUploaded = true;
            //     $uploadedFile = $filename;
            // }

            $values['status'] = CUSTOMER_STATUS_TRANSFER;
            $values['transfer_by'] = $user->id;
            $values['transfer_at'] = date('Y-m-d H:i:s');

            $customer->update($values);

            if (!empty($oldImage) && $isUploaded) {
                Storage::disk($customer->image_disk)->delete($oldImage);
            }

            session([
                'message' => "Bukti transfer Jamaah <b>{$customer->name}</b> telah berhasil dilampirkan.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.detail', ['customer' => $customer->id]), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            if ($isUploaded && !empty($uploadedFile)) {
                Storage::disk($customer->image_disk)->delete($uploadedFile);
            }

            return response(errorMessageContent($msg), 500);
        }
    }
}
