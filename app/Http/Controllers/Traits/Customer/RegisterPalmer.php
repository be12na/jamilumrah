<?php

namespace App\Http\Controllers\Traits\Customer;

// use App\Mail\InfoRegistrationToAgencyMail;
// use App\Mail\InfoRegistrationToBranchMail;
// use App\Mail\RegistrationCustomerMail;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\Package;
use App\Models\User;
use App\Notifications\Register\AgencyNotification;
use App\Notifications\Register\MainNotification;
use App\Notifications\Register\PalmerNotification;
use App\Repositories\RegionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

trait RegisterPalmer
{
    private function landingToBePalmer(Request $request)
    {
        $param = [];
        if ($request->registerPalmerByToken) {
            $param = ['registerPalmerByToken' => $request->registerPalmerByToken->username];
        }

        $urlRegister = route('viewToBePalmer', $param);

        return view('landing-register-customer', [
            'urlRegister' => $urlRegister,
        ]);
    }

    public function toBePalmer(Request $request)
    {
        $agency = $request->registerPalmerByToken;
        $sessValue = $agency ? $agency->username : -1;
        $landing = session('landing-passed');

        if ((empty($landing) || ($landing != $sessValue)) && !empty($agency)) {
            session(['landing-passed' => $sessValue]);

            return $this->landingToBePalmer($request);
        }

        if (empty($agency)) {
            $agency = User::query()->byMainUserType(USER_MAIN_SUPER)->orderBy('id')->first();
        }

        $branches = collect();
        $selectedBranch = null;

        if ($agency->main_user || $agency->agency_user) {
            $branches = Branch::query()
                ->byActive()
                ->whereHas('admin')
                ->orderBy('name')
                ->get();
        } else {
            $selectedBranch = $agency->branch;
        }

        $packages = Package::query()->byActive()->orderBy('price', 'desc')->get();

        if (strtolower($request->method()) != 'post') {
            return view('customers.register-palmer', [
                'agency' => $agency,
                'branches' => $branches,
                'packages' => $packages,
                'selectedBranch' => $selectedBranch,
            ]);
        }

        $values = $request->except(['_token']);
        $rules = [];

        $branchEmail = '';

        if ($agency->branch_user) {
            $values['branch_id'] = $agency->branch_id;
            $branch = $selectedBranch;
            $branchEmail = $agency->email;
        } else {
            $branches = Branch::query()->byActive(true)->get();
            $branchIds = $branches->isNotEmpty() ? $branches->pluck('id')->toArray() : [-999999];
            $branch = $branches->where('id', '=', $values['branch_id'])->first();
            $rules['branch_id'] = ['required', 'in:' . implode(',', $branchIds)];

            $branchEmail = $branch->admin->email;
        }

        if ($agency->main_user) {
            $agency = $branch->admin;
        }

        $packages = Package::query()->byActive()->get();
        $packageIds = $packages->isNotEmpty() ? $packages->pluck('id')->toArray() : [-999999];
        $package = $packages->where('id', '=', $values['package_id'])->first();
        $timeBirth = translatedDateToTime($values['birth_date'] ?? '', ' ');

        $values['birth_date'] = $timeBirth ? date('Y-m-d', $timeBirth) : null;
        $values['rule_birth_date'] = $timeBirth ? date('d F Y', $timeBirth) : null;
        // efek cangkeul
        $values['is_wni'] = true; //isset($values['is_wni']);
        $values['smoking'] = false; //isset($values['smoking']);
        $values['school_id'] = SCHOOL_LEVEL_OTHER;
        $values['blood_id'] = BLOODS[0];

        $rules['package_id'] = ['required', 'in:' . implode(',', $packageIds)];
        $rules['identity'] = ['required', 'string', 'regex:/^[a-z0-9]/i', 'max:50', 'unique:customers,identity'];
        $rules['name'] = ['required', 'string', 'max:100'];
        $rules['gender'] = ['required', 'in:' . implode(',', array_keys(GENDERS))];
        $rules['birth_city'] = ['required', 'string', 'max:100'];
        $rules['rule_birth_date'] = ['required', 'date_format:d F Y'];
        $rules['email'] = ['required', 'email', 'max:100'];
        $rules['phone'] = ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'];
        $rules['address'] = ['required', 'string', 'max:250'];
        $rules['village_id'] = ['required', 'exists:villages,id'];
        // $rules['father_name'] = ['nullable', 'string', 'max:100'];
        // $rules['profession'] = ['nullable', 'string', 'max:100'];
        // $rules['school_id'] = ['required', 'in:' . implode(',', array_keys(SCHOOL_LEVELS))];
        // $rules['blood_id'] = ['required', 'in:' . implode(',', BLOODS)];
        // $rules['companion_id'] = ['required', 'in:' . implode(',', array_keys(COMPANION_RELATIONSHIP))];

        // if ($values['companion_id'] > COMPANION_NONE) {
        //     $rules['companion'] = ['required', 'string', 'max:100'];
        // } else {
        //     $values['companion'] = null;
        // }

        // if ($values['is_wni'] !== true) {
        //     $rules['wna_country'] = ['required', 'string', 'max:100'];
        // } else {
        //     $values['wna_country'] = null;
        // }

        $values['companion_id'] = COMPANION_NONE;
        $values['companion'] = null;
        $values['wna_country'] = null;

        $validator = Validator::make($values, $rules, [
            'phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'identity' => 'No. Identitas',
            'name' => 'Nama',
            'gender' => 'Jns. Kelamin',
            'rule_birth_date' => 'Tgl. Lahir',
            'address' => 'Alamat',
            'village_id' => 'Daerah',
            'pos_code' => 'Kode Pos',
            'email' => 'Email',
            'phone' => 'No. Handphone',
            'branch_id' => 'Kantor Perwakilan',
            'package_id' => 'Paket',
            'father_name' => 'Nama Ayah',
            'birth_city' => 'Tmp. Lahir',
            'wna_country' => 'Negara Asal',
            'school_id' => 'Pendidikan',
            'profession' => 'Pekerjaan / Profesi',
            'companion' => 'Nama Pendamping',
            'companion_id' => 'Pendamping',
            'blood_id' => 'Golongan Darah',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $village = optional(RegionRepository::getVillageById($values['village_id'], ['district', 'city', 'province']));

        $birthCarbon = Carbon::createFromFormat('Y-m-d', $values['birth_date']);
        $todayCarbon = Carbon::today();
        $values['age'] = $todayCarbon->diffInYears($birthCarbon);
        $values['code'] = Customer::makeCode();
        $values['agency_id'] = $agency->id;
        // $values['agency_detail'] = $agency->toArray();
        $values['branch'] = $branch;
        $values['package'] = $package;
        // $values['branch_detail'] = $branch ? $branch->toArray() : null;
        // $values['package_detail'] = $package ? $package->toArray() : null;
        $values['village_name'] = $village->name;
        $values['district_name'] = ($district = optional($village->district))->name;
        $values['city_name'] = ($city = optional($district->city))->name;
        $values['province_name'] = optional($city->province)->name;

        $sendEmailAgency = $agency->agency_user;
        $superadmin = User::query()->byUsername('superadmin')->first();

        DB::beginTransaction();
        try {
            $newCustomer = Customer::create($values);

            // email to new customer
            // Mail::to($values['email'])->send(new RegistrationCustomerMail($newCustomer));
            // email to branch admin
            // Mail::to($branchEmail)->send(new InfoRegistrationToBranchMail($newCustomer));
            // email to agency
            // if ($sendEmailAgency) {
            //     Mail::to($agency->email)->send(new InfoRegistrationToAgencyMail($newCustomer));
            // }

            // notification for customer
            $newCustomer->notify(new PalmerNotification($newCustomer, 'database', 'mail'));
            $newCustomer->notify(new PalmerNotification($newCustomer, 'database', 'onesender'));
            // notification for customer:end

            // notification for family center
            $superadmin->notify(new MainNotification($newCustomer, 'database', 'mail'));
            $superadmin->notify(new MainNotification($newCustomer, 'database', 'onesender'));
            // notification for family center:end

            // notification for agency
            if ($sendEmailAgency) {
                $agency->notify(new AgencyNotification($newCustomer, 'database', 'mail'));
                $agency->notify(new AgencyNotification($newCustomer, 'database', 'onesender'));
            }
            // notification for agency:end

            session([
                'message' => 'Selamat! Anda berhasil mendaftar sebagai jamaah kami. Pihak kami akan segera menghubungi anda. Terima Kasih.',
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('login'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
