<?php

namespace App\Http\Controllers\Traits\Customer;

use App\Libs\Tools\CsvImportPalmer;
use App\Libs\Tools\ExcelImportPalmer;
use App\Libs\Tools\ImportPalmerHelper;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\Package;
use App\Models\User;
use App\Repositories\RegionRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

trait ImportPalmer
{
    public function viewImport(Request $request)
    {
        $user = $request->user();
        $agencies = $this->getAgencies();

        return view('customers.form-import', [
            'user' => $user,
            'agencies' => $agencies,
        ]);
    }

    public function applyImport(Request $request)
    {
        if (!$request->hasFile('excel')) {
            return ajaxError('Tidak ada file yang diunggah.', 400);
        }

        $values = $request->only('excel');

        $validator = Validator::make($values, [
            // 'excel' => ['required', 'file', 'mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            'excel' => ['required', 'file', 'mimes:xlsx,csv'],
        ], [
            // 'excel.mimetypes' => ':attribute harus file Excel 2007-365 (.xlsx)'
            'excel.mimes' => ':attribute bukan file Excel 2007-365 (.xlsx) atau Text CSV (.csv)'
        ], [
            'excel' => 'File yang diunggah',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator, 'danger', 'Proses Upload Gagal.'), 400);
        }

        $file = $request->file('excel');
        $ext = strtolower($file->extension());
        $filePath = $file->getRealPath();

        $row = [];
        $emptyMessage = errorMessageContent('Tidak ada data yang dapat diimport', 'danger', 'Proses Upload Gagal.');


        if ($ext === 'xlsx') {
            $obj = new ExcelImportPalmer;
            $records = Excel::toArray($obj, $filePath, null, \Maatwebsite\Excel\Excel::XLSX);
            $rows = $obj->rows($records[0]);
        } else {
            $rows = (new CsvImportPalmer($filePath))->rows();
        }

        if (empty($rows)) {
            return response($emptyMessage, 400);
        }

        $result = '';
        $tr = '<tr>%s</tr>';
        $td = '<td>%s</td>';
        $input = '<input type="%s" name="%s" class="form-control" value="%s" autocomplete="off" required>';
        $selectGender = '<select class="form-select" name="%s" autocomplete="off">
        <option value="L" %s>Laki - Laki</option>
        <option value="P" %s>Perempuan</option>
        </select>';

        foreach ($rows as $row) {
            $inputTglLahir = view('partials.datepicker', [
                'dateName' => 'birth_date[]',
                'dateValue' => $row['birth_date'],
                'endDate' => Carbon::today(),
                'required' => true,
                'placeholder' => 'Tgl. Lahir',
            ])->render();

            $gender = $row['gender'];

            $htmlArray = [
                sprintf($td, sprintf($input, 'text', 'identity[]', $row['identity'])),
                sprintf($td, sprintf($input, 'text', 'name[]', $row['name'])),
                sprintf($td, sprintf($selectGender, 'gender[]', ($gender == 'L') ? 'selected' : '', ($gender == 'P') ? 'selected' : '')),
                sprintf($td, sprintf($input, 'text', 'birth_city[]', $row['birth_city'])),
                sprintf($td, $inputTglLahir),
                sprintf($td, sprintf($input, 'email', 'email[]', $row['email'])),
                sprintf($td, sprintf($input, 'text', 'phone[]', $row['phone'])),
                sprintf($td, sprintf($input, 'text', 'address[]', $row['address'])),
                sprintf($td, sprintf($input, 'text', 'pos_code[]', $row['pos_code'])),
            ];

            $result .= sprintf($tr, implode('', $htmlArray));
        }

        return response($result, 200);
    }

    private function getAgencies(): Collection
    {
        return User::query()
            ->byAgencyUser()
            ->byHasProfile(true)
            ->byStatus(USER_STATUS_ACTIVE)
            ->byActivated(true)
            ->orderBy('name')
            ->get();
    }

    public function saveImport(Request $request)
    {
        $baseValues = $request->only(['branch_id', 'package_id', 'agency_id', 'village_id']);
        $values = $request->except(['_token', 'branch_id', 'package_id', 'agency_id', 'village_id']);

        $branches = Branch::query()->byActive(true)->get();
        $branchIds = $branches->isNotEmpty() ? $branches->pluck('id')->toArray() : [-999999];
        $branch = $branches->where('id', '=', $baseValues['branch_id'])->first();
        $strBranchIds = implode(',', $branchIds);

        $agencies = $this->getAgencies();
        $agencyIds = $agencies->pluck('id')->toArray();
        $strAgencyIds = implode(',', array_merge([-1], $agencyIds));

        $agency = ($baseValues['agency_id'] > -1) ? $agencies->where('id', '=', $baseValues['agency_id'])->first() : $branch->admin;

        $packages = Package::query()->byActive()->get();
        $packageIds = $packages->isNotEmpty() ? $packages->pluck('id')->toArray() : [-999999];
        // $package = $packages->where('id', '=', $baseValues['package_id'])->first();
        $strPackageIds = implode(',', $packageIds);

        // $baseValues['branch_detail'] = $branch ? $branch->toArray() : null;
        // $baseValues['agency_detail'] = $agency ? $agency->toArray() : null;
        // $baseValues['package_detail'] = $package ? $package->toArray() : null;

        $baseRules = [
            'branch_id' => ['required', 'in:' . $strBranchIds],
            'agency_id' => ['required', 'in:' . $strAgencyIds],
            'package_id' => ['required', 'in:' . $strPackageIds],
            'village_id' => ['required', 'exists:villages,id'],
        ];

        $village = optional(RegionRepository::getVillageById($baseValues['village_id'], ['district', 'city', 'province']));

        $baseValues['village_name'] = $village->name;
        $baseValues['district_name'] = ($district = optional($village->district))->name;
        $baseValues['city_name'] = ($city = optional($district->city))->name;
        $baseValues['province_name'] = optional($city->province)->name;

        $countItems = count($values['identity']);
        $customerValues = [];
        $itemRules = [];

        $itemValues['identity'] = [];
        $itemValues['name'] = [];
        $itemValues['email'] = [];
        $itemValues['phone'] = [];
        $itemValues['gender'] = [];
        $itemValues['birth_city'] = [];
        $itemValues['rule_birth_date'] = [];
        $itemValues['address'] = [];

        for ($i = 0; $i < $countItems; $i++) {
            $timeBirth = translatedDateToTime($values['birth_date'][$i] ?? '', ' ');
            $birthDate = $timeBirth ? date('Y-m-d', $timeBirth) : null;
            $age = 0;

            if (!is_null($birthDate)) {
                $birthCarbon = Carbon::createFromFormat('Y-m-d', $birthDate);
                $todayCarbon = Carbon::today();
                $age = $todayCarbon->diffInYears($birthCarbon);
            }

            $rowValues = $baseValues;
            $rowValues['agency_id'] = $agency ? $agency->id : null;

            $customerValues[] = array_merge($rowValues, [
                'identity' => $values['identity'][$i],
                'name' => $values['name'][$i],
                'email' => $values['email'][$i],
                'gender' => $values['gender'][$i],
                'birth_city' => $values['birth_city'][$i],
                'birth_date' => $timeBirth ? date('Y-m-d', $timeBirth) : null,
                'age' => $age,
                'is_wni' => true,
                'address' => $values['address'][$i],
                'pos_code' => $values['pos_code'][$i],
                'phone' => $values['phone'][$i],
                'school_id' => SCHOOL_LEVEL_OTHER,
                'profession' => null,
                'companion' => null,
                'companion_id' => COMPANION_NONE,
                'blood_id' => BLOODS[0],
                'smoking' => false,
            ]);

            $itemValues['identity'][$i]['value'] = $values['identity'][$i];
            $itemValues['name'][$i]['value'] = $values['name'][$i];
            $itemValues['email'][$i]['value'] = $values['email'][$i];
            $itemValues['phone'][$i]['value'] = $values['phone'][$i];
            $itemValues['gender'][$i]['value'] = $values['gender'][$i];
            $itemValues['birth_city'][$i]['value'] = $values['birth_city'][$i];
            $itemValues['rule_birth_date'][$i]['value'] = $timeBirth ? date('d F Y', $timeBirth) : null;
            $itemValues['address'][$i]['value'] = $values['address'][$i];

            $itemRules["identity.{$i}.value"] = ['required', 'string', 'regex:/^[a-z0-9]/i', 'max:50'];
            $itemRules["name.{$i}.value"] = ['required', 'string', 'max:100'];
            $itemRules["email.{$i}.value"] = ['required', 'email', 'max:100'];
            $itemRules["phone.{$i}.value"] = ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'];
            $itemRules["gender.{$i}.value"] = ['required', 'in:' . implode(',', array_keys(GENDERS))];
            $itemRules["birth_city.{$i}.value"] = ['required', 'string', 'max:100'];
            $itemRules["rule_birth_date.{$i}.value"] = ['required', 'date_format:d F Y'];
            $itemRules["address.{$i}.value"] = ['required', 'string', 'max:250'];
        }

        $validator = Validator::make(array_merge($baseValues, $itemValues), array_merge($baseRules, $itemRules), [], [
            'branch_id' => 'Kantor Perwakilan',
            'agency_id' => 'Agen Jamaah',
            'package_id' => 'Paket Program',
            'identity' => 'No. Identitas',
            'name' => 'Nama',
            'gender' => 'Jns. Kelamin',
            'rule_birth_date' => 'Tgl. Lahir',
            'address' => 'Alamat',
            'village_id' => 'Daerah',
            'pos_code' => 'Kode Pos',
            'email' => 'Email',
            'phone' => 'No. Handphone',
            'father_name' => 'Nama Ayah',
            'birth_city' => 'Tmp. Lahir',
            'wna_country' => 'Negara Asal',
            'school_id' => 'Pendidikan',
            'profession' => 'Pekerjaan / Profesi',
            'companion' => 'Nama Pendamping',
            'companion_id' => 'Pendamping',
            'blood_id' => 'Golongan Darah',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator, 'danger', 'Proses Gagal.'), 400);
        }

        DB::beginTransaction();

        try {
            foreach ($customerValues as $customer) {
                $customer['code'] = Customer::makeCode();

                Customer::create($customer);
            }

            session([
                'message' => 'Jamaah baru berhasil di-import.',
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.excel.import.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function downloadImportFormat(Request $request)
    {
        $format = $request->get('format', 'csv');

        return Customer::downloadImportFormat($format);
    }
}
