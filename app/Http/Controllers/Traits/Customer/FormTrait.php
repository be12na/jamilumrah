<?php

namespace App\Http\Controllers\Traits\Customer;

use App\Models\Branch;
use App\Models\Customer;
use App\Models\Package;
use App\Models\User;
use App\Notifications\Register\MainNotification;
use App\Notifications\Register\PalmerNotification;
use App\Repositories\RegionRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait FormTrait
{
    private function makeValidator(array &$values, User $user, Customer $customer = null): ValidatorContract
    {
        $rules = [];

        $branches = Branch::query()->byActive(true)->get();
        $branchIds = $branches->isNotEmpty() ? $branches->pluck('id')->toArray() : [-999999];
        $branch = $branches->where('id', '=', $values['branch_id'])->first();
        $packages = Package::query()->byActive()->get();
        $packageIds = $packages->isNotEmpty() ? $packages->pluck('id')->toArray() : [-999999];
        $package = $packages->where('id', '=', $values['package_id'])->first();
        $village = optional(RegionRepository::getVillageById($values['village_id'], ['district', 'city', 'province']));
        $timeBirth = translatedDateToTime($values['birth_date'] ?? '', ' ');

        $values['branch'] = $branch;
        $values['package'] = $package;
        // $values['branch_detail'] = $branch ? $branch->toArray() : null;
        $agency = $user->agency_user ? $user : $branch->admin;
        $values['agency_id'] = $agency->id;
        // $values['agency_detail'] = $agency->toArray();
        // $values['package_detail'] = $package ? $package->toArray() : null;

        $values['birth_date'] = $timeBirth ? date('Y-m-d', $timeBirth) : null;
        $values['rule_birth_date'] = $timeBirth ? date('d F Y', $timeBirth) : null;
        $values['village_name'] = $village->name;
        $values['district_name'] = ($district = optional($village->district))->name;
        $values['city_name'] = ($city = optional($district->city))->name;
        $values['province_name'] = optional($city->province)->name;
        $values['is_wni'] = true; //isset($values['is_wni']);

        $rules['branch_id'] = ['required', 'in:' . implode(',', $branchIds)];
        $rules['package_id'] = ['required', 'in:' . implode(',', $packageIds)];
        $rules['identity'] = ['required', 'string', 'regex:/^[a-z0-9]/i', 'max:50', 'unique:customers,identity'];
        $rules['name'] = ['required', 'string', 'max:100'];
        $rules['gender'] = ['required', 'in:' . implode(',', array_keys(GENDERS))];
        $rules['birth_city'] = ['required', 'string', 'max:100'];
        $rules['rule_birth_date'] = ['required', 'date_format:d F Y'];
        $rules['email'] = ['required', 'email', 'max:100'];
        $rules['phone'] = ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'];
        $rules['address'] = ['required', 'string', 'max:250'];
        $rules['village_id'] = ['required', 'exists:villages,id'];
        // $rules['father_name'] = ['nullable', 'string', 'max:100'];
        // $rules['profession'] = ['nullable', 'string', 'max:100'];
        // $rules['school_id'] = ['required', 'in:' . implode(',', array_keys(SCHOOL_LEVELS))];
        // $rules['blood_id'] = ['required', 'in:' . implode(',', BLOODS)];
        // $rules['companion_id'] = ['required', 'in:' . implode(',', array_keys(COMPANION_RELATIONSHIP))];

        // if ($values['companion_id'] > COMPANION_NONE) {
        //     $rules['companion'] = ['required', 'string', 'max:100'];
        // } else {
        //     $values['companion'] = null;
        // }

        // if ($values['is_wni'] !== true) {
        //     $rules['wna_country'] = ['required', 'string', 'max:100'];
        // } else {
        //     $values['wna_country'] = null;
        // }

        // efek cangkeul
        $values['school_id'] = SCHOOL_LEVEL_OTHER;
        $values['blood_id'] = BLOODS[0];
        $values['companion_id'] = COMPANION_NONE;
        $values['companion'] = null;
        $values['wna_country'] = null;
        $values['smoking'] = false; //isset($values['smoking']);

        return Validator::make($values, $rules, [
            'phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'identity' => 'No. Identitas',
            'name' => 'Nama',
            'gender' => 'Jns. Kelamin',
            'rule_birth_date' => 'Tgl. Lahir',
            'address' => 'Alamat',
            'village_id' => 'Daerah',
            'pos_code' => 'Kode Pos',
            'email' => 'Email',
            'phone' => 'No. Handphone',
            'branch_id' => 'Kantor Perwakilan',
            'package_id' => 'Paket Program',
            'father_name' => 'Nama Ayah',
            'birth_city' => 'Tmp. Lahir',
            'wna_country' => 'Negara Asal',
            'school_id' => 'Pendidikan',
            'profession' => 'Pekerjaan / Profesi',
            'companion' => 'Nama Pendamping',
            'companion_id' => 'Pendamping',
            'blood_id' => 'Golongan Darah',
        ]);
    }

    public function create(Request $request)
    {
        $user = $request->user();

        return view('customers.form', [
            'user' => $user,
            'customer' => null,
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $values = $request->except(['_token']);
        $validator = $this->makeValidator($values, $user);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $values['code'] = Customer::makeCode();
        $values['created_by'] = $user->id;

        $birthCarbon = Carbon::createFromFormat('Y-m-d', $values['birth_date']);
        $todayCarbon = Carbon::today();
        $values['age'] = $todayCarbon->diffInYears($birthCarbon);
        $superadmin = User::query()->byUsername('superadmin')->first();

        DB::beginTransaction();
        try {
            $newCustomer = Customer::create($values);

            // notification for customer
            $newCustomer->notify(new PalmerNotification($newCustomer, 'database', 'mail'));
            $newCustomer->notify(new PalmerNotification($newCustomer, 'database', 'onesender'));
            // notification for customer:end

            // notification for family center
            $superadmin->notify(new MainNotification($newCustomer, 'database', 'mail'));
            $superadmin->notify(new MainNotification($newCustomer, 'database', 'onesender'));
            // notification for family center:end

            session([
                'message' => 'Jamaah berhasil ditambahkan.',
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function edit(Request $request)
    {
        $customer = $request->customerEditable;
    }

    public function update(Request $request)
    {
        $customer = $request->customerEditable;
    }
}
