<?php

namespace App\Http\Controllers\Traits\Customer;

use App\Models\BonusBranch;
use App\Models\BonusLevel;
use App\Models\Customer;
use App\Models\Setting;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait ActionTrait
{
    public function approveCustomer(Request $request)
    {
        $customer = $request->customerApprove;

        if (!$request->isMethod('POST')) {
            return view('customers.action', [
                'modalTitle' => 'Terima Jamaah',
                'customer' => $customer,
                'postUrl' => route('customer.approve.save', ['customerApprove' => $customer->id]),
                'actionId' => CUSTOMER_STATUS_APPROVED,
            ]);
        }

        $user = $request->user();
        $customerArray = null; //$customer->toArray();
        $setting = optional(Setting::query()->first());
        $bonusLevels = $setting->bonus_level ?? collect();
        $bonusBranch = $setting->bonus_branch ?? 0;
        $dateTime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $values = [
            'status' => CUSTOMER_STATUS_APPROVED,
            'approved_by' => $user->id,
            'approved_at' => $dateTime,
            'register_token' => Customer::makeRegisterToken(),
        ];

        $bonuses = [];
        if ($bonusBranch > 0) {
            $bonuses['branch'] = [
                'bonus_date' => $date,
                'branch_id' => $customer->branch_id,
                'customer_id' => $customer->id,
                'customer_detail' => $customerArray,
                'bonus' => $bonusBranch,
                'setting_id' => $setting->id,
            ];
        }

        $referral = $customer->agency;
        $userBonusLevels = [];
        // $levelDetails = [];
        // $updateAgency = [];

        // if ($referral->agency_user) {
        //     $countCustomer = $referral->approvedCustomers->count() + 1;
        //     if (!$referral->agency_user_director && ($countCustomer >= AGENCY_EXECUTIVE_REQUIRE_CUSTOMER)) {
        //         if ($countCustomer >= AGENCY_DIRECTOR_REQUIRE_CUSTOMER) {
        //             $updateAgency = [
        //                 'user_type' => USER_AGENCY_EXECUTIVE_DIRECTOR,
        //             ];
        //         } else {
        //             $updateAgency = [
        //                 'user_type' => USER_AGENCY_EXECUTIVE,
        //             ];
        //         }
        //     }
        // }

        $agencyIsCustomer = $referral->agency_is_from_customer;
        $bonusLevel1 = optional($bonusLevels->where('level', '=', 1)->first())['bonus'];
        if ($bonusLevel1 > 0) {
            // $levelDetails[] = $referral->toArray();
            $userBonusLevels[] = [
                'bonus_date' => $date,
                'user_id' => $referral->id,
                'customer_id' => $customer->id,
                'customer_detail' => $customerArray,
                'level' => 1,
                'level_detail' => null, //$levelDetails,
                'bonus' => $bonusLevel1,
                'setting_id' => $setting->id,
            ];
        }

        $referral = $referral->referral;

        if ($agencyIsCustomer && !empty($referral)) {
            // $levelDetails[] = $referral->toArray();
            $bonusLevel2 = optional($bonusLevels->where('level', '=', 2)->first())['bonus'];
            if ($bonusLevel2 > 0) {
                $userBonusLevels[] = [
                    'bonus_date' => $date,
                    'user_id' => $referral->id,
                    'customer_id' => $customer->id,
                    'customer_detail' => $customerArray,
                    'level' => 2,
                    'level_detail' => null, //$levelDetails,
                    'bonus' => $bonusLevel2,
                    'setting_id' => $setting->id,
                ];
            }
        }

        $bonuses['level'] = $userBonusLevels;

        DB::beginTransaction();
        try {
            $customer->update($values);

            // if (!empty($updateAgencies)) {
            //     foreach ($updateAgencies as $updateAgency) {
            //         $updateAgency['model']->update($updateAgency['values']);
            //     }
            // }

            if (!empty($bonuses)) {
                foreach ($bonuses as $keyModel => $bonusValues) {
                    if ($keyModel == 'branch') {
                        BonusBranch::create($bonusValues);
                    } elseif ($keyModel == 'level') {
                        foreach ($bonusValues as $bonusLevelValue) {
                            BonusLevel::create($bonusLevelValue);
                        }
                    }
                }
            }

            $customer->sendEmailToBeAgency();

            session([
                'message' => "Jamaah <b>{$customer->name}</b> telah berhasil diterima sebagai jamaah.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.detail', ['customer' => $customer->id]), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    private function validatorNotes(array $values): ValidatorContract
    {
        return Validator::make($values, [
            'notes' => ['required', 'string', 'max:250'],
        ], [], [
            'notes' => 'Keterangan / Alasan',
        ]);
    }

    public function rejectCustomer(Request $request)
    {
        $customer = $request->customerReject;

        if (!$request->isMethod('POST')) {
            return view('customers.action', [
                'modalTitle' => 'Penolakan Transfer',
                'customer' => $customer,
                'postUrl' => route('customer.reject.save', ['customerReject' => $customer->id]),
                'actionId' => CUSTOMER_STATUS_REJECTED,
            ]);
        }

        $values = $request->only('notes');
        $validator = $this->validatorNotes($values);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $user = $request->user();
        $values['status'] = CUSTOMER_STATUS_REJECTED;
        $values['rejected_by'] = $user->id;
        $values['rejected_at'] = date('Y-m-d H:i:s');
        $values['rejected_note'] = $values['notes'];

        DB::beginTransaction();
        try {
            $customer->update($values);

            session([
                'message' => "Jamaah <b>{$customer->name}</b> telah berhasil ditolak.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.detail', ['customer' => $customer->id]), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function cancelCustomer(Request $request)
    {
        $customer = $request->customerCancel;

        if (!$request->isMethod('POST')) {
            return view('customers.action', [
                'modalTitle' => 'Pembatalan Jamaah',
                'customer' => $customer,
                'postUrl' => route('customer.cancel.destroy', ['customerCancel' => $customer->id]),
                'actionId' => CUSTOMER_STATUS_CANCELED,
            ]);
        }

        DB::beginTransaction();
        try {
            $customer->delete();

            session([
                'message' => "Jamaah <b>{$customer->name}</b> telah berhasil dibatalkan.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function removeCustomer(Request $request)
    {
        $customer = $request->customerRemove;

        if (!$request->isMethod('POST')) {
            return view('customers.action', [
                'modalTitle' => 'Pembatalan Jamaah',
                'customer' => $customer,
                'postUrl' => route('customer.remove.destroy', ['customerRemove' => $customer->id]),
                'actionId' => CUSTOMER_STATUS_DELETED,
            ]);
        }

        DB::beginTransaction();
        try {
            $customer->delete();

            session([
                'message' => "Jamaah <b>{$customer->name}</b> telah berhasil dihapus.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('customer.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
