<?php

namespace App\Http\Controllers\Traits\Customer;

use App\Models\Customer;
use App\Models\User;
use App\Rules\RulePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

trait ToBeAgency
{
    public function toBeAgency(Request $request)
    {
        $customer = $request->unRegisteredCustomer;

        if (strtolower($request->method()) != 'post') {
            return view('customers.register-agency', [
                'customer' => $customer,
            ]);
        }

        $passwd = RulePassword::min(6)->numbers()->letters();
        $validator = Validator::make($values = $request->except(['_token']), [
            'username' => ['required', 'string', 'min:6', 'max:30', 'regex:/^[\w-]*$/', 'unique:users,username'],
            'email' => ['required', 'email', 'max:100'],
            'password' => ['required', 'string', 'confirmed', $passwd],
        ], [], [
            'username' => 'Username',
            'password' => 'Password',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $referral = $customer->agency;
        $values['password'] = Hash::make($values['password']);
        unset($values['password_confirmation']);

        $userValues = [
            'username' => $values['username'],
            'name' => $customer->name,
            'email' => $values['email'],
            'password' => $values['password'],
            'user_group' => USER_GROUP_AGENCY,
            'user_type' => USER_AGENCY_BASIC,
            'status' => USER_STATUS_ACTIVE,
            'referral_id' => $referral->id,
            'phone' => $customer->phone,
            'has_profile' => true,
            'identity' => $customer->identity,
            'gender' => $customer->gender,
            'birth_date' => $customer->birth_date,
            'address' => $customer->address,
            'village_id' => $customer->village_id,
            'village_name' => $customer->village_name,
            'district_name' => $customer->district_name,
            'city_name' => $customer->city_name,
            'province_name' => $customer->province_name,
            'pos_code' => $customer->pos_code,
            'activated_at' => date('Y-m-d H:i:s'),
            'activated_by' => 0,
            'customer_id' => $customer->id,
            'link_token' => User::makeLinkToken(),
        ];

        DB::beginTransaction();
        try {
            $customer->update([
                'is_agency' => true,
            ]);

            $newAgency = User::create($userValues);
            $newAgency->structure()->create(['parent_id' => $referral->structure->id]);

            session([
                'message' => "Anda berhasil menjadi agen kami. Silahkan login",
                'messageClass' => 'success',
            ]);

            DB::commit();

            return response(route('login'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function resendToBeAgency(Request $request)
    {
        $customer = $request->unRegisteredCustomerId;

        if (strtolower($request->method()) != 'post') {
            return view('customers.resend-agency', [
                'customer' => $customer
            ]);
        }

        $values = [
            'email' => $request->get('email'),
            'register_token' => Customer::makeRegisterToken(),
        ];

        $validator = Validator::make($values, [
            'email' => ['required', 'email', 'max:100'],
        ], [], [
            'email' => 'Email Tujuan',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        DB::beginTransaction();
        try {
            $customer->update($values);
            $customer->sendEmailToBeAgency();

            session([
                'message' => "Registrasi agen bagi jamaah {$customer->name} berhasil dikirim ulang.",
                'messageClass' => 'success',
            ]);

            DB::commit();

            return response(route('customer.detail', ['customer' => $customer->id]), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
