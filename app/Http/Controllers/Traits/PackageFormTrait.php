<?php

namespace App\Http\Controllers\Traits;

use App\Models\Branch;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Unique;

trait PackageFormTrait
{
    private function validationInput(array &$values, Package $package = null)
    {
        $uniqueCode = new Unique('packages', 'code');
        if (!empty($package)) {
            $uniqueCode = $uniqueCode->ignore($package->id, 'id');
        }

        $validator = Validator::make($values, [
            'code' => ['required', 'string', 'max:30', $uniqueCode],
            'name' => 'required|string|max:100',
            'price' => 'required|integer|gt:0',
            'description' => 'nullable|max:250',
        ], [], [
            'code' => 'Kode',
            'name' => 'Nama',
            'price' => 'Harga',
            'description' => 'Keterangan',
        ]);

        $result = [
            'valid' => !($isFail = $validator->fails()),
            'message' => '',
        ];

        if ($isFail) {
            $result['message'] = errorMessageContent($validator);
        }

        return $result;
    }

    public function create(Request $request)
    {
        return view('packages.main-form', [
            'user' => $request->user(),
            'postUrl' => route('package.store'),
            'data' => null,
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $values = $request->except(['_token']);

        $content = route('package.index');
        $statusCode = 200;

        $validate = $this->validationInput($values);
        if ($validate['valid'] === true) {
            $values['created_by'] = $user->id;

            DB::beginTransaction();
            try {
                Package::create($values);

                session([
                    'message' => "Data Paket berhasil ditambahkan.",
                    'messageClass' => 'success',
                ]);

                DB::commit();
            } catch (\Exception $e) {
                $statusCode = 500;
                $message = isLive() ? MESSAGE_500 : $e->getMessage();
                $content = errorMessageContent($message);
            }
        } else {
            $content = $validate['message'];
            $statusCode = 400;
        }

        return response($content, $statusCode);
    }

    public function edit(Request $request)
    {
        $package = $request->mainPackage;

        return view('packages.main-form', [
            'user' => $request->user(),
            'postUrl' => route('package.update', ['mainPackage' => $package->id]),
            'data' => $package,
        ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $package = $request->mainPackage;
        $values = $request->except(['_token']);

        $content = route('package.index');
        $statusCode = 200;

        $validate = $this->validationInput($values, $package);
        if ($validate['valid'] === true) {
            $values['updated_by'] = $user->id;

            DB::beginTransaction();
            try {
                $package->update($values);

                session([
                    'message' => "Data Paket berhasil diperbarui.",
                    'messageClass' => 'success',
                ]);

                DB::commit();
            } catch (\Exception $e) {
                $statusCode = 500;
                $message = isLive() ? MESSAGE_500 : $e->getMessage();
                $content = errorMessageContent($message);
            }
        } else {
            $content = $validate['message'];
            $statusCode = 400;
        }

        return response($content, $statusCode);
    }
}
