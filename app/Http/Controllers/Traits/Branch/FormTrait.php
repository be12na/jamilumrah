<?php

namespace App\Http\Controllers\Traits\Branch;

use App\Models\Branch;
use App\Models\User;
use App\Repositories\RegionRepository;
use App\Rules\RulePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Unique;

trait FormTrait
{
    private function branchAdminRules(User $admin = null): array
    {
        $isNewAdmin = empty($admin);
        $uniqueUsername = new Unique('users', 'username');
        $passwdRule = RulePassword::min(6)->numbers()->letters();

        if (!$isNewAdmin) {
            $uniqueUsername = $uniqueUsername->ignore($admin->id, 'id');
        }

        return [
            'admin_name' => ['required', 'string', 'max:100'],
            'username' => ['required', 'string', 'min:6', 'max:30', 'regex:/^[\w-]*$/', $uniqueUsername],
            'email' => ['required', 'string', 'max:100', 'email'],
            'admin_phone' => ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'],
            'password' => [$isNewAdmin ? 'required' : 'nullable', 'string', 'confirmed', $passwdRule],
        ];
    }

    private function validationInput(array &$values, Branch $branch = null)
    {
        $uniqueName = new Unique('branches', 'name');
        $admin = null;
        if (!empty($branch)) {
            $uniqueName = $uniqueName->ignore($branch->id, 'id');
            $admin = $branch->admin;
        }

        $validator = Validator::make($values, array_merge([
            'name' => ['required', 'string', 'max:100', $uniqueName],
            'leader_name' => ['required', 'string', 'max:100'],
            'leader_phone' => ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'],
            'address' => 'required|string|max:250',
            'village_id' => 'required|exists:villages,id',
            'pos_code' => 'nullable|digits:5',
            'phone' => ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)|(^\(0\d{2}\) \d)|(^0\d{2}\-\d)|(^\(0\d{3}\) \d)|(^0\d{3}\-\d)/'],
            'fax' => ['nullable', 'min:9', 'max:15', 'regex:/(^\(0\d{2}\)\-\d)|(^\(0\d{3}\) \d)|(^0\d{3}\-\d)/'],
        ], $this->branchAdminRules($admin)), [
            'phone.regex' => 'Format :attribute tidak dikenali.',
            'fax.regex' => 'Format :attribute tidak dikenali.',
            'admin_phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'name' => 'Nama Cabang',
            'leader_name' => 'Nama Pimpinan',
            'leader_phone' => 'Handphone Pimpinan',
            'address' => 'Alamat',
            'village_id' => 'Daerah',
            'pos_code' => 'Kode Pos',
            'phone' => 'Telepon / Kontak',
            'fax' => 'Fax',
            'admin_name' => 'Nama Adminstrator',
            'username' => 'Username',
            'email' => 'Email',
            'admin_phone' => 'Handphone Administrator',
            'password' => 'Password',
        ]);

        $result = [
            'valid' => !($isFail = $validator->fails()),
            'message' => '',
        ];

        if ($isFail) {
            $result['message'] = errorMessageContent($validator);
        } else {
            $village = RegionRepository::getVillageById($values['village_id'], ['district', 'city', 'province']);
            $values['village_name'] = $village->name;
            $values['district_name'] = $village->district->name;
            $values['city_name'] = $village->district->city->name;
            $values['province_name'] = $village->district->city->province->name;

            if (!empty($branch)) {
                $values['is_active'] = isset($values['is_active']);
            } else {
                $values['is_active'] = true;
            }
        }

        return $result;
    }

    private function setAdminValues(User $user, array &$values, User $admin = null): void
    {
        if (empty($admin)) {
            $values['user_group'] = USER_GROUP_BRANCH;
            $values['user_type'] = USER_BRANCH_ADMIN;
            $values['activated_at'] = date('Y-m-d H:i:s');
            $values['activated_by'] = $user->id;
            $values['has_profile'] = true;
            $values['password'] = Hash::make($values['password']);
            $values['link_token'] = User::makeLinkToken();
        } else {
            if (empty($values['password'])) {
                unset($values['password']);
            } else {
                $values['password'] = Hash::make($values['password']);
            }

            if (empty($admin->link_token)) $values['link_token'] = User::makeLinkToken();
        }
    }

    public function create(Request $request)
    {
        return view('branches.main-form', [
            'user' => $request->user(),
            'postUrl' => route('branch.store'),
            'data' => null
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $values = $request->except(['_token']);
        $content = route('branch.index');
        $statusCode = 200;
        $validate = $this->validationInput($values);

        if ($validate['valid'] === true) {
            $values['created_by'] = $user->id;

            $adminValues = $values;
            $adminValues['name'] = $values['admin_name'];
            $adminValues['phone'] = $values['admin_phone'];
            $this->setAdminValues($user, $adminValues);

            DB::beginTransaction();
            try {
                $branch = Branch::create($values);
                $admin = $branch->admin()->create($adminValues);
                $admin->structure()->create();

                session([
                    'message' => "Data Cabang berhasil ditambahkan.",
                    'messageClass' => 'success'
                ]);

                DB::commit();
            } catch (\Exception $e) {
                $statusCode = 500;
                $message = isLive() ? MESSAGE_500 : $e->getMessage();
                $content = errorMessageContent($message);
            }
        } else {
            $content = $validate['message'];
            $statusCode = 400;
        }

        return response($content, $statusCode);
    }

    public function edit(Request $request)
    {
        $branch = $request->mainBranch;

        return view('branches.main-form', [
            'user' => $request->user(),
            'postUrl' => route('branch.update', ['mainBranch' => $branch->id]),
            'data' => $branch
        ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $branch = $request->mainBranch;
        $values = $request->except(['_token']);
        $content = route('branch.index');
        $statusCode = 200;
        $validate = $this->validationInput($values, $branch);

        if ($validate['valid'] === true) {
            $values['updated_by'] = $user->id;

            $admin = $branch->admin;
            $hasStructure = empty($admin) ? false : !empty($admin->structure);
            $adminValues = $values;
            $adminValues['name'] = $values['admin_name'];
            $adminValues['phone'] = $values['admin_phone'];
            $this->setAdminValues($user, $adminValues, $admin);

            DB::beginTransaction();
            try {
                $branch->update($values);

                if (empty($admin)) {
                    $admin = $branch->admin()->create($adminValues);
                } else {
                    $admin->update($adminValues);
                }

                if (!$hasStructure) {
                    $admin->structure()->create();
                }

                session([
                    'message' => "Data Cabang berhasil diperbarui.",
                    'messageClass' => 'success'
                ]);

                DB::commit();
            } catch (\Exception $e) {
                $statusCode = 500;
                $message = isLive() ? MESSAGE_500 : $e->getMessage();
                $content = errorMessageContent($message);
            }
        } else {
            $content = $validate['message'];
            $statusCode = 400;
        }

        return response($content, $statusCode);
    }

    public function viewFormAdmin(Request $request)
    {
        $branch = $request->mainBranch;

        return view('branches.admin-form', [
            'branch' => $branch,
            'modalTitle' => 'Setting',
        ]);
    }

    public function saveAdmin(Request $request)
    {
        $user = $request->user();
        $branch = $request->mainBranch;
        $admin = $branch->admin;
        $values = $request->except(['_token']);
        $values['admin_name'] = $values['name'];
        $values['admin_phone'] = $values['phone'];

        $validator = Validator::make($values, $this->branchAdminRules($admin), [
            'admin_phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'admin_name' => 'Nama',
            'username' => 'Username',
            'email' => 'Email',
            'admin_phone' => 'No. Handphone',
            'password' => 'Password',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $isNew = empty($admin);
        $this->setAdminValues($user, $values, $admin);
        $values['status'] = USER_STATUS_ACTIVE;
        $hasStructure = $isNew ? false : !empty($admin->structure);

        DB::beginTransaction();
        try {
            $msg = 'dibuat';
            if ($isNew) {
                $admin = $branch->admin()->create($values);
            } else {
                $admin = $branch->admin->update($values);
                $msg = 'diubah';
            }

            if (!$hasStructure) {
                $admin->structure()->create();
            }

            session([
                'message' => "Admin untuk cabang {$branch->name} berhasil {$msg}.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('branch.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
