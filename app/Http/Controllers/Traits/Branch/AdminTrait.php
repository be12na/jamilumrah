<?php

namespace App\Http\Controllers\Traits\Branch;

use App\Models\Branch;
use App\Models\User;
use App\Rules\RulePassword;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Unique;

trait AdminTrait
{
    public function viewFormAdmin(Request $request)
    {
        $branch = $request->mainBranch;

        return view('branches.admin-form', [
            'branch' => $branch,
            'modalTitle' => 'Setting',
        ]);
    }

    private function validationAdmin(array $values, User $admin = null): ValidatorContract
    {
        $isNew = empty($admin);

        $uniqueUsername = new Unique('users', 'username');
        $passwdRule = RulePassword::min(6)->numbers()->letters();

        if (!$isNew) {
            $uniqueUsername = $uniqueUsername->ignore($admin->id, 'id');
        }

        return Validator::make($values, [
            'username' => ['required', 'string', 'min:6', 'max:30', 'regex:/^[\w-]*$/', $uniqueUsername],
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'max:100', 'email'],
            'phone' => ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'],
            'password' => [$isNew ? 'required' : 'nullable', 'string', 'confirmed', $passwdRule],
        ], [
            'phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'username' => 'Username',
            'name' => 'Nama',
            'email' => 'Email',
            'phone' => 'No. Handphone',
            'password' => 'Password',
        ]);
    }

    public function saveAdmin(Request $request)
    {
        $user = $request->user();
        $branch = $request->mainBranch;
        $admin = $branch->admin;
        $values = $request->except(['_token']);

        $validator = $this->validationAdmin($values, $admin);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $isNew = empty($admin);
        $values['status'] = USER_STATUS_ACTIVE;

        unset($values['password_confirmation']);

        $hasStructure = $isNew ? false : !empty($admin->structure);

        if ($isNew) {
            $values['user_group'] = USER_GROUP_BRANCH;
            $values['user_type'] = USER_BRANCH_ADMIN;
            $values['activated_at'] = date('Y-m-d H:i:s');
            $values['activated_by'] = $user->id;
            $values['has_profile'] = true;
            $values['password'] = Hash::make($values['password']);
            $values['link_token'] = User::makeLinkToken();
        } else {
            if (empty($values['password'])) {
                unset($values['password']);
            } else {
                $values['password'] = Hash::make($values['password']);
            }

            if (empty($admin->link_token)) $values['link_token'] = User::makeLinkToken();
        }

        DB::beginTransaction();
        try {
            $msg = 'dibuat';
            if ($isNew) {
                $admin = $branch->admin()->create($values);
            } else {
                $admin = $branch->admin->update($values);
                $msg = 'diubah';
            }

            if (!$hasStructure) {
                $admin->structure()->create();
            }

            session([
                'message' => "Admin untuk cabang {$branch->name} berhasil {$msg}.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('branch.index'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
