<?php

namespace App\Http\Controllers\Traits\Agency;

use App\Rules\RulePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

trait AuthTrait
{
    public function editAuth(Request $request)
    {
        $agent = $request->mainAgent;

        if (strtolower($request->method()) == 'get') {
            return view('agencies.main.auth-form', [
                'data' => $agent
            ]);
        }

        $values = $request->except(['_token']);

        $passwd = RulePassword::min(6)->numbers()->letters();

        $validator = Validator::make($values, [
            'username' => ['required', 'string', 'min:6', 'max:30', 'regex:/^[\w-]*$/', "unique:users,username,{$agent->id},id"],
            'password' => ['nullable', 'string', 'confirmed', $passwd],
        ], [], [
            'username' => 'Username',
            'password' => 'Password',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $content = route('agency.index');
        $statusCode = 200;

        if (!empty($values['password'])) {
            $values['password'] = Hash::make($values['password']);
        } else {
            unset($values['password']);
        }

        DB::beginTransaction();
        try {
            $agent->update($values);

            session([
                'message' => "Autentikasi Agen berhasil diperbarui.",
                'messageClass' => 'success',
            ]);

            DB::commit();
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = isLive() ? MESSAGE_500 : $e->getMessage();
            $content = errorMessageContent($message);
        }

        return response($content, $statusCode);
    }
}
