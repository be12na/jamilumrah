<?php

namespace App\Http\Controllers\Traits\Agency;

use App\Models\BonusLevel;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait ActionTrait
{
    public function approve(Request $request)
    {
        $agent = $request->mainAgent;

        if (strtolower($request->method()) == 'get') {
            return view('agencies.main.approve', [
                'data' => $agent
            ]);
        }

        $values = [
            'status' => USER_STATUS_ACTIVE,
            'activated_at' => date('Y-m-d H:i:s'),
            'activated_by' => $request->user()->id,
        ];

        $content = route('agency.activation');
        $statusCode = 200;

        $referral = $agent->referral;

        DB::beginTransaction();
        try {
            $agent->update($values);
            $agent->structure()->create(['parent_id' => $referral->structure->id]);

            session([
                'message' => "Agen baru berhasil diaktifkan.",
                'messageClass' => 'success',
            ]);

            DB::commit();
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = isLive() ? MESSAGE_500 : $e->getMessage();
            $content = errorMessageContent($message);
        }

        return response($content, $statusCode);
    }

    public function reject(Request $request)
    {
        $agent = $request->mainAgent;

        if (strtolower($request->method()) == 'get') {
            return view('agencies.main.reject', [
                'data' => $agent
            ]);
        }

        $values = $request->only(['rejected_note']);

        $validator = Validator::make($values, [
            'rejected_note' => 'required|string|max:250',
        ], [], [
            'rejected_note' => 'Keterangan',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $values = array_merge([
            'status' => USER_STATUS_REJECTED,
            'rejected_at' => date('Y-m-d H:i:s'),
            'rejected_by' => $request->user()->id,
        ], $values);

        $content = route('agency.activation');
        $statusCode = 200;

        DB::beginTransaction();
        try {
            $agent->update($values);

            session([
                'message' => "Agen baru berhasil ditolak.",
                'messageClass' => 'success',
            ]);

            DB::commit();
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = isLive() ? MESSAGE_500 : $e->getMessage();
            $content = errorMessageContent($message);
        }

        return response($content, $statusCode);
    }

    public function deactivate(Request $request)
    {
        $user = $request->user();
        $agency = $request->mainAgent;
        $structure = $agency->structure;
        $exceptUserIds = $structure->descendantsWithSelf()->pluck('user_id')->toArray();
        $agencies = User::query()->byAgencyUser()->byActivated(true)->whereNotIn('id', $exceptUserIds);

        if (!$request->isMethod('POST')) {
            $routeName = $request->route()->getName();

            if ($routeName == 'agency.deactivate.index') {
                return view('agencies.main.deactivate-form', [
                    'agency' => $agency
                ]);
            } elseif ($routeName == 'agency.deactivate.confirm') {
                $referral = $agencies->byId($request->get('new_referral', 0))->first();

                return view('agencies.main.deactivate-confirm', [
                    'agency' => $agency,
                    'referral' => $referral,
                ]);
            }

            $parentId = $structure->parent->user_id;
            $search = $request->get('search');

            if (!is_null($search)) {
                $agencies = $agencies->where(function ($where) use ($search) {
                    return $where->where('username', 'like', "%{$search}%")
                        ->orWhere('name', 'like', "%{$search}%");
                });
            }

            $currentId = $request->get('current') ?? 0;

            $agencies = $agencies->orderByRaw("(CASE WHEN id={$currentId} THEN 0 WHEN id={$parentId} THEN 1 ELSE 10 END) asc")
                ->orderBy('name')
                ->limit(20)
                ->get();

            return response()->json($agencies->map(function ($row) {
                $name = $row->name;
                $username = $row->username;

                return ['id' => $row->id, 'text' => "{$name} ({$username})"];
            })->toArray());
        }

        $values = $request->except(['_token']);
        $newReferralId = $values['new_referral'];
        $newReferral = $agencies->byId($newReferralId)->first();
        $newStructure = $newReferral->structure;
        $children = $structure->children;
        $childrenIds = [];

        $agencyId = $agency->id;
        $agencyName = $agency->name;
        $agencyUsername = $agency->username;

        // pindahkan semua member si agen yg dinonaktifkan ke agen yg dipilih (jika ada)
        if ($children->isNotEmpty()) {
            $childrenIds = $children->pluck('user_id')->toArray();
            $position = $newStructure->children->count();

            foreach ($children->sortBy('position')->values() as $child) {
                $child->moveTo($position, $newStructure);
                $position++;
            }
        }

        // hapus struktur si member yg dinonaktifkan
        $structure->deleteSubTree(true, true);

        // update status si member yg dinonaktifkan
        $agency->update([
            'status' => USER_STATUS_BLOCKED,
            'blocked_at' => date('Y-m-d H:i:s'),
            'blocked_by' => $user->id,
            'blocked_note' => 'Dinonaktifkan',
        ]);

        // update member L1 info referral
        User::query()->whereIn('id', $childrenIds)->update([
            'referral_id' => $newReferralId,
        ]);

        // pindahkan bonus yg belum ditransfer
        BonusLevel::query()->byUser($agency->id)
            ->where('status', '=', BONUS_STATUS_PENDING)
            ->update([
                'user_id' => $newReferralId,
                'bonus_info' => "Pindahan dari {$agencyName} [{$agencyId}]",
            ]);

        // pindahkan semua jamaah dari si agen yg dinonaktifkan ke agen baru (yg dipilih)
        // Customer::query()->byOwner($agency)->update([
        //     'agency_id' => $newReferralId,
        // ]);

        session([
            'message' => "Agen dengan nama <b>{$agencyName}</b> ({$agencyUsername}) berhasil dinonaktifkan.",
            'messageClass' => 'success',
        ]);

        return response(route('agency.index'), 200);
    }
}
