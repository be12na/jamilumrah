<?php

namespace App\Http\Controllers\Traits\Dashboard;

use App\Models\BonusBranch;
use App\Models\Customer;
use Illuminate\Http\Request;

trait BranchTrait
{
    protected function dataBranchDashboard(Request $request)
    {
        $user = $request->user();
        $data = $request->get('data');
        $scope = $request->get('scope');

        $result = 0;

        if ($data == 'customer') $result = $this->countBranchCustomer($request, $scope);
        if ($data == 'bonus') $result = $this->sumBranchBonus($user->branch_id, $scope);

        return formatNumber($result, 0);
    }

    private function countBranchCustomer(Request $request, $scope): int
    {
        if (!in_array($scope, ['new', 'active', 'total', 'transfer'])) return 0;

        $query = Customer::query()->byOwner($request)->where('status', '!=', CUSTOMER_STATUS_CANCELED);
        if ($scope == 'new') {
            $query = $query->byStatus(Customer::newStatus());
        } elseif ($scope == 'active') {
            $query = $query->byApproved();
        } elseif ($scope == 'transfer') {
            $query = $query->byApproving();
        }

        return $query->count();
    }

    private function sumBranchBonus($branchId, $scope): int
    {
        if (!in_array($scope, ['pending', 'transferred', 'total'])) return 0;

        $query = BonusBranch::query()->byBranch($branchId);

        if ($scope == 'pending') {
            $query = $query->byPending();
        } elseif ($scope == 'transferred') {
            $query = $query->byTransferred();
        }

        return $query->sum('bonus');
    }
}
