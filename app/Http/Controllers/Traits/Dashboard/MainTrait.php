<?php

namespace App\Http\Controllers\Traits\Dashboard;

use App\Models\Branch;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;

trait MainTrait
{
    protected function dataMainDashboard(Request $request)
    {
        $data = $request->get('data');
        $scope = $request->get('scope');

        $result = 0;

        if ($data == 'branch') $result = $this->getCountMainBranches($scope);
        if ($data == 'agency') $result = $this->getCountMainAgencies($scope);
        if ($data == 'customer') $result = $this->getCountMainCustomers($scope);

        return formatNumber($result, 0);
    }

    private function getCountMainBranches($scope): int
    {
        if (!in_array($scope, ['active', 'inactive', 'total'])) return 0;

        $query = Branch::query();
        if ($scope != 'total') $query = $query->byActive(($scope == 'active'));

        return $query->count();
    }

    private function getCountMainAgencies($scope): int
    {
        if (!in_array($scope, ['activation', 'active', 'total'])) return 0;

        $query = User::query()->byAgencyUser();
        if ($scope == 'active') {
            $query = $query->byStatus(USER_STATUS_ACTIVE);
        } elseif ($scope == 'activation') {
            $query = $query->byStatus([USER_STATUS_INACTIVE]);
        } else {
            $query = $query->byStatus([USER_STATUS_INACTIVE, USER_STATUS_ACTIVE]);
        }

        return $query->count();
    }

    private function getCountMainCustomers($scope): int
    {
        if (!in_array($scope, ['new', 'active', 'total', 'transfer'])) return 0;

        $query = Customer::query()->where('status', '!=', CUSTOMER_STATUS_CANCELED);
        if ($scope == 'new') {
            $query = $query->byStatus(Customer::newStatus());
        } elseif ($scope == 'active') {
            $query = $query->byApproved();
        } elseif ($scope == 'transfer') {
            $query = $query->byApproving();
        }

        return $query->count();
    }

    // private function dataMainTransfers($scope): int
    // {
    //     if (!in_array($scope, ['confirm'])) return 0;

    //     $query = Customer::query()->byApproving();

    //     return $query->count();
    // }
}
