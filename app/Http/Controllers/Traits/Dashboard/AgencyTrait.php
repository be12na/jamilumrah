<?php

namespace App\Http\Controllers\Traits\Dashboard;

use App\Models\BonusLevel;
use App\Models\Customer;
use Illuminate\Http\Request;

trait AgencyTrait
{
    protected function dataAgencyDashboard(Request $request)
    {
        $user = $request->user();
        $data = $request->get('data');
        $scope = $request->get('scope');

        $result = 0;

        if ($data == 'customer') $result = $this->countAgencyCustomer($request, $scope);
        if ($data == 'bonus') $result = $this->sumAgencyBonus($user->id, $scope);

        return formatNumber($result, 0);
    }

    private function countAgencyCustomer(Request $request, $scope): int
    {
        if (!in_array($scope, ['new', 'active', 'total'])) return 0;

        $query = Customer::query()->byOwner($request)->where('status', '!=', CUSTOMER_STATUS_CANCELED);
        if ($scope == 'new') {
            $query = $query->byStatus(Customer::newStatus());
        } elseif ($scope == 'active') {
            $query = $query->byApproved();
        }

        return $query->count();
    }

    private function sumAgencyBonus($userId, $scope): int
    {
        if (!in_array($scope, ['pending', 'transferred', 'total'])) return 0;

        $query = BonusLevel::query()->byUser($userId);

        if ($scope == 'pending') {
            $query = $query->byPending();
        } elseif ($scope == 'transferred') {
            $query = $query->byTransferred();
        }

        return $query->sum('bonus');
    }
}
