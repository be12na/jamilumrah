<?php

namespace App\Http\Controllers;

use App\Libs\Tree;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class NetworkController extends Controller
{
    public function index(Request $request)
    {
        $statusId = session('filter.memberStatus', 0);
        $owner = $this->fixStructureOwner($request->user());

        return view('networks.index', [
            'user' => $owner,
            'currentStatus' => $statusId,
        ]);
    }

    private function fixStructureOwner(User $user): User
    {
        $owner = $user;

        if ($owner->branch_user) {
            $branch = $owner->branch;
            $owner = $branch->admin;
        }

        return $owner;
    }

    public function dataTable(Request $request)
    {
        $user = $this->fixStructureOwner($request->user());
        $userGroup = USER_GROUP_AGENCY;
        $statusId = intval($request->get('status_id', -1));

        $query = DB::table('users')
            ->leftJoin(DB::raw('users as members'), function ($join) {
                return $join->on('members.referral_id', '=', 'users.id')
                    ->where('members.status', '=', USER_STATUS_ACTIVE);
            })
            ->selectRaw("users.id, users.name, users.email, users.phone, users.user_type, users.status, COUNT(members.id) as count_member")
            ->where('users.user_group', '=', $userGroup)
            ->where('users.referral_id', '=', $user->id)
            ->groupByRaw("users.id, users.name, users.email, users.phone, users.user_type, users.status");

        if (in_array($statusId, [USER_STATUS_ACTIVE, USER_STATUS_INACTIVE])) {
            $query = $query->where('users.status', '=', $statusId);
        }

        session(['filter.memberStatus' => $statusId]);

        return datatables()->query($query)
            ->editColumn('name', function ($row) {
                $type = Arr::get(USER_AGENCY_NAMES, $row->user_type, 'Basic');
                $cls = 'bg-info text-dark';
                if ($row->user_type == USER_AGENCY_EXECUTIVE) {
                    $cls = 'bg-success';
                } elseif ($row->user_type == USER_AGENCY_EXECUTIVE_DIRECTOR) {
                    $cls = 'bg-primary';
                }
                $content = "<span class=\"fw-bold me-2\">{$row->name}</span><span class=\"badge rounded-pill {$cls}\">{$type}</span>";

                return new HtmlString($content);
            })
            ->editColumn('status', function ($row) {
                $cls = 'bg-success';
                if ($row->status != USER_STATUS_ACTIVE) {
                    $cls = 'bg-danger';
                }

                $status = Arr::get(USER_STATUS_NAMES, $row->status, 'Tidak Aktif');
                $content = "<span class=\"badge rounded-pill {$cls}\">{$status}</span>";

                return new HtmlString($content);
            })
            ->addColumn('view', function ($row) {
                $buttons = [];
                $treeUrl = route('network.tree', ['userTreeId' => $row->id]);
                if ($row->status == USER_STATUS_ACTIVE) {
                    $buttons[] = "<button type=\"button\" class=\"btn btn-sm btn-info\" data-href=\"{$treeUrl}\" title=\"Struktur Pohon\"><i class=\"fa-solid fa-network-wired\"></i></button>";
                }

                return (count($buttons) > 0) ? new HtmlString(implode('', $buttons)) : '';
            })
            ->escapeColumns()
            ->toJson();
    }

    public function table(Request $request)
    {
        // 
    }

    public function tree(Request $request)
    {
        $user = $this->fixStructureOwner($request->user());

        if ($request->userTreeId == $user->id) return redirect()->route('network.tree');

        $userTreeId = $request->userTreeId ?? $user->id;
        $topUser = User::byId($userTreeId)->first();
        $treeStructure = $this->setTreeNode($user, $topUser);

        return view('networks.tree', [
            'treeStructure' => $treeStructure,
        ]);
    }

    private function setTreeNode(User $owner, User $user, $level = 0, $maxLevel = 2): string
    {
        if ($level > $maxLevel) return '';

        $isOwner = ($owner->id == $user->id);
        $asRoot = ($level == 0);
        $htmlNode = $asRoot ? '<ul>%s</ul>' : '%s';

        $elements = [
            [
                'htmlText' => $user->name,
                'cssClass' => 'text-light fw-bold mx-1',
                'cssStyle' => ['background-color' => '#008000']
            ],
        ];

        $linkFormat = '<a href="%s" class="btn btn-sm %s" title="%s">%s</a>';
        $children = $user->structure->getChildren();
        $downlines = $children->pluck('user')->values();
        $members = $user->structure->getDescendants()->pluck('user')->values();
        $countActive = $members->where('status', '=', USER_STATUS_ACTIVE)->count();
        $countInActive = $members->where('status', '!=', USER_STATUS_ACTIVE)->count();

        $elements[] = [
            'htmlText' => '<span class="me-2">Status :</span><span class="text-' . ($user->status == USER_STATUS_ACTIVE ? 'primary' : 'danger') . '">' . $user->status_name . '</span>',
            'cssClass' => 'mb-1'
        ];

        $elements[] = [
            'htmlText' => "<div class=\"d-flex justify-content-between bg-primary text-white px-1\"><span>Aktif</span><span>{$countActive}</span></div>",
            // 'cssClass' => 'mb-1'
        ];
        $elements[] = [
            'htmlText' => "<div class=\"d-flex justify-content-between bg-danger text-white px-1\"><span>Tdk Aktif</span><span>{$countInActive}</span></div>",
            // 'cssClass' => 'px-1'
        ];

        $rootLink = '';
        $uplineLink = '';
        if (!$isOwner && ($level == 0)) {
            $rootLink = sprintf($linkFormat, route('network.tree'), 'btn-outline-primary mx-1', $owner->name, '<i class="fa-solid fa-home"></i>');

            if ($user->referral) {
                $uplineLink = sprintf($linkFormat, route('network.tree', ['userTreeId' => $user->referral->id]), 'btn-outline-success mx-1', 'Upline', '<i class="fa-solid fa-arrow-up"></i>');
            }
        }

        $downlineLink = '';
        if ($downlines->isNotEmpty() && ($level > 0)) {
            $downlineLink = sprintf($linkFormat, route('network.tree', ['userTreeId' => $user->id]), 'btn-outline-info mx-1', 'Downlines', '<i class="fa-solid fa-network-wired"></i>');
        }

        $controlLink = $rootLink . $uplineLink . $downlineLink;

        if (!empty($controlLink)) {
            $elements[] = [
                'htmlText' => $controlLink,
                'cssClass' => 'd-flex justify-content-around border-top text-center py-1'
            ];
        }

        $elementsNode = [
            'elements' => $elements,
        ];

        $htmlchildren = '';
        $level += 1;
        if ($downlines->isNotEmpty() && ($level <= $maxLevel)) {
            foreach ($downlines as $downline) {
                $htmlchildren .= $this->setTreeNode($owner, $downline, $level, $maxLevel);
            }
            $htmlchildren = sprintf('<ul>%s</ul>', $htmlchildren);
        }

        $html = Tree::setNode($elementsNode);
        $html = "<li>{$html}%s</li>";

        return sprintf($htmlNode, sprintf($html, $htmlchildren));
    }
}
