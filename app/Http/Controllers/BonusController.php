<?php

namespace App\Http\Controllers;

use App\Models\BonusBranch;
use App\Models\BonusLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class BonusController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        if ($user->agency_user) {
            return view('bonus.agency', [
                'user' => $user,
            ]);
        }

        $routeName = $request->route()->getName();

        if ($routeName == 'bonus.branch') {
            return view('bonus.branch', [
                'user' => $user,
            ]);
        }

        return view('bonus.agency', [
            'user' => $user,
        ]);
    }

    public function dataTable(Request $request)
    {
        $user = $request->user();
        $bonusType = $request->get('bonus_type');
        $bonusStatus = $request->get('bonus_status', -1);

        if (($user->branch_user || $user->main_user) && ($bonusType == 'branch')) {
            $query = DB::table('bonus_branches')
                ->join('branches', 'branches.id', '=', 'bonus_branches.branch_id')
                ->join('customers', 'customers.id', '=', 'bonus_branches.customer_id')
                ->selectRaw("bonus_branches.id, bonus_branches.bonus_date, bonus_branches.branch_id, bonus_branches.customer_id, bonus_branches.bonus, bonus_branches.status, customers.name as customer_name, branches.name as branch_name");

            if ($user->branch_user) {
                $query = $query->where('bonus_branches.branch_id', '=', $user->branch_id);
            }

            if (in_array($bonusStatus, array_keys(BONUS_STATUS_LIST))) {
                $query = $query->where('bonus_branches.status', '=', $bonusStatus);
            }

            $sql = $query->toSql();
            $sqlQuery = DB::table(DB::raw("({$sql}) as bonusan"))->mergeBindings($query);

            return datatables()->query($sqlQuery)
                ->editColumn('bonus', function ($row) {
                    return formatNumber($row->bonus, 0);
                })
                ->addColumn('check', function ($row) use ($user) {
                    $check = '';

                    if ($user->main_user_super) {
                        if ($row->status == BONUS_STATUS_PENDING) {
                            $check = '<input type="checkbox" class="check-row" value="' . $row->id . '" style="margin-right:2px;">';
                            $check = new HtmlString($check);
                        }
                    }

                    return $check;
                })
                ->addColumn('status', function ($row) {
                    $text = Arr::get(BONUS_STATUS_LIST, $row->status);
                    $bgBadge = ($row->status == BONUS_STATUS_TRANSFERRED) ? 'bg-primary' : 'bg-secondary';
                    $content = "<span class=\"badge badge-pill text-light {$bgBadge}\" style=\"--bs-badge-font-size:inherit\">{$text}</span>";

                    return new HtmlString($content);
                })
                ->escapeColumns()
                ->toJson();
        }

        $ownerBonus = $user;
        $userGroups = [USER_GROUP_AGENCY];
        if ($user->branch_user) {
            $ownerBonus = $user->branch->admin;
            $userGroups = [USER_GROUP_BRANCH];
        } elseif ($user->main_user) {
            $userGroups = [USER_GROUP_AGENCY, USER_GROUP_BRANCH];
        }

        $query = DB::table('bonus_levels')
            ->join('users', 'users.id', '=', 'bonus_levels.user_id')
            ->join('customers', 'customers.id', '=', 'bonus_levels.customer_id')
            ->selectRaw("bonus_levels.id, bonus_levels.bonus_date, bonus_levels.user_id, bonus_levels.customer_id, bonus_levels.level, bonus_levels.bonus, bonus_levels.status, customers.name as customer_name, bonus_levels.bonus_info, users.name as agency_name, users.user_type, users.user_group")
            ->whereIn('users.user_group', $userGroups);

        if (!$user->main_user) {
            $query = $query->where('bonus_levels.user_id', '=', $ownerBonus->id);
        }

        if (in_array($bonusStatus, array_keys(BONUS_STATUS_LIST))) {
            $query = $query->where('bonus_levels.status', '=', $bonusStatus);
        }

        $sql = $query->toSql();
        $sqlQuery = DB::table(DB::raw("({$sql}) as bonusan"))->mergeBindings($query);

        return datatables()->query($sqlQuery)
            ->editColumn('bonus_date', function ($row) {
                $content = '<div>' . translateDatetime(strtotime($row->bonus_date), __('format.date.medium')) . '</div>';

                if (!empty($row->bonus_info)) {
                    $info = $row->bonus_info;
                    $info = trim(explode('[', $info)[0]);

                    $content .= '<div class="text-wrap small text-success">' . $info . '</div>';
                }

                return new HtmlString($content);
            })
            ->editColumn('bonus', function ($row) {
                return formatNumber($row->bonus, 0);
            })
            ->editColumn('customer_name', function ($row) {
                return trim($row->customer_name, '"');
            })
            ->editColumn('agency_name', function ($row) use ($user) {
                $content = "<div>{$row->agency_name}</div>";
                if ($row->user_group == USER_GROUP_AGENCY) {
                    $type = 'Basic';
                    if ($row->user_type == USER_AGENCY_EXECUTIVE) {
                        $type = 'Executive';
                    } elseif ($row->user_type == USER_AGENCY_EXECUTIVE_DIRECTOR) {
                        $type = 'Executive Director';
                    }

                    $content .= "<div class=\"text-primary fst-italic text-decoration-underline\">{$type}</div>";
                }

                return new HtmlString($content);
            })
            ->addColumn('check', function ($row) use ($user) {
                $check = '';

                if ($user->main_user_super) {
                    if ($row->status == BONUS_STATUS_PENDING) {
                        $check = '<input type="checkbox" class="check-row" value="' . $row->id . '" style="margin-right:2px;">';
                        $check = new HtmlString($check);
                    }
                }

                return $check;
            })
            ->addColumn('status', function ($row) {
                $text = Arr::get(BONUS_STATUS_LIST, $row->status);
                $bgBadge = ($row->status == BONUS_STATUS_TRANSFERRED) ? 'bg-primary' : 'bg-secondary';
                $content = "<span class=\"badge badge-pill text-light {$bgBadge}\" style=\"--bs-badge-font-size:inherit\">{$text}</span>";

                return new HtmlString($content);
            })
            ->escapeColumns()
            ->toJson();
    }

    public function transfer(Request $request)
    {
        $bonusType = $request->get('bonus_type');

        if (!in_array($bonusType, ['branch', 'agency'])) {
            return ajaxError('Kategori ujroh tidak tersedia.', 400);
        }

        if (strtolower($request->method()) != 'post') {
            $ids = $request->get('ids');
            if (empty($ids)) {
                return ajaxError('Tidak ada ujroh yang dipilih');
            }

            $checks = explode(',', $ids);

            return view('bonus.confirm-transfer', [
                'bonusType' => $bonusType,
                'checks' => $checks,
            ]);
        }

        $bonusIds = $request->get('checks', []);
        if (count($bonusIds) == 0) {
            return ajaxError('Data yang dikirimkan tidak ditemukan.', 404);
        }

        $values = [
            'status' => BONUS_STATUS_TRANSFERRED,
            'status_at' => date('Y-m-d H:i:s'),
            'status_by' => $request->user()->id,
        ];

        $table = ($bonusType == 'branch') ? 'bonus_branches' : 'bonus_levels';
        $bonusName = ($bonusType == 'branch') ? 'Admin Cabang' : 'Agen';

        DB::beginTransaction();
        try {
            DB::table($table)->whereIn('id', $bonusIds)->where('status', '=', BONUS_STATUS_PENDING)->update($values);

            session([
                'message' => "Transfer Ujroh {$bonusName} berhasil dikonfirmasi.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('bonus.' . $bonusType), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
