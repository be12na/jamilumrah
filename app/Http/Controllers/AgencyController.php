<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Agency\ActionTrait;
use App\Http\Controllers\Traits\Agency\AuthTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class AgencyController extends Controller
{
    use ActionTrait, AuthTrait;

    public function index(Request $request)
    {
        $user = $request->user();
        $register = ($request->route()->getName() == 'agency.activation');

        return view('agencies.index', [
            'user' => $user,
            'register' => $register
        ]);
    }

    public function detail(Request $request)
    {
        $agent = $request->mainAgent;

        return view('agencies.detail', [
            'data' => $agent
        ]);
    }

    public function dataTable(Request $request)
    {
        $register = ($request->get('register', 0) == 1);

        $baseQuery = DB::table('users')
            ->leftJoin(DB::raw('users as referral'), 'referral.id', '=', 'users.referral_id')
            ->selectRaw("users.id, users.username, users.name, users.email, users.phone, users.status, users.user_type, users.referral_id, referral.name as referral_name, referral.username as referral_username, referral.user_type as referral_user_type")
            ->where('users.user_group', '=', USER_GROUP_AGENCY);

        if ($register) {
            $baseQuery = $baseQuery->whereNull('users.activated_at')->where('users.status', '=', USER_STATUS_INACTIVE);
        } else {
            $baseQuery = $baseQuery->where('users.status', '=', USER_STATUS_ACTIVE);
        }

        $query = DB::table(DB::raw('(' . $baseQuery->toSql() . ') as agency'))->mergeBindings($baseQuery);

        return datatables()->query($query)
            ->editColumn('name', function ($row) use ($register) {
                $content = "<div>{$row->name}</div>";
                if (!$register) {
                    $type = 'Basic';
                    if ($row->user_type == USER_AGENCY_EXECUTIVE) {
                        $type = 'Executive';
                    } elseif ($row->user_type == USER_AGENCY_EXECUTIVE_DIRECTOR) {
                        $type = 'Executive Director';
                    }

                    $content .= "<div class=\"text-primary fst-italic text-decoration-underline\">{$type}</div>";
                }

                return new HtmlString($content);
            })
            ->editColumn('referral_name', function ($row) {
                if (empty($row->referral_id)) return '-';

                $content = "<div>{$row->referral_name}</div>";
                $type = 'Basic';
                if ($row->referral_user_type == USER_AGENCY_EXECUTIVE) {
                    $type = 'Executive';
                } elseif ($row->referral_user_type == USER_AGENCY_EXECUTIVE_DIRECTOR) {
                    $type = 'Executive Director';
                }

                $content .= "<div class=\"text-primary fst-italic text-decoration-underline\">{$type}</div>";

                return new HtmlString($content);
            })
            ->editColumn('phone', function ($row) {
                return $row->phone ?? '-';
            })
            ->addColumn('action', function ($row) use ($register) {
                $detailUrl = route('agency.detail', ['mainAgent' => $row->id, 'agentRegister' => $register ? 1 : 0]);
                return new HtmlString("<button type=\"button\" class=\"btn btn-sm btn-info btn-data-detail\" title=\"Info Detail\" data-modal-url=\"{$detailUrl}\" data-bs-toggle=\"modal\" data-bs-target=\"#agencyModal\"><i class=\"fa-solid fa-circle-info\"></i></button>");
            })
            ->escapeColumns()
            ->toJson();
    }
}
