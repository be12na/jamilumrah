<?php

namespace App\Http\Controllers;

use App\Rules\RulePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    public function index(Request $request)
    {
        return view('password', [
            'user' => $request->user()
        ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $values = $request->only(['old_password', 'new_password', 'new_password_confirmation']);

        if (!Hash::check($values['old_password'], $user->password)) {
            return response(errorMessageContent('Password lama salah.'), 400);
        }

        $passwd = RulePassword::min(6)->numbers()->letters();

        $validator = Validator::make($values, [
            'new_password' => ['required', 'string', 'confirmed', 'different:old_password', $passwd],
        ], [], [
            'new_password' => 'Password baru',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        DB::beginTransaction();
        try {
            $user->update(['password' => Hash::make($values['new_password'])]);

            session([
                'message' => 'Password berhasil diganti.',
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('dashboard'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
