<?php

namespace App\Http\Controllers;

use App\Repositories\RegionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        return view('profile', [
            'user' => $request->user()
        ]);
    }

    public function edit(Request $request)
    {
        return view('profile', [
            'user' => $request->user(),
            'isEdit' => true,
        ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $values = $request->except(['_token']);
        $birthDate = translatedDateToTime($values['birth_date'], ' ');
        $values['birth_date'] = $birthDate ? date('Y-m-d', $birthDate) : null;
        $values['rule_birth_date'] = $birthDate ? date('d F Y', $birthDate) : null;

        $validator = Validator::make($values, [
            'identity' => ['required', 'string', 'max:50', "unique:users,identity,{$user->id},id"],
            'name' => ['required', 'string', 'max:100'],
            'gender' => ['required', 'string', 'in:' . implode(',', array_keys(GENDERS))],
            'rule_birth_date' => ['required', 'date_format:d F Y'],
            'address' => 'required|string|max:250',
            'village_id' => 'required|exists:villages,id',
            'pos_code' => 'nullable|digits:5',
            'email' => ['required', 'string', 'max:100', 'email'],
            'phone' => ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'],
        ], [
            'phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'identity' => 'No. Identitas',
            'name' => 'Nama',
            'gender' => 'Jns. Kelamin',
            'rule_birth_date' => 'Tgl. Lahir',
            'address' => 'Alamat',
            'village_id' => 'Daerah',
            'pos_code' => 'Kode Pos',
            'email' => 'Email',
            'phone' => 'No. Handphone',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $values['has_profile'] = true;
        $village = RegionRepository::getVillageById($values['village_id'], ['district', 'city', 'province']);
        $values['village_name'] = $village->name;
        $values['district_name'] = $village->district->name;
        $values['city_name'] = $village->district->city->name;
        $values['province_name'] = $village->district->city->province->name;

        DB::beginTransaction();
        try {
            $user->update($values);

            DB::commit();

            $messageCls = $user->has_active_bank ? 'success' : 'warning';
            $otherMessage = $user->has_active_bank ? '' : 'Anda tidak memiliki rekening bank yang aktif.';

            session([
                'message' => "Profile anda berhasil diperbarui. {$otherMessage}",
                'messageClass' => $messageCls,
            ]);

            $redirectTo = $user->has_active_bank ? 'profile.index' : 'bank.index';

            return response(route($redirectTo), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
