<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        session()->forget('landing-passed');

        return view('login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $creds = $request->only($this->username(), 'password');
        $creds['status'] = USER_STATUS_ACTIVE;

        return $creds;
    }

    protected function emailCredentials(Request $request)
    {
        $creds = $request->only('password');
        $creds['email'] = $request->get('username');
        $creds['status'] = USER_STATUS_ACTIVE;

        return $creds;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $okUsername = $this->guard()->attempt(
            $this->credentials($request),
            $request->boolean('remember')
        );

        return $okUsername;
        // ?:
        //     $this->guard()->attempt(
        //         $this->emailCredentials($request),
        //         $request->boolean('remember')
        //     );
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ($user->branch_user) {
            if (empty($user->branch) || !$user->branch->is_active) {
                $this->guard()->logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();

                session()->flash('loginFailed', $user->username);

                session([
                    'message' => __('auth.failed'),
                    'messageClass' => 'danger'
                ]);

                return response(route('login'), 200);
            }
        }

        $new_sessid = Session::getId();
        $last_session = ($handler = Session::getHandler())->read($user->session_id);
        if ($last_session) {
            $handler->destroy($user->session_id);
        }

        $user->saveSession($new_sessid);

        return response(route('home'), 200);
    }
}
