<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegistrationAgencyMail;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Rules\RulePassword;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    private function getReferral(Request $request)
    {
        return $request->registerReferral ?:
            User::where('user_group', '=', USER_GROUP_AGENCY)
            ->orderBy('id')
            ->first();
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm(Request $request)
    {
        return view('register-agent', ['referral' => $this->getReferral($request)]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:6', 'confirmed'],
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        $data['user_group'] = USER_GROUP_AGENCY;
        $data['user_type'] = USER_AGENCY_BASIC;
        $data['status'] = USER_STATUS_INACTIVE;

        return User::create($data);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        // $this->validator($request->all())->validate();

        $passwd = RulePassword::min(6)->numbers()->letters();
        $validator = Validator::make($data = $request->except(['_token']), [
            'username' => ['required', 'string', 'min:6', 'max:30', 'regex:/^[\w-]*$/', 'unique:users,username'],
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'max:100', 'email'],
            'phone' => ['required', 'min:9', 'max:15', 'regex:/(^\+628\d)|(^08\d)/'],
            'password' => ['required', 'string', 'confirmed', $passwd],
        ], [
            'phone.regex' => 'Format :attribute harus berawalan +628 atau 08.',
        ], [
            'username' => 'Username',
            'name' => 'Nama',
            'email' => 'Email',
            'phone' => 'No. Handphone',
            'password' => 'Password',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        $referral = $this->getReferral($request);
        $data['referral_id'] = $referral->id;

        DB::beginTransaction();
        try {
            $agency = $this->create($data);

            Mail::to($agency)->send(new RegistrationAgencyMail($agency));

            session([
                'message' => 'Pendaftaran berhasil. Silahkan periksa email anda dan tunggu konfirmasi dari admin kami. Terima kasih',
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('login'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();
            // $msg = $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
