<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\PackageFormTrait;
use App\Models\Branch;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class PackageController extends Controller
{
    use PackageFormTrait;

    public function index(Request $request)
    {
        $user = $request->user();
        $branches = Branch::byActive()->orderBy('name')->get();

        return view('packages.main-index', [
            'user' => $user,
            'branches' => $branches,
        ]);
    }

    public function dataTable(Request $request)
    {
        $user = $request->user();
        $query = Package::query();

        if (!$user->main_user) {
            $query = $query->byActive();
        }

        $editable = $user->main_user_super;

        return datatables()->eloquent($query)
            ->editColumn('is_active', function ($row) {
                return new HtmlString(contentCheck($row->is_active));
            })
            ->editColumn('price', function ($row) {
                return formatNumber($row->price, 0);
            })
            ->addColumn('view', function ($row) use ($editable) {
                // return $editable ? route('package.edit', ['mainPackage' => $row->id]) : '';
                if ($editable) {
                    $button = '<button type="button" class="btn btn-sm btn-success" data-href="' . route('package.edit', ['mainPackage' => $row->id]) . '" title="Edit Data"><i class="fa-solid fa-pencil-alt"></i></button>';

                    return new HtmlString($button);
                }

                return '';
            })
            ->toJson();
    }
}
