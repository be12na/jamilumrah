<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Customer\ActionTrait;
use App\Http\Controllers\Traits\Customer\ToBeAgency;
use App\Http\Controllers\Traits\Customer\FormTrait;
use App\Http\Controllers\Traits\Customer\ImportPalmer;
use App\Http\Controllers\Traits\Customer\RegisterPalmer;
use App\Http\Controllers\Traits\Customer\TransferTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class CustomerController extends Controller
{
    use FormTrait, TransferTrait, ActionTrait, ToBeAgency, RegisterPalmer, ImportPalmer;

    public function index(Request $request)
    {
        $user = $request->user();
        $filter = $request->get('filter', null);

        if (!is_null($filter)) {
            session(['filter.customerStatus' => $filter]);
            return redirect()->route('customer.index');
        }

        $statusId = session('filter.customerStatus', $user->main_user ? CUSTOMER_STATUS_TRANSFER : CUSTOMER_STATUS_REGISTER);

        $vars = [
            'user' => $user,
            'currentStatus' => $statusId,
        ];

        if ($user->agency_user) {
            return view('customers.agency-index', $vars);
        } elseif ($user->branch_user) {
            return view('customers.branch-index', $vars);
        }

        return view('customers.main-index', $vars);
    }

    public function dataTable(Request $request)
    {
        $user = $request->user();
        $statusId = intval($request->get('status_id', -1));

        // JSON_EXTRACT(customers.agency_detail, \"$.name\") as agency_name,
        // JSON_EXTRACT(customers.agency_detail, \"$.user_group\") as agency_group,
        // JSON_EXTRACT(customers.branch_detail, \"$.name\") as branch_name,
        // JSON_EXTRACT(customers.package_detail, \"$.name\") as package_name,
        // JSON_EXTRACT(customers.package_detail, \"$.price\") as package_price,
        // JSON_EXTRACT(customers.package_detail, \"$.description\") as package_description

        $baseQuery = DB::table('customers')
            ->selectRaw("customers.id, customers.code, customers.name, customers.email, customers.phone, customers.status, customers.is_agency, branches.name as branch_name, agen.name as agency_name, agen.user_group as agency_group, packages.name as package_name, packages.price as package_price, packages.description as package_description
            ")
            ->join('branches', 'branches.id', '=', 'customers.branch_id')
            ->join('packages', 'packages.id', '=', 'customers.package_id');

        if ($user->agency_user) {
            $baseQuery = $baseQuery
                ->join(DB::raw('users as agen'), 'agen.id', '=', 'customers.agency_id')
                ->where('customers.agency_id', '=', $user->id);
        } elseif ($user->branch_user) {
            $baseQuery = $baseQuery
                ->leftJoin(DB::raw('users as agen'), 'agen.id', '=', 'customers.agency_id')
                ->where('customers.branch_id', '=', $user->branch_id);
        } else {
            $baseQuery = $baseQuery->leftJoin(DB::raw('users as agen'), 'agen.id', '=', 'customers.agency_id');
        }

        if (in_array($statusId, array_keys(CUSTOMER_STATUS_LIST))) {
            $filterStatus = $statusId;
            if ($user->agency_user) {
                if ($statusId == CUSTOMER_STATUS_TRANSFER) {
                    $filterStatus = [CUSTOMER_STATUS_TRANSFER, CUSTOMER_STATUS_REJECTED];
                }
            }

            if (is_array($filterStatus)) {
                $baseQuery = $baseQuery->whereIn('customers.status', $filterStatus);
            } else {
                $baseQuery = $baseQuery->where('customers.status', '=', $filterStatus);
            }
        }

        $sql = $baseQuery->toSql();

        $query = DB::table(DB::raw("({$sql}) as q"))->mergeBindings($baseQuery)
            ->selectRaw("id, code, name, email, phone, status, agency_name, agency_group, is_agency, branch_name, package_name, package_price, package_description");

        session(['filter.customerStatus' => $statusId]);
        $statusList = customerStatusList($user);

        return datatables()->query($query)
            ->editColumn('name', function ($row) {
                $content = "<div class=\"fw-bold\">{$row->name}</div>";
                if ($row->is_agency == 1) {
                    $content .= "<span class=\"badge bg-primary\">Agen</span>";
                }

                return new HtmlString($content);
            })
            ->editColumn('agency_name', function ($row) {
                return mainAgency($row, 'agency_group') ? 'Kantor Pusat' : trim($row->agency_name, '"');
            })
            ->editColumn('branch_name', function ($row) {
                return trim($row->branch_name, '"');
            })
            ->editColumn('package_name', function ($row) {
                $pkgName = trim($row->package_name, '"');
                $price = formatNumber($row->package_price, 0);
                $html = "<div>{$pkgName}</div><div>Rp {$price}</div>";

                return new HtmlString($html);
            })
            ->editColumn('status', function ($row) use ($statusList) {
                $status = Arr::get($statusList, $row->status, []);
                $html = '';
                if (!empty($status)) {
                    $text = $status[0];
                    $cls = $status[1];
                    $html = "<span class=\"badge badge-pill {$cls}\" style=\"--bs-badge-font-size:inherit;\">{$text}</span>";
                }

                return new HtmlString($html);
            })
            ->addColumn('view', function ($row) use ($user) {
                $buttons = [];
                // detail
                $detailRoute = route('customer.detail', ['customer' => $row->id]);
                $buttons[] = "<button type=\"button\" class=\"btn btn-sm btn-info\" data-href=\"{$detailRoute}\">Detail</button>";

                if (($row->status == CUSTOMER_STATUS_REGISTER) && $user->main_user_super) {
                    $cancelRoute = route('customer.cancel.view', ['customerCancel' => $row->id]);
                    $buttons[] = "<button type=\"button\" class=\"btn btn-sm btn-danger\" data-bs-toggle=\"modal\" data-bs-target=\"#my-action-modal\" data-modal-url=\"{$cancelRoute}\">Batalkan</button>";
                }

                $content = '<div class="d-block">%s</div>';

                return new HtmlString(sprintf($content, implode(' ', $buttons)));
            })
            ->escapeColumns()
            ->toJson();
    }

    public function detail(Request $request)
    {
        $user = $request->user();
        $customer = $request->customer;

        return view('customers.detail', [
            'user' => $user,
            'customer' => $customer,
        ]);
    }
}
