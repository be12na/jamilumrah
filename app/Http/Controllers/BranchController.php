<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Branch\FormTrait;
use App\Models\Branch;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;

class BranchController extends Controller
{
    use FormTrait;

    public function index(Request $request)
    {
        $user = $request->user();

        if ($user->main_user) {
            return view('branches.main-index', [
                'user' => $user
            ]);
        }
    }

    public function dataTable(Request $request)
    {
        $user = $request->user();

        $editable = $user->main_user_super;
        $eloquent = Branch::query()->with('admin');

        return datatables()->eloquent($eloquent)
            ->editColumn('is_active', function ($row) {
                return new HtmlString(contentCheck($row->is_active));
            })
            ->addColumn('full_address', function ($row) {
                return $row->complete_address;
            })
            ->addColumn('phone', function ($row) {
                $arr = [$row->phone];
                if ($row->fax) {
                    $arr[] = $row->fax;
                }

                return implode(' / ', $arr);
            })
            ->addColumn('leader', function ($row) {
                if (empty($row->leader_name)) return '-';

                $content = "<div>{$row->leader_name}</div>";
                $content .= "<div>({$row->leader_phone})</div>";

                return new HtmlString($content);
            })
            ->addColumn('administrator', function ($row) {
                if (empty($row->admin)) return '-';

                $content = "<div>{$row->admin->name}</div>";
                $content .= "<div>({$row->admin->phone})</div>";

                return new HtmlString($content);
            })
            ->addColumn('view', function ($row) use ($editable) {
                if ($editable) {
                    $button = '<button type="button" class="btn btn-sm btn-success me-2" data-href="' . route('branch.edit', ['mainBranch' => $row->id]) . '" title="Edit Data"><i class="fa-solid fa-pencil-alt"></i></button>';

                    $button .= '<button type="button" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#modal-admin" data-modal-url="' . route('branch.admin.form', ['mainBranch' => $row->id]) . '" title="Admin Cabang"><i class="fa-solid fa-user-secret"></i></button>';

                    return new HtmlString($button);
                }

                return '';
            })
            ->toJson();
    }

    public function packageOptions(Request $request)
    {
        $packages = Package::byActive()->orderBy('price', 'desc')->get();
        $format = '<option value="%s" data-package-description="%s" data-package-price="%s">%s</option>';

        $result = sprintf($format, '', '', '', '-- Pilih Paket --');
        foreach ($packages as $package) {
            $result .= sprintf($format, $package->id, $package->description, formatNumber($package->price), $package->name);
        }

        return $result;
    }

    // filter by province / city (area)
    public function byArea(Request $request)
    {
        $provinceName = $request->province ?? 'xxxxxx';
        $cityName = $request->city ?? '';
        $branches = Branch::query()->byActive()->likeProvinceName($provinceName);

        if (!empty($cityName)) {
            $branches = $branches->likeCityName($cityName);
        }

        $branches = $branches->orderBy('name')->get();

        $result = '<option value="" data-branch-address="" data-branch-contact="">-- Pilih Cabang --</option>';
        foreach ($branches as $branch) {
            $adminName = $branch->admin ? $branch->admin->name . '.' : '';
            $adminPhone = optional($branch->admin)->phone;
            $leaderName = $branch->leader_name ? $branch->leader_name . '.' : '';
            $leaderPhone = $branch->leader_phone ?? '';

            $result .= "<option value=\"{$branch->id}\" data-branch-address=\"{$branch->complete_address}\" data-branch-leader-name=\"{$leaderName}\" data-branch-leader-phone=\"{$leaderPhone}\" data-branch-admin-name=\"{$adminName}\" data-branch-admin-phone=\"{$adminPhone}\">{$branch->name}</option>";
        }

        return response($result);
    }
}
