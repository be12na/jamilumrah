<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPasswordMail;
use App\Mail\ForgotUsernameMail;
use App\Models\User;
use App\Rules\RulePassword;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Nette\Utils\Random;

class ForgotController extends Controller
{
    public function viewForgotUsername(Request $request)
    {
        return view('forgot.username');
    }

    public function submitForgotUsername(Request $request)
    {
        $email = $request->get('email');

        $users = User::query()->byEmail($email)
            ->byStatus(USER_STATUS_ACTIVE)
            ->orderBy('name')
            ->orderBy('username')
            ->get();

        if ($users->isEmpty()) {
            $message = "Akun dengan email <b>{$email} tidak terdaftar.</b>";

            return response(errorMessageContent($message, 'danger', 'Error'), 400);
        }

        try {
            Mail::to($email)->send(new ForgotUsernameMail($users, $email));

            session([
                'message' => "Kami telah mengirimkan daftar akun melalui email <b>{$email}</b>",
                'messageClass' => 'success'
            ]);

            return response(route('login'), 200);
        } catch (\Exception $e) {
            $msg = isLive() ? 'Gagal mengirimkan email.' : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function selectForgotUsername(Request $request)
    {
        session(['loginFailed' => $request->forgotUsername]);

        return redirect()->route('login');
    }

    public function viewForgotPassword(Request $request)
    {
        return view('forgot.password');
    }

    public function submitForgotPassword(Request $request)
    {
        $username = $request->get('username');
        $email = $request->get('email');

        $user = User::query()->byUsername($username)->byEmail($email)->byStatus(USER_STATUS_ACTIVE)->first();

        if (empty($user)) {
            $message = "Akun dengan username <b>{$username}</b> dan email <b>{$email}</b> tidak terdaftar.";

            return response(errorMessageContent($message, 'danger', 'Error'), 400);
        }

        $timeReset = Carbon::now();

        DB::beginTransaction();
        try {
            $user->update([
                'password_token' => Random::generate(32),
                'password_token_at' => $timeReset,
                'password_exp_at' => $timeReset->addMinutes(10),
            ]);

            Mail::to($email)->send(new ForgotPasswordMail($user));

            session([
                'message' => "Tautan untuk mengatur ulang password telah dikirim ke email <b>{$email}</b>",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('login'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? 'Gagal mengirimkan email.' : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }

    public function resetForgotPassword(Request $request)
    {
        $user = User::query()->byResetPasswordToken($request->forgotPasswordToken)->first();
        $format = 'YmdHis';
        $now = Carbon::now()->format($format);

        if (empty($user) || ($user->password_exp_at->format($format) < $now)) abort(404);

        if (strtolower($request->method()) != 'post') {
            return view('forgot.password-reset', [
                'user' => $user,
            ]);
        }

        $values = $request->except(['_token']);
        $passwd = RulePassword::min(6)->numbers()->letters();

        $validator = Validator::make($values, [
            'password' => ['required', 'string', 'confirmed', $passwd],
        ], [], [
            'password' => 'Password',
        ]);

        if ($validator->fails()) {
            return response(errorMessageContent($validator), 400);
        }

        DB::beginTransaction();
        try {
            $user->update([
                'password' => Hash::make($values['password']),
                'password_token' => null, // reset token
                'password_reset_at' => Carbon::now(),
            ]);

            session([
                'message' => "Password untuk akun <b>{$user->username}</b> berhasil diganti.",
                'messageClass' => 'success'
            ]);

            DB::commit();

            return response(route('login'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $msg = isLive() ? MESSAGE_500 : $e->getMessage();

            return response(errorMessageContent($msg), 500);
        }
    }
}
