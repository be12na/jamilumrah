<?php

namespace App\Notifications\Register;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PalmerNotification extends Notification
{
    use Queueable;

    private Customer $customer;
    private string $usingVia;
    private string|null $forDriver;

    public $content;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, string $usingVia, string $driver = null)
    {
        $this->customer = $customer;
        $this->usingVia = strtolower($usingVia);
        $this->forDriver = $driver;

        if ($this->usingVia == 'onesender') $this->setWhatsappContent();
    }

    private function setWhatsappContent(): void
    {
        $customer = $this->customer;
        $branch = $customer->branch;
        $appName = config('app.name');
        $admin = $branch->admin;
        $package = $customer->package;

        $contents = [
            "*Informasi Pendaftaran Jamaah*",
            "*{$appName}*",
            "",
            "Selamat datang *{$customer->name}*",
            "Berikut adalah data kepesertaan anda sebagai jamaah kami:",
            "",
        ];

        $contentKp = [
            "*Kantor Perwakilan*",
            "*{$branch->name}*",
            $branch->complete_address,
            "Kontak: {$admin->name} - {$admin->phone}",
            "",
        ];

        $price = formatNumber($package->price, 0);

        $contentPackage = [
            "*Program*",
            $package->name,
            "Harga Rp {$price}",
        ];

        if ($package->description) {
            $contentPackage[] = $package->description;
        }

        $contentPackage[] = "";

        $contentPersonal = [
            "*Data Personal*",
            "No. Identitas: {$customer->identity}",
            "Nama: {$customer->name}",
            "Kelamin: {$customer->gender_name}",
            "Tempat Lahir: {$customer->birth_city}",
            "Tanggal Lahir: " . translateDatetime($customer->birth_date, 'd F Y'),
            "Alamat: {$customer->complete_address}",
            "",
        ];

        $contentFooter = [
            "Pihak Kantor Perwakilan yang dipilih akan segera menghubungi anda untuk proses lebih lanjut.",
            "Jika ada ketidaksesuaian data, silahkan hubungi agen kami yang telah mendaftarkan anda. Terima kasih",
        ];

        $this->content = implode("\r\n", array_merge($contents, $contentKp, $contentPackage, $contentPersonal, $contentFooter));
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [$this->usingVia];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Informasi Pendaftaran Jamaah')
            ->view('email.registration-customer', [
                'customer' => $this->customer
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'driver' => $this->forDriver
        ];
    }
}
