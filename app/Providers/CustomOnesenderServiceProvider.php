<?php

namespace App\Providers;

use App\Libs\CustomOneSender;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\Facades\Notification;
use Pasya\OneSenderLaravel\OneSenderServiceProvider;

class CustomOnesenderServiceProvider extends OneSenderServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomOneSender::class, function ($app) {
            $apiUrl = config('onesender.base_api_url') . '/api/v1/messages';
            $apiKey = config('onesender.api_key');

            return new CustomOneSender($apiUrl, $apiKey);
        });

        Notification::resolved(function (ChannelManager $service) {
            $service->extend('onesender', function ($app) {
                return $app->get(CustomOneSender::class);
            });
        });
    }
}
