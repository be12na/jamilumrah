<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\HtmlString;
use Illuminate\Support\ServiceProvider;

class BladeDirectiveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // if
        Blade::if('isLive', function () {
            return isLive();
        });

        Blade::if('hasProfile', function ($user = null) {
            return hasProfile($user);
        });

        Blade::if('canBankAction', function (string $actionName, $user = null) {
            return canBankAction($actionName, $user);
        });

        Blade::if('canEditCustomer', function ($customer, $user = null) {
            return canEditCustomer($customer, $user);
        });

        Blade::if('super', function () {
            return auth()->user() ? auth()->user()->main_user_super : false;
        });

        // directive string
        Blade::directive('menuActive', function ($requireNames) {
            return "<?php echo matchMenu($requireNames) ? 'active' : ''; ?>";
        });

        Blade::directive('sidebarActiveOrCollapsed', function ($requireNames) {
            return "<?php echo matchMenu($requireNames) ? 'active' : 'collapsed'; ?>";
        });

        Blade::directive('sidebarShow', function ($requireNames) {
            return "<?php echo matchMenu($requireNames) ? 'show' : ''; ?>";
        });

        Blade::directive('formatNumber', function ($value, $decimals = 0) {
            return "<?php echo formatNumber($value, $decimals); ?>";
        });

        Blade::directive('translateDatetime', function ($value, $format = null) {
            return "<?php echo translateDatetime($value, $format); ?>";
        });

        Blade::directive('dayName', function ($value) {
            return "<?php echo dayName($value); ?>";
        });

        Blade::directive('contentCheck', function ($value) {
            return "<?php echo contentCheck((bool) $value); ?>";
        });

        Blade::directive('optionSelected', function ($data_value, $true_value = '0') {
            return "<?php echo optionSelected($data_value, $true_value); ?>";
        });
    }
}
