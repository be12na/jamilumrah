<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AplikasiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();
        $this->routeBindingModel();
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('byIP100', function (Request $request) {
            return Limit::perMinute(100)->by($request->ip())->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('byIP50', function (Request $request) {
            return Limit::perMinute(50)->by($request->ip())->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('byIP20', function (Request $request) {
            return Limit::perMinute(20)->by($request->ip())->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession50', function (Request $request) {
            return Limit::perMinute(50)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession100', function (Request $request) {
            return Limit::perMinute(100)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession200', function (Request $request) {
            return Limit::perMinute(200)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });
    }

    protected function routeBindingModel()
    {
        Route::bind('registerReferral', function ($value) {
            return fallbackRouteBinding(
                \App\Models\User::byUsername($value)
                    ->byHasProfile(true)
                    ->where('user_group', '=', USER_GROUP_AGENCY)
                    ->where('status', '=', USER_STATUS_ACTIVE)
                    ->first(),
                "Referral {$value} tidak ditemukan.",
                'login'
            );
        });

        Route::bind('registerPalmerByToken', function ($value) {
            return fallbackRouteBinding(
                \App\Models\User::byLinkToken($value)
                    ->byHasProfile(true)
                    ->where(function ($qWhere) {
                        return $qWhere->where(function ($where1) {
                            return $where1->where('user_group', '=', USER_GROUP_BRANCH)
                                ->where('user_type', '=', USER_BRANCH_ADMIN);
                        })->orWhere('user_group', '=', USER_GROUP_AGENCY);
                    })
                    ->where('status', '=', USER_STATUS_ACTIVE)
                    ->first(),
                "Agen Referral {$value} tidak ditemukan.",
                'login'
            );
        });

        Route::bind('mainBranch', function ($value) {
            return fallbackRouteBinding(
                \App\Models\Branch::byId($value)->first()
            );
        });

        Route::bind('mainPackage', function ($value) {
            return fallbackRouteBinding(
                \App\Models\Package::byId($value)->first()
            );
        });

        Route::bind('mainAgent', function ($value, RoutingRoute $route) {
            $query = \App\Models\User::byId($value)->byAgencyUser();
            if ($route->parameter('agentRegister') == 1) {
                $query = $query->byActivated(false)->byStatus(USER_STATUS_INACTIVE);
            } else {
                $query = $query->byStatus(USER_STATUS_ACTIVE);
            }

            return fallbackRouteBinding(
                $query->first()
            );
        });

        Route::bind('userBank', function ($value) {
            return fallbackRouteBinding(
                \App\Models\UserBank::byId($value)->with('bank')->first()
            );
        });

        Route::bind('customer', function ($value) {
            $user = auth()->user();

            if (empty($user)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value);

            if (!$user->main_user) {
                $query = $query->byOwner($user);
            }

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('customerEditable', function ($value) {
            $user = auth()->user();

            if (empty($user) || (!$user->branch_user_admin)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value);

            if (!$user->main_user) {
                $query = $query->byOwner($user);
            }

            $query = $query->byEditable();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('customerTransferable', function ($value) {
            $user = auth()->user();

            if (empty($user) || (!$user->branch_user_admin && !$user->main_user_super)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value);

            if (!$user->main_user) {
                $query = $query->byOwner($user);
            }

            $query = $query->byTransferable();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('customerReject', function ($value) {
            $user = auth()->user();

            if (empty($user) || (!$user->main_user_super)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value)->byApproving();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('customerApprove', function ($value) {
            $user = auth()->user();

            if (empty($user) || (!$user->main_user_super)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value)->byApproving();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('customerCancel', function ($value) {
            $user = auth()->user();

            if (empty($user) || (!$user->main_user_super)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value)->byCanceling();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('customerRemove', function ($value) {
            $user = auth()->user();

            if (empty($user) || (!$user->main_user_super)) $value = 0;

            $query = \App\Models\Customer::query()->byId($value);

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'customer.index'
            );
        });

        Route::bind('unRegisteredCustomer', function ($value) {
            $query = \App\Models\Customer::query()->byRegisterToken($value)->byRegisteringAgency();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.',
                'login'
            );
        });

        Route::bind('unRegisteredCustomerId', function ($value) {
            $query = \App\Models\Customer::query()->byId($value)->byRegisteringAgency();

            return fallbackRouteBinding(
                $query->first(),
                'Data Jamaah tidak ditemukan.'
            );
        });

        Route::bind('document', function ($value) {
            return fallbackRouteBinding(
                \App\Models\Document::byCode($value)->first()
            );
        });
    }
}
