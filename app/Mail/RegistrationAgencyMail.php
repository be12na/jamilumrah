<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationAgencyMail extends Mailable
{
    use Queueable, SerializesModels;

    private User $agency;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $agency)
    {
        $this->agency = $agency;
        $this->subject('Pendaftaran Agen');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.registration-agency', [
            'agency' => $this->agency
        ]);
    }
}
