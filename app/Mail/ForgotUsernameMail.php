<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotUsernameMail extends Mailable
{
    use Queueable, SerializesModels;

    private Collection $users;
    private string $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Collection $users, string $email)
    {
        $this->users = $users;
        $this->email = $email;
        $this->subject('Permohonan Lupa Username');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.forgot-username', [
            'users' => $this->users,
            'email' => $this->email,
        ]);
    }
}
