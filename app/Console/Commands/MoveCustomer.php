<?php

namespace App\Console\Commands;

use App\Models\BonusBranch;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MoveCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zzz:move-customer {customer : Mencari data customer beradasarkan options table dan field. Available(user-id, user-username, customer-id, customer-code)} {branch : ID KP tujuan} {--table=user : Mencari data customer dari Table. Available(user, customer)} {--field=id : Pencarian data pada field dari table} {--reset-bonus=1 : Reset atau rebuild bonus jika bonus sudah tercatat}';
    // {customer} {--table=user} {--field=id} {--reset-bonus=1}

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pindahkan cabang jamaah dan rebuild bonus';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $findValue = explode(',', $this->argument('customer'));
        $options = $this->options();
        $isCustomerTable = ($options['table'] == 'customer');
        $model = $isCustomerTable ? Customer::class : User::class;
        $dataValues = $model::query()->whereIn($options['field'], $findValue)->get();
        $branchTarget = Branch::query()->byId($this->argument('branch'))->first();

        if ($dataValues->isNotEmpty() && !empty($branchTarget)) {
            $getAll = (count($findValue) == $dataValues->count());
            $needConfirm = !$getAll;
            $confirmed = true;
            $branchArray = $branchTarget->toArray();

            if ($needConfirm) {
                $exists = $dataValues->pluck($options['field'])->toArray();
                $notValue = implode('", "', array_filter($findValue, function ($value) use ($exists) {
                    return !in_array($value, $exists);
                }));

                $confirmed = $this->confirm("Parameter jamaah: \"{$notValue}\" tidak ditemukan. Lanjutkan?");
            }

            if ($confirmed) {
                DB::beginTransaction();

                try {
                    foreach ($dataValues as $data) {
                        $customer = $isCustomerTable ? $data : $data->fromCustomer;
                        if ($branchTarget->id == $customer->branch_id) continue;

                        $customer->update([
                            'branch_id' => $branchTarget->id,
                            'branch_detail' => $branchArray,
                        ]);

                        if (($options['reset-bonus'] == 1)
                            && ($bonusKP = BonusBranch::query()->byCustomers($customer->id)->get())->isNotEmpty()
                        ) {
                            $customerArray = $customer->toArray();
                            $bonusIds = $bonusKP->pluck('id')->toArray();

                            DB::table('bonus_branches')->whereIn('id', $bonusIds)->update([
                                'branch_id' => $branchTarget->id,
                                'customer_detail' => $customerArray,
                            ]);
                        }
                    }

                    DB::commit();

                    $this->info("Jamaah berhasil dipindahkan.");
                } catch (\Exception $e) {
                    DB::rollBack();

                    $this->error($e->getMessage());
                }
            } else {
                $this->info('Proses dibatalkan.');
            }
        } else {
            $dataValues->isEmpty() ? $this->error('Jamaah tidak ditemukan.') : $this->error('KP tujuan tidak ditemukan.');
        }
    }
}
