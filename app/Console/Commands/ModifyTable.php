<?php

namespace App\Console\Commands;

use App\Models\Setting;
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ModifyTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zzz:database-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modifikasi atau tambah table di database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->modifyTableField();
    }

    private function modifyTableField()
    {
        // Schema::table('bonus_levels', function (Blueprint $table) {
        //     $table->string('bonus_info', 250)->nullable();
        // });

        // Schema::table('bonus_branches', function (Blueprint $table) {
        //     $table->string('bonus_info', 250)->nullable();
        // });

        Schema::table('customers', function (Blueprint $table) {
            $table->json('agency_detail')->nullable()->change();
            $table->json('branch_detail')->nullable()->change();
            $table->json('package_detail')->nullable()->change();
        });

        Schema::table('bonus_levels', function (Blueprint $table) {
            $table->json('level_detail')->nullable()->change();
        });

        DB::table('customers')->update([
            'agency_detail' => null,
            'branch_detail' => null,
            'package_detail' => null,
        ]);

        DB::table('bonus_sponsors')->update([
            'customer_detail' => null,
        ]);

        DB::table('bonus_branches')->update([
            'customer_detail' => null,
        ]);

        DB::table('bonus_levels')->update([
            'customer_detail' => null,
            'level_detail' => null,
        ]);

        // tanpa bonusan
        $setting = Setting::query()->first();
        $setting->update([
            'bonus_sponsor' => 0,
            'bonus_level' => [
                // ['level' => 1, 'bonus' => 1000000],
                // ['level' => 2, 'bonus' => 500000],
                ['level' => 1, 'bonus' => 0],
                ['level' => 2, 'bonus' => 0],
            ],
            'bonus_point' => [
                'referred_count' => 450,
                // 'bonus' => 50000,
                'bonus' => 0,
            ],
            // 'bonus_branch' => 450000,
            'bonus_branch' => 0,
        ]);
    }
}
