<?php

namespace App\Console\Commands;

use App\Models\BonusBranch;
use App\Models\BonusLevel;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Console\Command;

class ResetData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zzz:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset semua data agen, jamaah dan ujroh.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $agencies = User::query()
        //     ->byAgencyUser()
        //     ->whereNotIn('username', ['Kgs', 'top1'])
        //     ->get();

        // foreach ($agencies as $agency) {
        //     if ($agency->structure) {
        //         $agency->structure->deleteSubTree(true);
        //     }

        //     $agency->delete();
        // }

        // BonusLevel::truncate();
        // BonusBranch::truncate();
        // Customer::truncate();
    }
}
