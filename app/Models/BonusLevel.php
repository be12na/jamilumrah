<?php

namespace App\Models;

use App\Casts\JsonCollectionCast;
use App\Casts\JsonObjectCast;
use App\Models\Traits\Relationship\BelongsCustomer;
use App\Models\Traits\Relationship\BelongsUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusLevel extends Model
{
    use HasFactory;
    use BelongsUser, BelongsCustomer;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'bonus_date',
        'user_id',
        'customer_id',
        'customer_detail',
        'level',
        'level_detail',
        'bonus',
        'setting_id',
        'status',
        'status_at',
        'status_note',
        'status_by',
        'bonus_info',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'bonus_date' => 'date',
        'customer_detail' => JsonObjectCast::class,
        'level' => 'integer',
        'level_detail' => JsonCollectionCast::class,
        'bonus' => 'integer',
        'status' => 'integer',
    ];

    // scope
    public function scopeByUser(Builder $builder, $branchId): Builder
    {
        return $builder->where('user_id', '=', $branchId);
    }

    public function scopeByPending(Builder $builder): Builder
    {
        return $builder->where('status', '=', BONUS_STATUS_PENDING);
    }

    public function scopeByTransferred(Builder $builder): Builder
    {
        return $builder->where('status', '=', BONUS_STATUS_TRANSFERRED);
    }
}
