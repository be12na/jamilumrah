<?php

namespace App\Models\Traits\Relationship;

use App\Models\Customer;

trait BelongsCustomer
{
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
