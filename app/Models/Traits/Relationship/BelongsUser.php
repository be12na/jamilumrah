<?php

namespace App\Models\Traits\Relationship;

use App\Models\User;

trait BelongsUser
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
