<?php

namespace App\Models\Traits\Relationship;

use App\Models\Branch;

trait BelongsBranch
{
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }
}
