<?php

namespace App\Models\Traits\Relationship;

use App\Models\Package;

trait BelongsPackage
{
    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }
}
