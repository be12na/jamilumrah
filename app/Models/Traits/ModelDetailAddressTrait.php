<?php

namespace App\Models\Traits;

use Illuminate\Support\HtmlString;

trait ModelDetailAddressTrait
{
    public function getDetailCompleteAddressAttribute()
    {
        if (!isset($this->detailColumnHasAddress)) return null;

        $columnName = $this->detailColumnHasAddress;
        $value = optional((object) $this->$columnName);

        $addresses = [];
        if ($value->address) $addresses[] = $value->address;
        if ($value->village_name) $addresses[] = $value->village_name;
        if ($value->district_name) $addresses[] = 'Kec. ' . $value->district_name;
        if ($value->city_name) $addresses[] = $value->city_name;
        if ($value->province_name) $addresses[] = $value->province_name;

        return implode(', ', $addresses) . ($value->pos_code ? '. Kode Pos ' . $value->pos_code : '');
    }

    public function getDetailContentAddressAttribute()
    {
        if (!isset($this->detailColumnHasAddress)) return null;

        $columnName = $this->detailColumnHasAddress;
        $value = optional((object) $this->$columnName);

        $addresses = [];
        if ($value->address) $addresses[] = $value->address;
        if ($value->village_name) $addresses[] = $value->village_name;
        if ($value->district_name) $addresses[] = 'Kec. ' . $value->district_name;
        if ($value->city_name) $addresses[] = $value->city_name;
        if ($value->province_name) $addresses[] = $value->province_name;

        $strAddress = implode(',</span><span>', $addresses);
        $strAddress = "<span>{$strAddress}.</span>";

        if ($value->pos_code) {
            $strAddress .= "<span>Kode Pos {$value->pos_code}</span>";
        }

        return new HtmlString($strAddress);
    }
}
