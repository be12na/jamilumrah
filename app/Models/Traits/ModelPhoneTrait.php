<?php

namespace App\Models\Traits;

trait ModelPhoneTrait
{
    public function getPhoneWithoutCodeAttribute()
    {
        if (empty($this->phone)) return null;

        $phone = $this->phone;
        if (substr($phone, 0, 2) == '08') return substr($phone, 2);
        if (substr($phone, 0, 3) == '+62') return substr($phone, 3);

        return $phone;
    }
}
