<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ModelAddressTrait
{
    // scope
    public function scopeLikeProvinceName(Builder $builder, string $provinceName): Builder
    {
        return $builder->where('province_name', 'like', "%{$provinceName}%");
    }

    public function scopeLikeCityName(Builder $builder, string $cityName): Builder
    {
        return $builder->where('city_name', 'like', "%{$cityName}%");
    }

    // accessor
    public function getCompleteAddressAttribute()
    {
        $addresses = [];
        if ($this->address) $addresses[] = $this->address;
        if ($this->village_name) $addresses[] = $this->village_name;
        if ($this->district_name) $addresses[] = 'Kec. ' . $this->district_name;
        if ($this->city_name) $addresses[] = $this->city_name;
        if ($this->province_name) $addresses[] = $this->province_name . '.';
        // if ($this->pos_code) $addresses[] = $this->pos_code;

        return implode(', ', $addresses) . ($this->pos_code ? ' Kode Pos ' . $this->pos_code : '');
    }
}
