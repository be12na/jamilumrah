<?php

namespace App\Models\Traits\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

trait UserTypeTrait
{
    // scope
    public function scopeByMainUser(Builder $builder): Builder
    {
        return $builder->where('user_group', '=', USER_GROUP_MAIN);
    }

    public function scopeByMainUserType(Builder $builder, int $type): Builder
    {
        return $builder->byMainUser()->where('user_type', '=', $type);
    }

    public function scopeByAgencyUser(Builder $builder): Builder
    {
        return $builder->where('user_group', '=', USER_GROUP_AGENCY);
    }

    public function scopeByBranchUser(Builder $builder): Builder
    {
        return $builder->where('user_group', '=', USER_GROUP_BRANCH);
    }

    public function scopeByBranchUserAdmin(Builder $builder): Builder
    {
        return $builder->byBranchUser()->where('user_type', '=', USER_BRANCH_ADMIN);
    }

    // accessor
    // main user
    public function getMainUserAttribute()
    {
        return ($this->user_group == USER_GROUP_MAIN);
    }

    public function getMainUserSuperAttribute()
    {
        return ($this->main_user && ($this->user_type == USER_MAIN_SUPER));
    }

    public function getMainUserMasterAttribute()
    {
        return ($this->main_user && ($this->user_type == USER_MAIN_MASTER));
    }

    public function getMainUserAdminAttribute()
    {
        return ($this->main_user && ($this->user_type == USER_MAIN_ADMIN));
    }

    // travel user
    public function getBranchUserAttribute()
    {
        return ($this->user_group == USER_GROUP_BRANCH);
    }

    public function getBranchUserAdminAttribute()
    {
        return ($this->branch_user && ($this->user_type == USER_BRANCH_ADMIN));
    }

    // agency user
    public function getAgencyUserAttribute()
    {
        return ($this->user_group == USER_GROUP_AGENCY);
    }

    public function getAgencyUserBasicAttribute()
    {
        return ($this->agency_user && ($this->user_type == USER_AGENCY_BASIC));
    }

    public function getAgencyUserExecutiveAttribute()
    {
        return ($this->agency_user && ($this->user_type == USER_AGENCY_EXECUTIVE));
    }

    public function getAgencyUserDirectorAttribute()
    {
        return ($this->agency_user && ($this->user_type == USER_AGENCY_EXECUTIVE_DIRECTOR));
    }

    // type name
    public function getUserTypeNameAttribute()
    {
        $arr = [];

        if ($this->main_user) {
            $arr = USER_MAIN_NAMES;
        } elseif ($this->branch_user) {
            $arr = USER_BRANCH_NAMES;
        } elseif ($this->agency_user) {
            $arr = USER_AGENCY_NAMES;
        }

        return Arr::get($arr, $this->user_type);
    }
}
