<?php

namespace App\Models\Traits\User;

use App\Models\Structure;

trait UserStructureTrait
{
    public function structure()
    {
        return $this->hasOne(Structure::class, 'user_id', 'id');
    }
}
