<?php

namespace App\Models\Traits\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

trait UserStatusTrait
{
    // scope
    public function scopeByStatus(Builder $builder, array|int|string $status): Builder
    {
        if (!is_array($status)) $status = [$status];

        return $builder->whereIn('status', $status);
    }

    public function scopeByActivated(Builder $builder, bool $activated = null): Builder
    {
        if (!is_null($activated)) {
            if ($activated) {
                return $builder->whereNotNull('activated_at');
            }

            return $builder->whereNull('activated_at');
        }

        return $builder;
    }

    // accessor
    public function getIsActiveAttribute()
    {
        return ($this->status == USER_STATUS_ACTIVE);
    }

    public function getIsRegisterAgentAttribute()
    {
        return (($this->status == USER_STATUS_INACTIVE) && is_null($this->activated_at));
    }

    public function getStatusNameAttribute()
    {
        return Arr::get(USER_STATUS_NAMES, $this->status);
    }
}
