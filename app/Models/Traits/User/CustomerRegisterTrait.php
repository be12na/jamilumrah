<?php

namespace App\Models\Traits\User;

use App\Models\Customer;

trait CustomerRegisterTrait
{
    public function registering()
    {
        return $this->hasMany(Customer::class, 'agency_id', 'id');
    }
}
