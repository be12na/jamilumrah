<?php

namespace App\Models\Traits\User;

use App\Models\BonusLevel;
use App\Models\BonusPoint;
use App\Models\BonusSponsor;

trait UserBonusTrait
{
    public function bonusSponsor()
    {
        return $this->hasMany(BonusSponsor::class, 'user_id', 'id');
    }

    public function bonusLevel()
    {
        return $this->hasMany(BonusLevel::class, 'user_id', 'id');
    }

    public function bonusPoint()
    {
        return $this->hasMany(BonusPoint::class, 'user_id', 'id');
    }

    // accessor
    public function getSummaryBonusAttribute()
    {
        // $qSponsor = $this->bonusSponsor();
        $qLevel = $this->bonusLevel();
        // $qPoint = $this->bonusPoint();

        $totalSponsor = 0; //$qSponsor->sum('bonus');
        $totalLevel = $qLevel->sum('bonus');
        $totalPoint = 0; //$qPoint->sum('bonus');
        $totalBonus = $totalSponsor + $totalLevel + $totalPoint;

        $transferSponsor = 0; //$qSponsor->where('status', '=', BONUS_STATUS_TRANSFERRED)->sum('bonus');
        $transferLevel = $qLevel->where('status', '=', BONUS_STATUS_TRANSFERRED)->sum('bonus');
        $transferPoint = 0; //$qPoint->where('status', '=', BONUS_STATUS_TRANSFERRED)->sum('bonus');
        $totalTransfer = $transferSponsor + $transferLevel + $transferPoint;

        return (object) [
            // 'sponsor' => (object) [
            //     'total' => $totalSponsor,
            //     'transferred' => $transferSponsor,
            // ],
            'level' => (object) [
                'total' => $totalLevel,
                'transferred' => $transferLevel,
            ],
            // 'point' => (object) [
            //     'total' => $totalPoint,
            //     'transferred' => $transferPoint,
            // ],
            'total' => $totalBonus,
            'transferred' => $totalTransfer,
        ];
    }
}
