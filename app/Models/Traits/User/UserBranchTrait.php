<?php

namespace App\Models\Traits\User;

use App\Models\Branch;

trait UserBranchTrait
{
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }
}
