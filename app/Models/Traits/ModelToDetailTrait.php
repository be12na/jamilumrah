<?php

namespace App\Models\Traits;

trait ModelToDetailTrait
{
    public function toDetail(): array
    {
        $only = ['id'];
        if (isset($this->ignoresDetail) && is_array($this->ignoresDetail) && !empty($this->ignoresDetail)) {
            $only = array_unique(array_merge($only, $this->ignoresDetail));
        }

        return $this->only($only);
    }
}
