<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ModelIDTrait
{
    public function scopeById(Builder $builder, $id):Builder
    {
        return $builder->where('id', '=', $id);
    }
}
