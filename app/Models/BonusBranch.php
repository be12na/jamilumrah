<?php

namespace App\Models;

use App\Casts\JsonObjectCast;
use App\Models\Traits\Relationship\BelongsBranch;
use App\Models\Traits\Relationship\BelongsCustomer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusBranch extends Model
{
    use HasFactory;
    use BelongsBranch, BelongsCustomer;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'bonus_date',
        'branch_id',
        'customer_id',
        'customer_detail',
        'bonus',
        'setting_id',
        'status',
        'status_at',
        'status_note',
        'status_by',
        'bonus_info',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'bonus_date' => 'date',
        'customer_detail' => JsonObjectCast::class,
        'bonus' => 'integer',
        'status' => 'integer',
    ];

    // scope
    public function scopeByBranch(Builder $builder, $branchId): Builder
    {
        return $builder->where('branch_id', '=', $branchId);
    }

    public function scopeByPending(Builder $builder): Builder
    {
        return $builder->where('status', '=', BONUS_STATUS_PENDING);
    }

    public function scopeByTransferred(Builder $builder): Builder
    {
        return $builder->where('status', '=', BONUS_STATUS_TRANSFERRED);
    }

    public function scopeByCustomers(Builder $builder, array|int $customers): Builder
    {
        if (!is_array($customers)) $customers = [$customers];

        return $builder->whereIn('customer_id', $customers);
    }
}
