<?php

namespace App\Models;

use App\Casts\UppercaseCast;
use App\Models\Traits\ModelAddressTrait;
use App\Models\Traits\ModelIDTrait;
use App\Models\Traits\ModelPhoneTrait;
use App\Models\Traits\ModelToDetailTrait;
use App\Models\Traits\User\UserBonusTrait;
use App\Models\Traits\User\CustomerRegisterTrait;
use App\Models\Traits\User\UserStructureTrait;
use App\Models\Traits\User\UserBranchTrait;
use App\Models\Traits\User\UserStatusTrait;
use App\Models\Traits\User\UserTypeTrait;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Nette\Utils\Random;

// use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    // use HasApiTokens;
    use HasFactory, Notifiable;
    use ModelIDTrait, ModelAddressTrait, ModelPhoneTrait, ModelToDetailTrait;
    use UserTypeTrait, UserStatusTrait, UserBonusTrait, UserBranchTrait, UserStructureTrait, CustomerRegisterTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'user_group',
        'user_type',
        'status',
        'branch_id',
        'referral_id',
        'referred_count',
        'phone',
        'has_profile',
        'identity',
        'gender',
        'birth_date',
        'address',
        'village_id',
        'village_name',
        'district_name',
        'city_name',
        'province_name',
        'pos_code',
        'photo',
        'activated_at',
        'activated_by',
        'blocked_at',
        'blocked_by',
        'blocked_note',
        'banned_at',
        'banned_by',
        'banned_note',
        'rejected_at',
        'rejected_by',
        'rejected_note',
        'customer_id',
        'session_id',
        'link_token',
        'password_token',
        'password_token_at',
        'password_exp_at',
        'password_reset_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'session_id',
        'link_token',
        'password_token',
        'password_token_at',
        'password_exp_at',
        'password_reset_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'user_group' => 'integer',
        'user_type' => 'integer',
        'status' => 'integer',
        'referred_count' => 'integer',
        'has_profile' => 'boolean',
        'gender' => UppercaseCast::class,
        'email_verified_at' => 'datetime',
        'birth_date' => 'date',
        'password_token_at' => 'datetime',
        'password_exp_at' => 'datetime',
        'password_reset_at' => 'datetime',
    ];

    protected $ignoresDetail = [
        'username',
        'name',
        'email',
        'user_group',
    ];

    // relation
    public function referral()
    {
        return $this->belongsTo(static::class, 'referral_id', 'id')->byAgencyUser();
    }

    public function downlines()
    {
        return $this->hasMany(static::class, 'referral_id', 'id')->byAgencyUser();
    }

    public function referredCustomers()
    {
        return $this->hasMany(Customer::class, 'agency_id', 'id');
    }

    public function approvedCustomers()
    {
        return $this->hasMany(Customer::class, 'agency_id', 'id')->byApproved();
    }

    public function fromCustomer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    // public
    public function saveSession(string $sessionId): void
    {
        $this->timestamps = false;
        $this->session_id = $sessionId;
        $this->save();
    }

    // static
    public static function makeLinkToken(): string
    {
        $token = Random::generate(32);

        while (static::query()->byLinkToken($token)->count() > 0) {
            $token = Random::generate(32);
        }

        return $token;
    }

    public static function makeResetPasswordToken(): string
    {
        $token = Random::generate(32);

        while (static::query()->byResetPasswordToken($token)->count() > 0) {
            $token = Random::generate(32);
        }

        return $token;
    }

    // scope
    public function scopeByUsername(Builder $builder, string $username)
    {
        return $builder->where('username', '=', $username);
    }

    public function scopeByEmail(Builder $builder, string $email)
    {
        return $builder->where('email', '=', $email);
    }

    public function scopeByLinkToken(Builder $builder, string $linkToken)
    {
        return $builder->where(function ($w) use ($linkToken) {
            return $w->where('link_token', '=', $linkToken)
                ->orWhere('username', '=', $linkToken);
        });
    }

    public function scopeByResetPasswordToken(Builder $builder, string $token)
    {
        return $builder->where('password_token', '=', $token);
    }

    public function scopeByHasProfile(Builder $builder, bool $has): Builder
    {
        return $builder->where('has_profile', '=', $has);
    }

    // accessor
    public function getAgencyIsFromCustomerAttribute()
    {
        return ($this->agency_user && !empty($this->fromCustomer));
    }

    public function getGenderNameAttribute()
    {
        return Arr::get(GENDERS, $this->gender);
    }

    public function getUserMiddlewareAttribute()
    {
        if ($this->main_user) {
            $names = ['main'];
            if ($this->main_user_super) {
                $names[] = 'super';
            } elseif ($this->main_user_master) {
                $names[] = 'master';
            } else {
                $names[] = 'admin';
            }

            return implode('.', $names);
        }
        if ($this->branch_user) {
            return $this->branch_user_admin ? 'branch.admin' : 'branch.user';
        }
        if ($this->agency_user) return 'user.agency';

        return 'user.customer';
    }

    public function getHasReferralLinkAttribute()
    {
        return $this->agency_user || $this->branch_user_admin;
    }

    public function getReferralLinkUrlAttribute()
    {
        if ($this->has_referral_link) {
            // return route('regAgency', ['registerReferral' => $this->username]);
            // return route('viewToBePalmer', ['registerPalmerByToken' => $this->link_token]);
            return route('viewToBePalmer', ['registerPalmerByToken' => $this->username]);
        }

        return null;
    }

    public function getUserBankTypeAttribute()
    {
        $type = BANK_TYPE_AGENCY;

        if ($this->main_user) {
            $type = BANK_TYPE_MAIN;
        } elseif ($this->branch_user) {
            $type = BANK_TYPE_BRANCH;
        }

        return $type;
    }

    public function getUserBankUserIdAttribute()
    {
        if ($this->main_user) {
            return BANK_TYPE_MAIN;
        } elseif ($this->branch_user) {
            return $this->branch->id;
        }

        return $this->id;
    }

    public function getUserBanksAttribute()
    {
        return UserBank::query()
            ->byUser($this->user_bank_user_id)
            ->byType($this->user_bank_type)
            ->orderBy('is_active', 'desc')
            ->orderBy('bank_name')
            ->with('bank')
            ->get();
    }

    public function getAgencyActiveBankAttribute()
    {
        return $this->user_banks->where('is_active', '=', true)->first();
    }

    public function getHasActiveBankAttribute()
    {
        return ($this->user_banks->where('is_active', '=', true)->count() > 0);
    }
}
