<?php

namespace App\Models;

use App\Models\Traits\ModelCodeTrait;
use App\Models\Traits\ModelIDTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;
    use ModelIDTrait, ModelCodeTrait;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'filename',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $casts = [];

    public const DOC_DISK = 'document';
    public const DOC_ASSET = 'files/';

    // accessor
    public function getDownloadUrlAttribute()
    {
        return asset(static::DOC_ASSET . $this->filename);
    }
}
