<?php

namespace App\Models;

use App\Models\Traits\ModelActiveTrait;
use App\Models\Traits\ModelIDTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBank extends Model
{
    use HasFactory, SoftDeletes;
    use ModelIDTrait, ModelActiveTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'bank_type',
        'bank_id',
        'bank_name',
        'account_no',
        'account_name',
        'is_main',
        'is_active',
        'created_by',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $casts = [
        'bank_type' => 'integer',
        'is_main' => 'boolean',
        'is_active' => 'boolean',
    ];

    // relation
    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    // scope
    public function scopeByType(Builder $builder, int $type)
    {
        return $builder->where('bank_type', '=', $type);
    }

    public function scopeByUser(Builder $builder, User|Branch|int|string $user)
    {
        $userId = (($user instanceof User) || ($user instanceof Branch)) ? $user->id : $user;

        return $builder->where('user_id', '=', $userId);
    }

    public function scopeByMain(Builder $builder)
    {
        return $builder->byType(BANK_TYPE_MAIN);
    }
}
