<?php

namespace App\Models;

use App\Casts\JsonObjectCast;
use App\Models\Traits\Relationship\BelongsCustomer;
use App\Models\Traits\Relationship\BelongsUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusPoint extends Model
{
    use HasFactory;
    use BelongsUser, BelongsCustomer;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'bonus_date',
        'user_id',
        'customer_id',
        'customer_detail',
        'point',
        'on_point',
        'bonus',
        'setting_id',
        'status',
        'status_at',
        'status_note',
        'status_by',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'bonus_date' => 'date',
        'customer_detail' => JsonObjectCast::class,
        'point' => 'integer',
        'on_point' => 'integer',
        'bonus' => 'integer',
        'status' => 'integer',
    ];
}
