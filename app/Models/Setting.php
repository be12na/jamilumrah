<?php

namespace App\Models;

use App\Casts\JsonCollectionCast;
use App\Casts\JsonObjectCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'bonus_sponsor',
        'bonus_level',
        'bonus_point',
        'bonus_branch',
        'created_by',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $casts = [
        'bonus_sponsor' => 'integer',
        'bonus_level' => JsonCollectionCast::class,
        'bonus_point' => JsonObjectCast::class,
        'bonus_branch' => 'integer',
    ];
}
