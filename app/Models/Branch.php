<?php

namespace App\Models;

use App\Models\Traits\ModelActiveTrait;
use App\Models\Traits\ModelAddressTrait;
use App\Models\Traits\ModelIDTrait;
use App\Models\Traits\ModelPhoneTrait;
use App\Models\Traits\ModelToDetailTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    use ModelIDTrait, ModelActiveTrait, ModelAddressTrait, ModelPhoneTrait, ModelToDetailTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'leader_name',
        'leader_phone',
        'address',
        'village_id',
        'village_name',
        'district_name',
        'city_name',
        'province_name',
        'pos_code',
        'phone',
        'fax',
        'is_active',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    protected $ignoresDetail = [
        'name',
    ];

    // relation
    public function bonus()
    {
        return $this->hasMany(BonusBranch::class, 'branch_id', 'id');
    }

    public function admin()
    {
        return $this->hasOne(User::class, 'branch_id', 'id')->byBranchUser();
    }

    // accessor
    public function getHasAdminAttribute()
    {
        return !empty($this->admin);
    }

    public function getWaArrayAttribute()
    {
        $contacts = [$this->phone];
        if (empty($this->admin)) return $contacts;

        $phone1 = $this->phone_without_code;
        $phone2 = $this->admin->phone_without_code;

        if ($phone1 != $phone2) {
            $contacts[] = $this->admin->phone;
        }

        return $contacts;
    }
}
