<?php

namespace App\Models;

use App\Casts\UppercaseCast;
use App\Models\Traits\ModelActiveTrait;
use App\Models\Traits\ModelIDTrait;
use App\Models\Traits\ModelToDetailTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    use ModelIDTrait, ModelActiveTrait, ModelToDetailTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'price',
        'description',
        'content',
        'is_active',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $casts = [
        'code' => UppercaseCast::class,
        'price' => 'integer',
        'is_active' => 'boolean',
    ];

    protected $ignoresDetail = [
        'name',
        'price',
        'description',
    ];
}
