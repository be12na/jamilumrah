<?php

namespace App\Models;

use App\Casts\JsonObjectCast;
use App\Casts\UppercaseCast;
use App\Mail\ActivationCustomerMail;
use App\Models\Traits\ModelAddressTrait;
use App\Models\Traits\ModelCodeTrait;
use App\Models\Traits\ModelDetailAddressTrait;
use App\Models\Traits\ModelIDTrait;
use App\Models\Traits\ModelToDetailTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Nette\Utils\Random;

class Customer extends Model
{
    use HasFactory, Notifiable;
    use ModelIDTrait, ModelCodeTrait, ModelAddressTrait, ModelDetailAddressTrait, ModelToDetailTrait;

    protected $detailColumnHasAddress = 'branch_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'agency_id',
        'branch_id',
        'package_id',
        'agency_detail',
        'branch_detail',
        'package_detail',
        'identity',
        'name',
        'email',
        'father_name',
        'gender',
        'birth_city',
        'birth_date',
        'age',
        'is_wni',
        'wna_country',
        'address',
        'village_id',
        'village_name',
        'district_name',
        'city_name',
        'province_name',
        'pos_code',
        'phone',
        'school_id',
        'profession',
        'companion',
        'companion_id',
        'blood_id',
        'smoking',
        'photo',
        'payment_method',
        'payment_image',
        'payment_note',
        'status',
        'approved_by',
        'approved_at',
        'rejected_by',
        'rejected_at',
        'rejected_note',
        'canceled_by',
        'canceled_at',
        'canceled_note',
        'transfer_by',
        'transfer_at',
        'created_by',
        'is_ever',
        'ever_id',
        'register_token',
        'is_agency',
        'main_bank_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'code' => UppercaseCast::class,
        'agency_detail' => JsonObjectCast::class,
        'branch_detail' => JsonObjectCast::class,
        'package_detail' => JsonObjectCast::class,
        'gender' => UppercaseCast::class,
        'birth_date' => 'date',
        'age' => 'integer',
        'is_wni' => 'boolean',
        'school_id' => 'integer',
        'companion_id' => 'integer',
        'blood_id' => UppercaseCast::class,
        'smoking' => 'boolean',
        'payment_method' => 'integer',
        'status' => 'integer',
        'is_ever' => 'boolean',
        'is_agency' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'register_token',
    ];

    protected $ignoresDetail = [
        'name',
        'email',
        'phone',
    ];

    public const IMAGE_DISK = 'transfer';
    public const IMAGE_ASSET = 'images/transfers/';

    // relation
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'customer_id', 'id');
    }

    public function agency()
    {
        return $this->belongsTo(User::class, 'agency_id', 'id');
    }

    public function userAgency()
    {
        return $this->hasOne(User::class, 'customer_id', 'id');
    }

    public function mainBank()
    {
        return $this->belongsTo(UserBank::class, 'main_bank_id', 'id');
    }

    // scope
    public function scopeByOwner(Builder $builder, Request|User $requestUser): Builder
    {
        $user = ($requestUser instanceof Request) ? $requestUser->user() : $requestUser;

        if ($user->agency_user) {
            return $builder->where('agency_id', '=', $user->id);
        } elseif ($user->branch_user) {
            return $builder->where('branch_id', '=', $user->branch_id);
        }

        return $builder;
    }

    public function scopeByStatus(Builder $builder, array|int $status): Builder
    {
        if (is_array($status)) return $builder->whereIn('status', $status);

        return $builder->where('status', '=', $status);
    }

    public function scopeByEditable(Builder $builder): Builder
    {
        return $builder->byStatus(static::editableStatus());
    }

    public function scopeByTransferable(Builder $builder): Builder
    {
        return $builder->byStatus(static::transferableStatus());
    }

    public function scopeByApproving(Builder $builder): Builder
    {
        return $builder->byStatus(CUSTOMER_STATUS_TRANSFER);
    }

    public function scopeByCanceling(Builder $builder): Builder
    {
        return $builder->byStatus([CUSTOMER_STATUS_REGISTER, CUSTOMER_STATUS_REJECTED]);
    }

    public function scopeByApproved(Builder $builder): Builder
    {
        return $builder->byStatus(CUSTOMER_STATUS_APPROVED);
    }

    public function scopeByRegisteringAgency(Builder $builder): Builder
    {
        return $builder->byStatus(CUSTOMER_STATUS_APPROVED)
            ->where('is_agency', '=', false);
    }

    public function scopeByRegisterToken(Builder $builder, string $token): Builder
    {
        return $builder->where('register_token', '=', $token);
    }

    // static
    public static function makeCode(): string
    {
        $tmpCode = 'CR-' . date('Ymd');
        $code = $tmpCode . Random::generate(5, '0-9');

        while (!empty(static::byCode($code)->first())) {
            $code = $tmpCode . Random::generate(5, '0-9');
        }

        return $code;
    }

    public static function makeRegisterToken(): string
    {
        $token = Random::generate(64);

        while (!empty(static::byRegisterToken($token)->first())) {
            $token = Random::generate(64);
        }

        return $token;
    }

    public static function editableStatus(): array
    {
        return [CUSTOMER_STATUS_REGISTER, CUSTOMER_STATUS_TRANSFER, CUSTOMER_STATUS_REJECTED];
    }

    public static function transferableStatus(): array
    {
        return [CUSTOMER_STATUS_REGISTER, CUSTOMER_STATUS_REJECTED];
    }

    public static function newStatus(): array
    {
        return [CUSTOMER_STATUS_REGISTER, CUSTOMER_STATUS_TRANSFER, CUSTOMER_STATUS_REJECTED];
    }

    public static function downloadImportFormat(string $format = 'csv')
    {
        $excelFormatFile = "import-jamaah.{$format}";
        $directory = app_path('Libs/Format');

        $file = $directory . '/' . $excelFormatFile;

        return response()->download($file, "format-{$excelFormatFile}");
    }

    // accessor
    public function getGenderNameAttribute()
    {
        return Arr::get(GENDERS, $this->gender);
    }

    public function getStudyNameAttribute()
    {
        return Arr::get(SCHOOL_LEVELS, $this->school_id);
    }

    public function getFullCompanionAttribute()
    {
        $result = Arr::get(COMPANION_RELATIONSHIP, $this->companion_id, '-');
        if ($this->companion_id > COMPANION_NONE) {
            $result = $this->companion . " ({$result})";
        }

        return $result;
    }

    public function getCitizenAttribute()
    {
        return $this->is_wni ? 'Indonesia' : $this->wna_country;
    }

    public function getIsCancelableAttribute()
    {
        return in_array($this->status, [CUSTOMER_STATUS_REGISTER, CUSTOMER_STATUS_REJECTED]);
    }

    public function getIsEditableAttribute()
    {
        return in_array($this->status, static::editableStatus());
    }

    public function getIsTransferableAttribute()
    {
        return in_array($this->status, static::transferableStatus());
    }

    public function getIsDeletableAttribute()
    {
        return ($this->status == CUSTOMER_STATUS_APPROVED);
    }

    public function getIsApprovingAttribute()
    {
        return ($this->status == CUSTOMER_STATUS_TRANSFER);
    }

    public function getIsResendToBeAgencyAttribute()
    {
        if ($this->status != CUSTOMER_STATUS_APPROVED) return false;

        return (empty($this->userAgency) && ($this->is_agency == false));
    }

    public function getImageDiskAttribute()
    {
        return static::IMAGE_DISK;
    }

    public function getPaymentImageUrlAttribute()
    {
        return $this->payment_image ? asset(static::IMAGE_ASSET . $this->payment_image) : '';
    }

    // public
    public function sendEmailToBeAgency(): void
    {
        Mail::to($this->email)->send(new ActivationCustomerMail($this));
    }
}
