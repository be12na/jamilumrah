<?php

use App\Models\Customer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

if (!function_exists('isLive')) {
    function isLive()
    {
        return (in_array(strtolower(env('APP_ENV')), ['production', 'staging']));
    }
}

if (!function_exists('hasProfile')) {
    function hasProfile($user = null): bool
    {
        if (empty($user)) $user = auth()->user();
        if (empty($user)) return false;

        return $user->agency_user ? $user->has_profile : true;
    }
}

if (!function_exists('canBankAction')) {
    function canBankAction(string $actionName, $user = null): bool
    {
        $actionName = strtolower($actionName);
        if (!in_array($actionName, ['enable', 'disable', 'create'])) return false;

        if (empty($user)) $user = auth()->user();
        if (empty($user)) return false;

        return (
            (($user->agency_user || $user->branch_user_admin) && in_array($actionName, ['create', 'enable']))
            || $user->main_user_super
        );
    }
}

if (!function_exists('mainAgency')) {
    function mainAgency($agency, $field = 'user_group'): bool
    {
        if (is_array($agency)) $agency = (object) $agency;

        return ($agency->$field == USER_GROUP_MAIN);
    }
}

if (!function_exists('canEditCustomer')) {
    function canEditCustomer($customer, $user = null): bool
    {
        if (empty($user)) $user = auth()->user();
        if (empty($user)) return false;

        if (!($customer instanceof Customer) && !($customer instanceof stdClass)) {
            $customer = Customer::query()->byId($customer)->first();
        }

        if (empty($customer)) return false;

        if ($user->branch_user_admin) {
            return !in_array($customer->status, [CUSTOMER_STATUS_APPROVED, CUSTOMER_STATUS_CANCELED]);
        }

        return false;
    }
}

if (!function_exists('formatNumber')) {
    function formatNumber($value, $decimals = 0): string
    {
        return number_format($value, $decimals, __('format.decimal'), __('format.thousand'));
    }
}

if (!function_exists('translatedDateToTime')) {
    function translatedDateToTime(string $translatedDate, string $sparator = null): int|null
    {
        if (empty($translatedDate)) return null;
        if (config('app.locale') != 'id') return strtotime($translatedDate);

        if (is_null($sparator)) $sparator = ' ';

        $arrDate = explode($sparator, $translatedDate);
        $test = [];
        foreach ($arrDate as $tt) {
            $res = $tt;
            if (!is_numeric($res)) {
                $res = strtolower($res);
                $res = ucfirst(Arr::get(DATE_ID, $res, $tt));
            }

            $test[] = $res;
        }

        $newDate = implode($sparator, $test);

        return strtotime($newDate);
    }
}

if (!function_exists('translateDatetime')) {
    function translateDatetime($time, string $format = null)
    {
        if (empty($time)) return null;
        if (empty($format)) $format = __('format.date.short') . ' H:i:s';

        $carbon = $time;
        if (!($time instanceof Carbon)) {
            if (is_int($time)) {
                $carbon = Carbon::createFromTimestamp($time);
            } else {
                $carbon = Carbon::createFromTimeString((string) $time);
            }
        }

        return $carbon->translatedFormat($format);
    }
}

if (!function_exists('dayName')) {
    function dayName($time)
    {
        $carbon = $time;
        if (!($time instanceof Carbon)) {
            if (!is_int($time)) $time = strtotime($time);

            $carbon = Carbon::createFromTimestamp($time);
        }

        return $carbon->dayName;
    }
}

if (!function_exists('matchMenu')) {
    function matchMenu($requireNames): bool
    {
        $match = false;
        $requiredList = is_array($requireNames) ? $requireNames : [$requireNames];
        $explodedName = explode('.', $currentName = Route::currentRouteName());
        if (!empty($currentName) && !empty($requiredList)) {
            $tmpName = '';
            $tmpList = [];
            foreach ($explodedName as $name) {
                $tmpName .= ".{$name}";
                $tmpName = ltrim($tmpName, '.');
                $tmpList[] = $tmpName;
            }

            foreach ($tmpList as $nameInList) {
                if (in_array($nameInList, $requiredList)) {
                    $match = true;
                    break;
                }
            }
        }

        return $match;
    }
}

if (!function_exists('optionSelected')) {
    function optionSelected($data_value, $true_value)
    {
        $data_value = (string) $data_value;
        $true_value = (string) $true_value;

        return ($data_value == $true_value) ? 'selected' : '';
    }
}

if (!function_exists('checkboxChecked')) {
    function checkboxChecked($data, $field, $true_value = 1, $default = false, $in_error = false)
    {
        $value = isset($field) ? old($field) : null;
        if ($in_error) {
            return $value ? 'checked' : '';
        }
        if (isset($value)) {
            return ($value == $true_value) ? 'checked' : '';
        } else {
            if (isset($data)) {
                if (isset($field)) {
                    return ($data->$field == $true_value) ? 'checked' : '';
                }
            }
        }

        return $default ? 'checked' : '';
    }
}

if (!function_exists('contentCheck')) {
    function contentCheck(bool $value)
    {
        return ($value === true) ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>';
    }
}

if (!function_exists('contentUrl')) {
    function contentUrl(string $url): string
    {
        $arrLink = explode('//', $url);
        $arrDomain = explode('/', $arrLink[1]);
        $url1 = $arrLink[0] . '//' . $arrDomain[0];
        unset($arrDomain[0]);

        return '<span class="text-decoration-underline">' . implode('</span><span class="d-inline-block text-decoration-underline">/', array_merge([$url1], $arrDomain)) . '</span>';
    }
}

if (!function_exists('customerStatusList')) {
    function customerStatusList(User $user): array
    {
        $key = 'main';
        if ($user->agency_user) {
            $key = 'agency';
        } elseif ($user->branch_user) {
            $key = 'branch';
        }

        $result = [];
        foreach (CUSTOMER_STATUS_LIST as $index => $value) {
            $result[$index] = [$value[$key]['text'], $value[$key]['class']];
        }

        return $result;
    }
}

if (!function_exists('errorMessageContent')) {
    function errorMessageContent(Validator|array|string $messages, string $errorLevel = 'danger', string $title = null)
    {
        if (empty($title)) $title = 'Proses gagal';

        $message = '<div class="fw-bold mb-1">' . $title . '</div>';

        if ($messages instanceof Validator) {
            $message .= '<ul class="mb-0 ps-3">';
            foreach ($messages->errors()->toArray() as $errors) {
                $message .= '<li>' . $errors[0] . '</li>';
            }
            $message .= '</ul>';
        } elseif (is_array($messages)) {
            $message .= '<ul class="mb-0 ps-3">';
            foreach ($messages as $error) {
                $message .= '<li>' . $error . '</li>';
            }
            $message .= '</ul>';
        } else {
            $message .= $messages;
        }

        return view('partials.alert', [
            'message' => $message,
            'messageClass' => $errorLevel
        ])->render();
    }
}

if (!function_exists('ajaxError')) {
    function ajaxError(string $message = null, int $status = 500)
    {
        if (empty($message)) $message = "<h5 class=\"bg-white text-danger\">Error</h5>";

        return response($message, $status);
    }
}

if (!function_exists('pageError')) {
    function pageError(string $message = null, string $redirectRoute = null, array $routeParams = null, string $errorLevel = 'danger')
    {
        if (empty($message)) $message = 'Page Error';
        if (empty($redirectRoute)) $redirectRoute = 'dashboard';
        if (is_null($routeParams)) $routeParams = [];

        return redirect()->route($redirectRoute, $routeParams)
            ->with('message', $message)
            ->with('messageClass', $errorLevel);
    }
}

if (!function_exists('fallbackRouteBinding')) {
    function fallbackRouteBinding($data, string $fallbackMessage = '', string $fallbackRoute = null, array $fallbackRouteParams = null)
    {
        if (!empty($data)) return $data;

        if (empty($fallbackMessage)) $fallbackMessage = 'Data tidak ditemukan.';
        if (empty($fallbackRoute)) $fallbackRoute = 'dashboard';
        if (is_null($fallbackRouteParams)) $fallbackRouteParams = [];

        throw new HttpResponseException(
            request()->ajax()
                ? ajaxError($fallbackMessage, 404)
                : pageError($fallbackMessage, $fallbackRoute, $fallbackRouteParams)
        );
    }
}
