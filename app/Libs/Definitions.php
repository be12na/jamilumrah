<?php

if (!defined('USER_STATUS_INACTIVE')) define('USER_STATUS_INACTIVE', 0);
if (!defined('USER_STATUS_ACTIVE')) define('USER_STATUS_ACTIVE', 1);
if (!defined('USER_STATUS_BLOCKED')) define('USER_STATUS_BLOCKED', 3);
if (!defined('USER_STATUS_BANNED')) define('USER_STATUS_BANNED', 5);
if (!defined('USER_STATUS_REJECTED')) define('USER_STATUS_REJECTED', 9);
if (!defined('USER_STATUS_NAMES')) define('USER_STATUS_NAMES', [
    USER_STATUS_INACTIVE => 'Belum Aktif',
    USER_STATUS_ACTIVE => 'Aktif',
    USER_STATUS_BLOCKED => 'Tidak Aktif',
    USER_STATUS_BANNED => 'Blokir Permanen',
    USER_STATUS_REJECTED => 'Ditolak',
]);

if (!defined('USER_GROUP_MAIN')) define('USER_GROUP_MAIN', 10);
if (!defined('USER_GROUP_BRANCH')) define('USER_GROUP_BRANCH', 20);
if (!defined('USER_GROUP_AGENCY')) define('USER_GROUP_AGENCY', 30);

if (!defined('USER_MAIN_SUPER')) define('USER_MAIN_SUPER', 1);
if (!defined('USER_MAIN_MASTER')) define('USER_MAIN_MASTER', 2);
if (!defined('USER_MAIN_ADMIN')) define('USER_MAIN_ADMIN', 3);
if (!defined('USER_MAIN_NAMES')) define('USER_MAIN_NAMES', [
    USER_MAIN_SUPER => 'Super Administrator',
    USER_MAIN_MASTER => 'Master Administrator',
    USER_MAIN_ADMIN => 'Administrator',
]);

if (!defined('USER_BRANCH_ADMIN')) define('USER_BRANCH_ADMIN', 5);
if (!defined('USER_BRANCH_NAMES')) define('USER_BRANCH_NAMES', [
    USER_BRANCH_ADMIN => 'Administrator Cabang',
]);

if (!defined('USER_AGENCY_BASIC')) define('USER_AGENCY_BASIC', 10);
if (!defined('USER_AGENCY_EXECUTIVE')) define('USER_AGENCY_EXECUTIVE', 20);
if (!defined('USER_AGENCY_EXECUTIVE_DIRECTOR')) define('USER_AGENCY_EXECUTIVE_DIRECTOR', 30);
if (!defined('USER_AGENCY_NAMES')) define('USER_AGENCY_NAMES', [
    USER_AGENCY_BASIC => 'Basic',
    USER_AGENCY_EXECUTIVE => 'Executive',
    USER_AGENCY_EXECUTIVE_DIRECTOR => 'Executive Director',
]);

if (!defined('AGENCY_EXECUTIVE_REQUIRE_CUSTOMER')) define('AGENCY_EXECUTIVE_REQUIRE_CUSTOMER', 45);
if (!defined('AGENCY_DIRECTOR_REQUIRE_CUSTOMER')) define('AGENCY_DIRECTOR_REQUIRE_CUSTOMER', 450);

if (!defined('SCHOOL_LEVEL_SD')) define('SCHOOL_LEVEL_SD', 10);
if (!defined('SCHOOL_LEVEL_SMP')) define('SCHOOL_LEVEL_SMP', 20);
if (!defined('SCHOOL_LEVEL_SMA')) define('SCHOOL_LEVEL_SMA', 30);
if (!defined('SCHOOL_LEVEL_S1')) define('SCHOOL_LEVEL_S1', 40);
if (!defined('SCHOOL_LEVEL_S2')) define('SCHOOL_LEVEL_S2', 50);
if (!defined('SCHOOL_LEVEL_S3')) define('SCHOOL_LEVEL_S3', 60);
if (!defined('SCHOOL_LEVEL_OTHER')) define('SCHOOL_LEVEL_OTHER', 90);
if (!defined('SCHOOL_LEVELS')) define('SCHOOL_LEVELS', [
    SCHOOL_LEVEL_SD => 'SD',
    SCHOOL_LEVEL_SMP => 'SMP',
    SCHOOL_LEVEL_SMA => 'SMA / D1 / D2 / D3',
    SCHOOL_LEVEL_S1 => 'S1',
    SCHOOL_LEVEL_S2 => 'S2',
    SCHOOL_LEVEL_S3 => 'S3',
    SCHOOL_LEVEL_OTHER => 'Lain - Lain',
]);

if (!defined('COMPANION_NONE')) define('COMPANION_NONE', 0);
if (!defined('COMPANION_PARENT')) define('COMPANION_PARENT', 1);
if (!defined('COMPANION_CHILD')) define('COMPANION_CHILD', 2);
if (!defined('COMPANION_COUPLE')) define('COMPANION_COUPLE', 3);
if (!defined('COMPANION_PARENT_LAW')) define('COMPANION_PARENT_LAW', 4);
if (!defined('COMPANION_SIBLING')) define('COMPANION_SIBLING', 5);
if (!defined('COMPANION_RELATIONSHIP')) define('COMPANION_RELATIONSHIP', [
    COMPANION_NONE => 'Tanpa Pendamping',
    COMPANION_PARENT => 'Orang Tua',
    COMPANION_CHILD => 'Anak',
    COMPANION_COUPLE => 'Suami / Istri',
    COMPANION_PARENT_LAW => 'Mertua',
    COMPANION_SIBLING => 'Saudara Kandung',
]);

if (!defined('BANK_TYPE_MAIN')) define('BANK_TYPE_MAIN', 1);
if (!defined('BANK_TYPE_BRANCH')) define('BANK_TYPE_BRANCH', 5);
if (!defined('BANK_TYPE_AGENCY')) define('BANK_TYPE_AGENCY', 10);

if (!defined('PAYMENT_CASH')) define('PAYMENT_CASH', 1);
if (!defined('PAYMENT_CREDIT')) define('PAYMENT_CREDIT', 12);
if (!defined('PAYMENT_INSTALLMENT')) define('PAYMENT_INSTALLMENT', 24);
if (!defined('PAYMENT_METHODS')) define('PAYMENT_METHODS', [
    PAYMENT_CASH => 'Cash',
    PAYMENT_CREDIT => "Pembiayaan",
    PAYMENT_INSTALLMENT => "Menabung",
]);

if (!defined('CUSTOMER_STATUS_REGISTER')) define('CUSTOMER_STATUS_REGISTER', 0);
if (!defined('CUSTOMER_STATUS_APPROVED')) define('CUSTOMER_STATUS_APPROVED', 1);
if (!defined('CUSTOMER_STATUS_TRANSFER')) define('CUSTOMER_STATUS_TRANSFER', 3);
if (!defined('CUSTOMER_STATUS_REJECTED')) define('CUSTOMER_STATUS_REJECTED', 5);
if (!defined('CUSTOMER_STATUS_CANCELED')) define('CUSTOMER_STATUS_CANCELED', 7);
if (!defined('CUSTOMER_STATUS_DELETED')) define('CUSTOMER_STATUS_DELETED', 9);
if (!defined('CUSTOMER_STATUS_LIST')) define('CUSTOMER_STATUS_LIST', [
    CUSTOMER_STATUS_REGISTER => [
        'agency' => [
            'text' => 'Verifikasi',
            'class' => 'bg-secondary text-light',
        ],
        'branch' => [
            'text' => 'Jamaah Baru',
            'class' => 'bg-secondary text-light',
        ],
        'main' => [
            'text' => 'Jamaah Baru',
            'class' => 'bg-secondary text-light',
        ],
    ],
    CUSTOMER_STATUS_APPROVED => [
        'agency' => [
            'text' => 'Aktif',
            'class' => 'bg-success text-light',
        ],
        'branch' => [
            'text' => 'Aktif',
            'class' => 'bg-success text-light',
        ],
        'main' => [
            'text' => 'Aktif',
            'class' => 'bg-success text-light',
        ],
    ],
    CUSTOMER_STATUS_TRANSFER => [
        'agency' => [
            'text' => 'Proses Transfer',
            'class' => 'bg-info text-light',
        ],
        'branch' => [
            'text' => 'Verifikasi Transfer',
            'class' => 'bg-info text-light',
        ],
        'main' => [
            'text' => 'Verifikasi Transfer',
            'class' => 'bg-info text-light',
        ],
    ],
    CUSTOMER_STATUS_REJECTED => [
        'agency' => [
            'text' => 'Proses Transfer',
            'class' => 'bg-info text-light',
        ],
        'branch' => [
            'text' => 'Transfer Ditolak',
            'class' => 'bg-warning text-dark',
        ],
        'main' => [
            'text' => 'Transfer Ditolak',
            'class' => 'bg-warning text-dark',
        ],
    ],
]);

if (!defined('BLOODS')) define('BLOODS', ['O', 'A', 'B', 'AB']);

if (!defined('GENDERS')) define('GENDERS', [
    'L' => 'Laki - Laki',
    'P' => 'Perempuan',
]);

if (!defined('DATE_ID')) define('DATE_ID', [
    'januari' => 'january',
    'februari' => 'february',
    'maret' => 'march',
    'april' => 'april',
    'mei' => 'may',
    'juni' => 'june',
    'juli' => 'july',
    'agustus' => 'august',
    'september' => 'september',
    'oktober' => 'october',
    // 'november' => 'november', // bootstrap datepicker
    'nopember' => 'november', // jquery datepicker
    'desember' => 'december',
]);

if (!defined('MESSAGE_400')) define('MESSAGE_400', 'Gagal memvalidasi.');
if (!defined('MESSAGE_403')) define('MESSAGE_403', 'Anda tidak memiliki akses.');
if (!defined('MESSAGE_404')) define('MESSAGE_404', 'Data tidak ditemukan.');
if (!defined('MESSAGE_500')) define('MESSAGE_500', 'Telah terjadi kesalahan pada server. Silahkan coba lagi.');

if (!defined('BONUS_STATUS_PENDING')) define('BONUS_STATUS_PENDING', 0);
if (!defined('BONUS_STATUS_TRANSFERRED')) define('BONUS_STATUS_TRANSFERRED', 1);
if (!defined('BONUS_STATUS_LIST')) define('BONUS_STATUS_LIST', [
    BONUS_STATUS_PENDING => 'Belum Ditransfer',
    BONUS_STATUS_TRANSFERRED => 'Sudah Ditransfer',
]);
