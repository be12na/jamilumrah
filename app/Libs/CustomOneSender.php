<?php

namespace App\Libs;

use App\Models\Customer;
use App\Models\User;
use Pasya\OneSenderLaravel\OneSender;

class CustomOneSender extends OneSender
{
    function getPhoneNumber($notifiable)
    {
        $phone = $notifiable;

        if (is_a($notifiable, User::class) || is_a($notifiable, Customer::class)) {
            $phoneField = config('onesender.phone_column');
            $phone = $notifiable->{$phoneField};
        }

        if (!empty($phone)) {
            $phone = preg_replace('/[^0-9]+/', '', $phone);
            if (substr($phone, 0, 2) == '08') {
                $phone = '628' . substr($phone, 2);
            }
        }

        return $phone;
    }
}
