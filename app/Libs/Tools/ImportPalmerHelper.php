<?php

namespace App\Libs\Tools;

use Carbon\Carbon;

class ImportPalmerHelper
{
    private static function genderCode(string $gender): string
    {
        $gender = strtolower($gender);
        $woman = ['p', 'perempuan', 'wanita'];

        return in_array($gender, $woman) ? 'P' : 'L';
    }

    private static function handphone(string $phone): string
    {
        if (!in_array(substr($phone, 0, 1), ['0', '+'])) {
            $phone = "0{$phone}";
        }

        return $phone;
    }

    private static function birthDate(string $date, $asPossibleCarbon = true): Carbon|string
    {
        if (empty($date)) return date('Y-m-d');

        $dates = explode('/', $date);

        if (count($dates) == 3) {
            $day = str_pad(strval(intval($dates[0])), 2, '0', STR_PAD_LEFT);
            $month = str_pad(strval(intval($dates[1])), 2, '0', STR_PAD_LEFT);;
            $year = intval($dates[2]);

            if ($year < 100) {
                $yearNow = intval(date('y'));
                if ($year > $yearNow) {
                    $year = 1000 + $year;
                } else {
                    $year = 2000 + $year;
                }
            }

            return $asPossibleCarbon ? Carbon::create($year, $month, $day) : "{$year}-{$month}-{$day}";
        } else {
            $time = intval($date);

            return $asPossibleCarbon ? Carbon::createFromTimestamp($time) : date('Y-m-d', $time);
        }
    }

    public static function mappingRow(array $row): array
    {
        return [
            'identity' => $row['identitas'],
            'name' => $row['nama'],
            'gender' => self::genderCode($row['kelamin']),
            'birth_city' => $row['tmp_lahir'],
            'birth_date' => self::birthDate($row['tgl_lahir']),
            'email' => $row['email'],
            'phone' => self::handphone($row['handphone']),
            'address' => $row['alamat'],
            'pos_code' => $row['kode_pos'],
        ];
    }
}
