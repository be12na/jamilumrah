<?php

namespace App\Libs\Tools;

class CsvImportPalmer
{
    private array $records;

    public function __construct(string $filePath)
    {
        $records = array_map('str_getcsv', file($filePath));
        $rows = [];

        if (count($records) > 0) {
            $fields = array_map('strtolower', $records[0]);
            array_shift($records);

            foreach ($records as $record) {
                if (count($fields) != count($record)) {
                    $rows = [];
                    break;
                }

                $record = array_map("html_entity_decode", $record);
                $record = array_combine($fields, $record);

                $rows[] = $this->clear_encoding_str($record);
            }
        }

        $this->records = $rows;
    }

    private function clear_encoding_str($value)
    {
        if (is_array($value)) {
            $clean = [];
            foreach ($value as $key => $val) {
                $clean[$key] = mb_convert_encoding($val, 'UTF-8', 'UTF-8');
            }
            return $clean;
        }
        return mb_convert_encoding($value, 'UTF-8', 'UTF-8');
    }

    public function rows(): array
    {
        if (empty($this->records)) return [];

        $rows = [];

        foreach ($this->records as $row) {
            $rows[] = ImportPalmerHelper::mappingRow($row);
        }

        return $rows;
    }
}
