<?php

namespace App\Libs\Tools;

use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExcelImportPalmer implements ToArray, WithHeadingRow
{
    public function array(array $array)
    {
        return ImportPalmerHelper::mappingRow($array);
    }

    public function rows(array $records): array
    {
        if (empty($records)) return [];

        $rows = [];

        foreach ($records as $row) {
            $rows[] = ImportPalmerHelper::mappingRow($row);
        }

        return $rows;
    }
}
