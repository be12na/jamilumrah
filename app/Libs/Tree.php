<?php

namespace App\Libs;

use Illuminate\Support\Arr;

class Tree
{
    public static function setNode($items = []): string
    {
        $elmNode = '<div class="box-node small fs-auto text-nowrap">%s</div>';

        $elms = '';

        $logo = Arr::get($items, 'logo', []);
        if (!empty($logo)) {
            $image = Arr::get($logo, 'image');
            $cssClass = Arr::get($logo, 'cssClass');
            $styles = Arr::get($logo, 'cssStyle', []);
            $elms .= self::setNodeLogo($image, $cssClass, $styles);
        }

        $infos = Arr::get($items, 'elements', []);
        if (count($infos) > 0) {
            foreach ($infos as $info) {
                $infoText = Arr::get($info, 'htmlText');
                $cssClass = Arr::get($info, 'cssClass');
                $styles = Arr::get($info, 'cssStyle', []);
                $elms .= self::setElementNode($infoText, $cssClass, $styles);
            }
        }

        return sprintf($elmNode, $elms);
    }

    public static function setNodeLogo(string $logo, string $cssClass = null, array $styles = null): string
    {
        $style = '';
        if (!empty($styles)) {
            $style = implode('', array_map(function ($k, $v) {
                return "{$k}:{$v};";
            }, array_keys($styles), array_values($styles)));
        }

        return sprintf('<img src="%s" alt="Logo" class="%s" style="%s"/>', $logo, $cssClass ?? '', $style);
    }

    public static function setElementNode(string $htmlText, string $cssClass = null, array $styles = null): string
    {
        $style = '';
        if (!empty($styles)) {
            $style = implode('', array_map(function ($k, $v) {
                return "{$k}:{$v};";
            }, array_keys($styles), array_values($styles)));
        }

        return sprintf('<div class="%s" style="%s">%s</div>', $cssClass ?? '', $style, $htmlText);
    }
}
