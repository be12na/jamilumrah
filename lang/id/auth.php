<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Identitas pengguna tidak ditemukan.',
    'password' => 'Password yang anda masukkan salah.',
    'throttle' => 'Terlalu banyak percobaan masuk. Silahkan dicoba kembali setelah :seconds detik.',
    'disabled' => 'Akun Anda telah dinonaktifkan.',

];
