<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            $table->string('name', 100);
            $table->string('leader_name', 100);
            $table->string('leader_phone', 15);
            $table->string('address', 250);
            $table->string('village_id', 15);
            $table->string('village_name', 100);
            $table->string('district_name', 100);
            $table->string('city_name', 100);
            $table->string('province_name', 100);
            $table->string('pos_code', 5)->nullable();
            $table->string('phone', 15);
            $table->string('fax', 15)->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
};
