<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 30)->unique();
            $table->string('name', 100);
            $table->string('email', 100);
            $table->unsignedSmallInteger('user_group');
            $table->unsignedSmallInteger('user_type');
            $table->unsignedSmallInteger('status')->default(USER_STATUS_INACTIVE);
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedBigInteger('referral_id')->nullable();
            $table->unsignedBigInteger('referred_count')->default(0);
            $table->string('phone', 15)->nullable();
            $table->boolean('has_profile')->default(false);
            $table->string('identity', 50)->nullable();
            $table->string('gender', 1)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('address', 250)->nullable();
            $table->string('village_id', 15)->nullable();
            $table->string('village_name', 100)->nullable();
            $table->string('district_name', 100)->nullable();
            $table->string('city_name', 100)->nullable();
            $table->string('province_name', 100)->nullable();
            $table->string('pos_code', 5)->nullable();
            $table->string('photo', 50)->nullable();
            $table->string('password', 255);
            $table->string('link_token', 32)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->unsignedBigInteger('activated_by')->nullable();
            $table->timestamp('blocked_at')->nullable();
            $table->unsignedBigInteger('blocked_by')->nullable();
            $table->string('blocked_note', 250)->nullable();
            $table->timestamp('banned_at')->nullable();
            $table->unsignedBigInteger('banned_by')->nullable();
            $table->string('banned_note', 250)->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->unsignedBigInteger('rejected_by')->nullable();
            $table->string('rejected_note', 250)->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('session_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->string('password_token', 32)->nullable();
            $table->timestamp('password_token_at')->nullable();
            $table->timestamp('password_exp_at')->nullable();
            $table->timestamp('password_reset_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
