<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            $table->unsignedBigInteger('bonus_sponsor')->default(1000000);
            $table->json('bonus_level')->default(json_encode([
                ['level' => 1, 'bonus' => 1000000],
                ['level' => 2, 'bonus' => 500000],
            ]));
            $table->json('bonus_point')->default(json_encode([
                'referred_count' => 450,
                'bonus' => 50000
            ]));
            $table->unsignedBigInteger('bonus_branch')->default(450000);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
};
