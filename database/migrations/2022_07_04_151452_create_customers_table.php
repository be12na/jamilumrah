<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('code', 50)->unique();
            $table->unsignedBigInteger('agency_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('package_id');
            $table->json('agency_detail');
            $table->json('branch_detail');
            $table->json('package_detail');
            $table->string('identity', 50);
            $table->string('name', 100);
            $table->string('email', 100);
            $table->string('father_name', 100)->nullable();
            $table->string('gender', 1)->default('L');
            $table->string('birth_city', 100);
            $table->date('birth_date');
            $table->unsignedInteger('age');
            $table->boolean('is_wni')->default(true);
            $table->string('wna_country', 50)->nullable();
            $table->string('address', 250);
            $table->string('village_id', 15);
            $table->string('village_name', 100);
            $table->string('district_name', 100);
            $table->string('city_name', 100);
            $table->string('province_name', 100);
            $table->string('pos_code', 5)->nullable();
            $table->string('phone', 15);
            $table->unsignedSmallInteger('school_id')->default(SCHOOL_LEVEL_OTHER);
            $table->string('profession', 100)->nullable();
            $table->string('companion', 100)->nullable();
            $table->unsignedSmallInteger('companion_id')->default(COMPANION_NONE);
            $table->string('blood_id', 2)->nullable();
            $table->boolean('smoking')->default(false);
            $table->string('photo', 50)->nullable();
            $table->unsignedSmallInteger('payment_method')->default(PAYMENT_CASH);
            $table->string('payment_image', 50)->nullable();
            $table->string('payment_note', 250)->nullable();
            $table->unsignedSmallInteger('status')->default(CUSTOMER_STATUS_REGISTER);
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->unsignedBigInteger('rejected_by')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->string('rejected_note', 250)->nullable();
            $table->unsignedBigInteger('canceled_by')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->string('canceled_note', 250)->nullable();
            $table->unsignedBigInteger('transfer_by')->nullable();
            $table->timestamp('transfer_at')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->boolean('is_ever')->default(false);
            $table->unsignedBigInteger('ever_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->string('register_token', 64)->nullable();
            $table->boolean('is_agency')->default(false);
            $table->unsignedBigInteger('main_bank_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
