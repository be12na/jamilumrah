<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_levels', function (Blueprint $table) {
            $table->id();
            $table->date('bonus_date');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_id');
            $table->json('customer_detail')->nullable();
            $table->unsignedSmallInteger('level');
            $table->json('level_detail');
            $table->unsignedBigInteger('bonus');
            $table->unsignedInteger('setting_id');
            $table->unsignedSmallInteger('status')->default(BONUS_STATUS_PENDING);
            $table->timestamp('status_at')->nullable();
            $table->string('status_note', 255)->nullable();
            $table->string('bonus_info', 255)->nullable();
            $table->unsignedBigInteger('status_by')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_levels');
    }
};
