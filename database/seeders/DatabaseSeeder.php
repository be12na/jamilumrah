<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Bank;
use App\Models\Setting;
use App\Models\User;
use App\Repositories\RegionRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // RegionRepository::dbSeed();

        // if (!User::byUsername('devtst')->exists()) {
        //     User::create([
        //         'username' => 'devtst',
        //         'name' => 'Developer',
        //         'email' => 'developer@' . env('APP_DOMAIN'),
        //         'password' => Hash::make('a'),
        //         'user_group' => USER_GROUP_MAIN,
        //         'user_type' => USER_MAIN_SUPER,
        //         'status' => USER_STATUS_ACTIVE,
        //     ]);
        // }
        // if (!User::byUsername('superadmin')->exists()) {
        //     User::create([
        //         'username' => 'superadmin',
        //         'name' => 'Super Administrator',
        //         'email' => 'super-admin@' . env('APP_DOMAIN'),
        //         'password' => Hash::make('a'),
        //         'user_group' => USER_GROUP_MAIN,
        //         'user_type' => USER_MAIN_SUPER,
        //         'status' => USER_STATUS_ACTIVE,
        //     ]);
        // }
        // if (!User::byUsername('masteradmin')->exists()) {
        //     User::create([
        //         'username' => 'masteradmin',
        //         'name' => 'Master Administrator',
        //         'email' => 'master-admin@' . env('APP_DOMAIN'),
        //         'password' => Hash::make('a'),
        //         'user_group' => USER_GROUP_MAIN,
        //         'user_type' => USER_MAIN_SUPER,
        //         'status' => USER_STATUS_ACTIVE,
        //     ]);
        // }

        // if (empty($top1 = User::byUsername('top1')->first())) {
        //     $top1 = User::create([
        //         'username' => 'top1',
        //         'name' => 'Master Agency',
        //         'email' => 'master-agency@' . env('APP_DOMAIN'),
        //         'password' => Hash::make('a'),
        //         'user_group' => USER_GROUP_AGENCY,
        //         'user_type' => USER_AGENCY_EXECUTIVE_DIRECTOR,
        //         'phone' => '081212121212',
        //         'identity' => '12345678910',
        //         'has_profile' => true,
        //         'status' => USER_STATUS_ACTIVE,
        //     ]);

        //     $top1->structure()->create();
        // } else {
        //     if (empty($top1->structure)) {
        //         $top1->structure()->create();
        //     }
        // }

        if (empty(Setting::query()->first())) {
            Setting::create([
                'bonus_sponsor' => 0,
                'bonus_level' => [
                    ['level' => 1, 'bonus' => 1000000],
                    ['level' => 2, 'bonus' => 500000],
                ],
                'bonus_point' => [
                    'referred_count' => 450,
                    'bonus' => 50000
                ],
                'bonus_branch' => 450000,
            ]);
        }

        // $banks = [
        //     [
        //         'code' => 'BCA',
        //         'name' => 'Bank Central Asia',
        //         'logo' => 'bca.jpg',
        //     ],
        //     [
        //         'code' => 'BRI',
        //         'name' => 'Bank Rakyat Indonesia',
        //         'logo' => 'bri.jpg',
        //     ],
        //     [
        //         'code' => 'BRI Syariah',
        //         'name' => 'Bank BRI Syariah',
        //         'logo' => 'bri-syariah.jpg',
        //     ],
        //     [
        //         'code' => 'BSI',
        //         'name' => 'Bank Syariah Indonesia',
        //         'logo' => 'bsi.jpg',
        //     ],
        //     [
        //         'code' => 'Mandiri',
        //         'name' => 'Bank Mandiri',
        //         'logo' => 'mandiri.jpg',
        //     ],
        //     [
        //         'code' => 'Mandiri Syariah',
        //         'name' => 'Bank Syariah Mandiri',
        //         'logo' => 'mandiri-syariah.jpg',
        //     ],
        // ];

        // foreach ($banks as $bank) {
        //     if (empty(Bank::byCode($bank['code'])->first())) {
        //         Bank::create($bank);
        //     }
        // }
    }
}
